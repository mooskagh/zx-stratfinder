#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include <chrono>

#include "games/writetyper.h"
#include "search/exception.h"
#include "search/search.h"

ABSL_FLAG(bool, ui, true, "Use UI");

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);

  Search search(absl::GetFlag(FLAGS_ui) ? std::make_unique<Ui>() : nullptr,
                writetyper::get_config());

  try {
    search.run(2000, 20);
  } catch (const SearchException& ex) {
    LOG(ERROR) << "SearchException: " << ex.what();
    LOG(ERROR) << sequence_as_string(ex.inputs());
    const auto& frame = ex.frame();
    if (frame) {
      frame->debug.debug_log();
      LOG(ERROR) << "source_idx=" << frame->source_idx;
    }
    return 1;
  }

  return 0;
}
