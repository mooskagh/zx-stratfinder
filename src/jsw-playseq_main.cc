#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include "games/jet-set-willy.h"
#include "misc/filebuffer.h"
#include "proto/jet-set-willy.pb.h"
#include "search/exception.h"
#include "search/search.h"

ABSL_FLAG(std::string, ascii_taskfile, "",
          "ASCII protobuf of a task definition");
ABSL_FLAG(std::string, taskfile, "", "Task definition");
ABSL_FLAG(std::string, inputsfile, "", "Inputs file");
ABSL_FLAG(int, inputstate, 0, "Index of input state");

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);

  CHECK(absl::GetFlag(FLAGS_ascii_taskfile).empty() !=
        absl::GetFlag(FLAGS_taskfile).empty())
      << "Exactly one of --ascii_taskfile or --taskfile must be defined";

  jet_set_willy::TaskRequest task_request;

  if (!absl::GetFlag(FLAGS_ascii_taskfile).empty()) {
    CHECK(google::protobuf::TextFormat::MergeFromString(
        read_file_to_string(absl::GetFlag(FLAGS_ascii_taskfile)),
        &task_request));
  } else if (!absl::GetFlag(FLAGS_taskfile).empty()) {
    CHECK(task_request.ParseFromString(
        read_file_to_string(absl::GetFlag(FLAGS_taskfile))));
  }

  jet_set_willy::RoomResult room_result;
  auto config = jet_set_willy::get_config(task_request, &room_result);

  Ui ui;
  Machine machine;
  ui.set_zx_screen_memory(machine.memory());
  machine.set_rzx_writer(std::make_unique<RzxWriter>());
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  config.init_func(&machine);
  auto start_frames = config.init_states_func(machine, config);

  LOG(INFO) << start_frames.size() << ' ' << absl::GetFlag(FLAGS_inputstate);
  machine.run();

  config.full_state->restore_from_string(
      start_frames[absl::GetFlag(FLAGS_inputstate)].snapshot, &machine);

  ui.draw();

  int frame = 0;
  // std::getchar();
  for (const auto& input :
       get_inputs_from_file(absl::GetFlag(FLAGS_inputsfile))) {
    machine.input_state() = input;
    machine.run();
    LOG(INFO) << machine.pc();
    ui.draw();
    ++frame;
    sf::sleep(sf::milliseconds(80));
  }
  machine.input_state() = {};
  machine.set_event_mask(Machine::kEventInterrupt);
  for (int i = 0; i < 1000; ++i) {
    machine.run(50000);
    sf::sleep(sf::milliseconds(80));
    ui.draw();
  }

  return 0;
}
