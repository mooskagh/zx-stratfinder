#include <glog/logging.h>
#include <libspectrum.h>

#include <iostream>
#include <string>

#include "misc/filebuffer.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;
  CHECK_EQ(argc, 2) << "Usage: ./dumprzx <filename>\n";

  std::string contents = read_file_to_string(argv[1]);
  auto* rzx = libspectrum_rzx_alloc();
  CHECK_EQ(libspectrum_rzx_read(
               rzx, reinterpret_cast<libspectrum_byte*>(contents.data()),
               contents.size()),
           0);

  libspectrum_snap* snap;
  CHECK_EQ(libspectrum_rzx_start_playback(rzx, 0, &snap), 0);

  int finished = 0;
  bool was_empty = false;
  do {
    libspectrum_byte val;
    while (libspectrum_rzx_playback(rzx, &val) == 0) {
      std::cout << int(uint8_t(~val)) << " ";
      was_empty = false;
    }
    CHECK_EQ(libspectrum_rzx_playback_frame(rzx, &finished, &snap), 0);
    if (!was_empty) {
      std::cout << "\n";
      was_empty = true;
    }
  } while (!finished);

  libspectrum_rzx_free(rzx);
}
