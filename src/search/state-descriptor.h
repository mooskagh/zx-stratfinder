#pragma once

#include "emulator/machine.h"

class StateDescriptor {
 public:
  virtual ~StateDescriptor() = default;
  template <class T>
  void add(T acc) {
    accessors_.emplace_back(acc.addr(), acc.size());
    total_size_ += acc.size();
  }

  std::string as_string(const Machine& machine) const;
  void restore_from_string(std::string_view str, Machine* machine) const;
  uint64_t as_hash(const Machine& machine);
  void set_include_cpu_state(bool val);
  void set_include_interrupt_counter(bool val);

 private:
  virtual uint64_t get_hash_seed(const Machine& machine) const { return 0; }

  std::vector<RangeAccessor> accessors_;
  size_t total_size_ = 0;
  bool include_cpu_state_ = false;
  bool include_interrupt_counter_ = false;
};