#include "search/search.h"

#include <algorithm>
#include <cstddef>
#include <cstdio>
#include <mutex>
#include <thread>

#include "emulator/machine.h"
#include "misc/config.h"
#include "search/dumper.h"
#include "search/exception.h"
#include "ui/zxscreen.h"

Search::Search(std::unique_ptr<Ui> ui, SearchConfig config)
    : ui_(std::move(ui)), config_(std::move(config)) {
  machines_.resize(config_.num_threads);
  for (auto& machine : machines_) {
    config_.init_func(&machine);
    cur_frames_ = config_.init_states_func(machine, config_);
    if (config_.mode_func) {
      config_.mode_func(&machine.memory(), SearchConfig::INIT);
      config_.mode_func(&machine.memory(), SearchConfig::FAST);
    }
  }
  if (ui_) ui_->set_zx_screen_memory(machines_[0].memory());
}

int Search::run(int how_many_frames, int max_no_minihashes) {
  int frames_with_no_minihashes = 0;
  int frame = 0;
  for (; frame < how_many_frames; ++frame) {
    if (current_best_ &&
        current_best_->score >= config_.scorer->sure_win_score(frame)) {
      // Sure win found.
      break;
    }
    if (cur_frames_.empty()) break;
    LOG(INFO) << "=================================== Starting frame " << frame;
    dump_to_rzx(config_, cur_frames_.front().inputs, "snap",
                "F" + std::to_string(frame), true, true);

    auto start_time = std::chrono::steady_clock::now();
    IterationResult result = do_one_frame(frame);
    result.elapsed_time_us = std::chrono::duration<double, std::micro>(
                                 std::chrono::steady_clock::now() - start_time)
                                 .count();
    print_debug_stats(result);
    if (result.fresh_microhashes == 0) {
      ++frames_with_no_minihashes;
    } else {
      frames_with_no_minihashes = 0;
    }
    if (max_no_minihashes && frames_with_no_minihashes >= max_no_minihashes) {
      LOG(INFO) << "Reached " << frames_with_no_minihashes
                << " frames with no new positions, exiting.";
      break;
    } else {
      LOG(INFO) << "no_new_hash_frames=" << frames_with_no_minihashes;
    }
  }
  if (current_best_) {
    LOG(INFO) << "Win!";
    current_best_->debug.debug_log();
    LOG(INFO) << sequence_as_string(current_best_->inputs);
    dump_to_rzx(config_, current_best_->inputs, "win");
  }
  return frame;
}

uint32_t Search::get_global_microhash_value(uint64_t hash) {
  auto iter = global_microhashes_.find(hash);
  if (iter == global_microhashes_.end()) return 0;
  return iter->second;
}

bool Search::is_duplicate_hash(SetSetId& setset, Set16 item_set) {
  bool res;
  std::tie(res, setset) = set_set_manager_.update(setset, item_set);
  return res;
}

namespace {
void dedup_by_minimal_hash(std::vector<FrameData>* frames) {
  std::sort(frames->begin(), frames->end(), [](const auto& a, const auto& b) {
    if (a.debug.minimal_hash != b.debug.minimal_hash)
      return a.debug.minimal_hash < b.debug.minimal_hash;
    if (a.debug.static_score.score != b.debug.static_score.score) {
      return a.debug.static_score.score > b.debug.static_score.score;
    }
    return a < b;
  });
  const auto before = frames->size();
  frames->erase(std::unique(frames->begin(), frames->end(),
                            [](const auto& a, const auto& b) {
                              return a.debug.minimal_hash ==
                                     b.debug.minimal_hash;
                            }),
                frames->end());
  LOG(INFO) << "Minidedup shrinkage: " << before << "->" << frames->size();
}
}  // namespace

void Search::dedup_by_items(std::vector<FrameData>* frames) {
  std::sort(frames->begin(), frames->end(), [](const auto& a, const auto& b) {
    if (a.debug.full_hash != b.debug.full_hash) {
      return a.debug.full_hash < b.debug.full_hash;
    }
    if (a.debug.item_set.count() != b.debug.item_set.count()) {
      return a.debug.item_set.count() > b.debug.item_set.count();
    }
    return a.debug.total_cpu_cycles < b.debug.total_cpu_cycles;
  });

  uint64_t prev_hash = 0;
  SetSetId setset;
  const auto before = frames->size();
  frames->erase(std::remove_if(frames->begin(), frames->end(),
                               [&](const auto& x) {
                                 if (prev_hash != x.debug.full_hash) {
                                   prev_hash = x.debug.full_hash;
                                   setset.reset();
                                 }
                                 return is_duplicate_hash(setset,
                                                          x.debug.item_set);
                               }),
                frames->end());
  LOG(INFO) << "Items shrinkage: " << before << "->" << frames->size();
}

void Search::do_one_state(int frame_idx, size_t idx, IterationResult& result,
                          Machine& machine) {
  std::lock_guard g(mutex_);
  using SC = SearchConfig;

  if (hook_) hook_->on_new_state(idx);
  const auto& cur = cur_frames_[idx];

  auto actions = config_.actions;
  std::random_shuffle(actions.begin() + 6, actions.end());
  for (const auto& input : actions) {
    config_.full_state->restore_from_string(cur.snapshot, &machine);
    if (config_.valid_state_func &&
        !config_.valid_state_func->is_valid(input, machine, frame_idx,
                                            cur.inputs)) {
      continue;
    }
    machine.input_state() = input;
    if (hook_) hook_->on_load_state(machine, input);
    const bool will_show_ui = ui_ && skip_count_ == 0 && config_.mode_func;
    if (will_show_ui) config_.mode_func(&machine.memory(), SC::DISPLAY);

    mutex_.unlock();
    auto elapsed_ticks = machine.run(config_.ticks_limit);
    mutex_.lock();

    if (elapsed_ticks >= config_.ticks_limit) {
      SearchException exception("Machine reached tick limit. pc=" +
                                std::to_string(machine.pc()));
      exception.inputs() = cur.inputs;
      exception.inputs().push_back(input);
      exception.frame() = cur;
      for (int i = 0; i < 20; ++i) {
        uint16_t addr = machine.get_sp() + i * 2;
        exception.push_stack(machine.memory()[U16Accessor(addr)]);
      }
      throw exception;
    }
    if (will_show_ui) config_.mode_func(&machine.memory(), SC::FAST);
    ++result.frames_ran;
    if (config_.valid_state_func) {
      config_.valid_state_func->here_what_happened(input, machine);
    }
    if (current_best_ && !config_.scorer->can_potentially_win(
                             machine, frame_idx, current_best_->score)) {
      if (hook_) hook_->on_loss(machine);
      continue;
    }

    TempFrameData tmp;
    tmp.static_score =
        config_.scorer->score(machine, frame_idx, cur.inputs, input);
    tmp.dynamic_score = 0.0f;
    tmp.parent_idx = idx;
    tmp.worst_idx = std::max(idx, cur.debug.worst_idx);
    tmp.total_cpu_cycles = cur.debug.total_cpu_cycles + elapsed_ticks;

    if (tmp.static_score.is_loss()) {
      if (hook_) hook_->on_loss(machine);
      continue;
    }
    if (tmp.static_score.is_win()) {
      if (!current_best_ || current_best_->score < tmp.static_score.score) {
        current_best_ = make_new_frame_data(tmp.static_score, cur.inputs, input,
                                            tmp, machine, cur.source_idx);
        LOG(INFO) << "Getting a win candidate with score "
                  << tmp.static_score.score << "( sure win score="
                  << config_.scorer->sure_win_score(frame_idx) << ")";
        current_best_->debug.debug_log();
        LOG(INFO) << "Moves: " << sequence_as_string(current_best_->inputs);
        dump_to_rzx(config_, current_best_->inputs, "candidate",
                    "F" + std::to_string(frame_idx));
      }
      continue;
    }
    if (tmp.static_score.is_candidate()) {
      if (config_.candidate_func) {
        auto inputs = cur.inputs;
        inputs.push_back(input);
        config_.candidate_func(config_, cur.source_idx, inputs, machine);
      }
      continue;
    }
    tmp.full_hash = config_.hash_state->as_hash(machine);
    if (hook_) hook_->on_hash(tmp.full_hash, machine);
    tmp.item_set = config_.itemset_func(machine);
    if (is_duplicate_hash(hashes_[tmp.full_hash], tmp.item_set)) {
      if (hook_) hook_->on_duplicate(tmp.full_hash, machine);
      continue;
    }
    if (config_.minimal_state) {
      tmp.minimal_hash = config_.minimal_state->as_hash(machine);
      tmp.pos_dups = microhashes_[tmp.minimal_hash]++;
      tmp.glob_pos_dups = get_global_microhash_value(tmp.minimal_hash);
      if (tmp.glob_pos_dups == 0) ++result.fresh_microhashes;
      if (config_.local_duplicate_score) {
        tmp.volatile_score += config_.local_duplicate_score(tmp.pos_dups);
      }
      if (config_.global_duplicate_score) {
        tmp.dynamic_score += config_.global_duplicate_score(tmp.glob_pos_dups);
      }
    }
    // machine.memory().dump_bin_to_file("/tmp/mems/step" +
    //                                   std::to_string(frame_idx) + "_" +
    //                                   std::to_string(tmp.minimal_hash) + "_"
    //                                   + std::to_string(tmp.pos_dups) +
    //                                   ".bin");
    const float score =
        tmp.static_score.score + tmp.dynamic_score + tmp.volatile_score;
    if (ui_ && skip_count_-- == 0) {
      skip_count_ = config_.show_every;
      ui_->get_progress_bar().set_val(idx, cur_frames_.size(),
                                      config_.search_width);
      ui_->set_zx_screen_memory(machine.memory());
      ui_->draw();
    }

    ++result.candidates;
    if (int(next_frames_.size()) == config_.search_width) {
      if (score > next_frames_.back().score) {
        if (hook_) hook_->on_eviction(next_frames_.back());
        next_frames_.pop_back();
        next_frames_.push_back(make_new_frame_data(
            score, cur.inputs, input, tmp, machine, cur.source_idx));
        if (hook_) hook_->on_insertion(next_frames_.back(), machine);
        std::push_heap(next_frames_.begin(), next_frames_.end());
        std::pop_heap(next_frames_.begin(), next_frames_.end());
      } else {
        if (hook_) {
          hook_->on_discarding(
              make_new_frame_data(score, cur.inputs, input, tmp, machine,
                                  cur.source_idx),
              machine);
        }
      }
    } else {
      next_frames_.push_back(make_new_frame_data(score, cur.inputs, input, tmp,
                                                 machine, cur.source_idx));
      if (hook_) hook_->on_insertion(next_frames_.back(), machine);
      if (int(next_frames_.size()) == config_.search_width) {
        std::make_heap(next_frames_.begin(), next_frames_.end());
        std::pop_heap(next_frames_.begin(), next_frames_.end());
      }
    }
  }
}

IterationResult Search::do_one_frame(int frame_idx) {
  if (hook_) hook_->on_new_frame(frame_idx);
  next_frames_.clear();
  if (config_.hash_is_per_frame) hashes_.clear();
  microhashes_.clear();
  IterationResult result;
  skip_count_ = 0;

  if (machines_.size() == 1) {
    for (size_t idx = 0; idx < cur_frames_.size(); ++idx) {
      do_one_state(frame_idx, idx, result, machines_[0]);
    }
  } else {
    size_t idx = 0;
    std::mutex idx_mutex;

    std::vector<std::thread> threads;

    for (size_t i = 0; i < machines_.size(); ++i) {
      threads.emplace_back([this, i, frame_idx, &idx, &idx_mutex, &result]() {
        while (true) {
          size_t my_idx = 0;
          {
            std::lock_guard guard(idx_mutex);
            if (idx >= cur_frames_.size()) return;
            my_idx = idx++;
          }
          do_one_state(frame_idx, my_idx, result, machines_[i]);
        }
      });
    }
    while (!threads.empty()) {
      threads.back().join();
      threads.pop_back();
    }
  }

  cur_frames_.clear();
  if (config_.dedup_by_minimal_hash) dedup_by_minimal_hash(&next_frames_);
  if (config_.dedup_by_items) dedup_by_items(&next_frames_);
  // std::sort(next_frames_.begin(), next_frames_.end());
  std::sort(next_frames_.begin(), next_frames_.end(),
            [](const auto& a, const auto& b) {
              if (a.debug.static_score.score != b.debug.static_score.score) {
                return a.debug.static_score.score > b.debug.static_score.score;
              }
              return a < b;
            });

  std::swap(cur_frames_, next_frames_);
  for (const auto& kv : microhashes_) ++global_microhashes_[kv.first];
  return result;
}

FrameData Search::make_new_frame_data(const Score& score,
                                      const std::vector<InputState>& inputs,
                                      InputState new_input,
                                      const TempFrameData& debug,
                                      const Machine& machine, uint16_t source) {
  FrameData result{score.score, config_.full_state->as_string(machine), inputs,
                   debug, source};
  result.inputs.push_back(new_input);
  return result;
}

void Search::print_debug_stats(const IterationResult& res) const {
  LOG(INFO) << "states=" << cur_frames_.size()
            << " candidates=" << res.candidates << " vm_ran=" << res.frames_ran
            << " elapsed=" << (res.elapsed_time_us / 1000) << "ms"
            << " fps="
            << ((res.elapsed_time_us == 0)
                    ? 0.0
                    : (1000000.0 * res.frames_ran / res.elapsed_time_us))
            << " hash_size=" << hashes_.size()
            << " microhash_size=" << microhashes_.size()
            << " glob_microhash_size=" << global_microhashes_.size()
            << " fresh_microhashes=" << res.fresh_microhashes;
  if (!cur_frames_.empty()) {
    LOG(INFO) << "best_score=" << cur_frames_.front().score;
    cur_frames_.front().debug.debug_log();
    LOG(INFO) << "worst_score=" << cur_frames_.back().score;
    cur_frames_.back().debug.debug_log();
  }
}
