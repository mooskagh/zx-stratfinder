#pragma once

#include <functional>
#include <memory>
#include <string>

#include "emulator/machine.h"
#include "misc/config.h"
#include "misc/set-set-manager.h"
#include "search/scorer.h"
#include "search/state-descriptor.h"
#include "search/types.h"

class StateValidator {
 public:
  virtual ~StateValidator() = default;
  virtual bool is_valid(const InputState& state, const Machine& machine,
                        int frame, const std::vector<InputState>& history) = 0;
  virtual void here_what_happened(const InputState& state,
                                  const Machine& machine) {}
};

struct SearchConfig;
std::vector<FrameData> StdInitStates(const Machine& machine,
                                     const SearchConfig& config);

inline Set16 default_item_set(Machine&) { return Set16(0); }

struct SearchConfig {
  using SimpleFunc = std::function<float(float)>;
  using InitFunc = std::function<void(Machine*)>;
  using InitStates = std::function<std::vector<FrameData>(const Machine&,
                                                          const SearchConfig&)>;
  using ItemSet = std::function<Set16(Machine& machine)>;
  using CandidateFunc =
      std::function<void(const SearchConfig&, uint16_t source_idx,
                         std::vector<InputState>&, Machine&)>;

  enum CodeMode { INIT, FAST, DISPLAY };
  using SelectModeFunc = std::function<void(Memory*, CodeMode)>;

  // Game and run names.
  std::string game_name;
  std::string run_name;

  // Full frame save/load rules.
  std::unique_ptr<StateDescriptor> full_state;
  std::unique_ptr<StateDescriptor> hash_state;
  std::unique_ptr<ScorerBase> scorer;
  std::unique_ptr<StateDescriptor> minimal_state;

  // Search core.
  int search_width = 1;
  InitFunc init_func;
  InitStates init_states_func = StdInitStates;
  SelectModeFunc mode_func;
  ItemSet itemset_func = default_item_set;
  CandidateFunc candidate_func;
  std::vector<InputState> actions;

  // Search options.
  bool hash_is_per_frame = false;
  SimpleFunc local_duplicate_score;
  SimpleFunc global_duplicate_score;
  std::unique_ptr<StateValidator> valid_state_func;
  bool dedup_by_minimal_hash = false;
  bool dedup_by_items = false;
  uint32_t show_every = 1000;
  uint32_t ticks_limit = 1000000;
  int num_threads = 1;
};

inline std::vector<FrameData> StdInitStates(const Machine& machine,
                                            const SearchConfig& config) {
  return std::vector<FrameData>{FrameData{0.0f,
                                          config.full_state->as_string(machine),
                                          std::vector<InputState>{},
                                          {}}};
}

class StdModeFunc {
 public:
  StdModeFunc(const std::vector<uint16_t>& addr) : addr_(addr) {}

  void operator()(Memory* memory, SearchConfig::CodeMode mode) {
    switch (mode) {
      case SearchConfig::INIT:
        data_.clear();
        std::for_each(addr_.begin(), addr_.end(), [&](uint64_t addr) {
          data_.push_back(memory->read(addr));
        });
        break;
      case SearchConfig::FAST:
        std::for_each(addr_.begin(), addr_.end(),
                      [&](uint64_t addr) { memory->write(addr, 0); });
        break;
      case SearchConfig::DISPLAY:
        for (size_t i = 0; i < data_.size(); ++i) {
          memory->write(addr_[i], data_[i]);
        }
        break;
    }
  }

 private:
  std::vector<uint16_t> addr_;
  std::vector<uint8_t> data_;
};