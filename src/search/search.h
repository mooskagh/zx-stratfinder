#pragma once

#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>
#include <glog/logging.h>

#include <mutex>

#include "search/search-config.h"
#include "search/search-hook.h"
#include "search/types.h"
#include "ui/ui.h"

struct IterationResult {
  uint32_t candidates = 0;
  uint32_t frames_ran = 0;
  uint32_t fresh_microhashes = 0;
  uint64_t elapsed_time_us = 0;
};

class Search {
 public:
  Search(std::unique_ptr<Ui> ui, SearchConfig config);
  void set_hook(std::unique_ptr<SearchHook> hook) { hook_ = std::move(hook); }
  int run(int how_many_frames, int max_no_minihashes = 0);

 private:
  IterationResult do_one_frame(int frame_idx);
  void do_one_state(int frame_idx, size_t idx, IterationResult& result,
                    Machine& machine);
  FrameData make_new_frame_data(const Score& score,
                                const std::vector<InputState>& inputs,
                                InputState new_input,
                                const TempFrameData& debug,
                                const Machine& machine, uint16_t source);
  uint32_t get_global_microhash_value(uint64_t hash);
  void print_debug_stats(const IterationResult&) const;
  bool is_duplicate_hash(SetSetId& setset, Set16 item_set);
  void dedup_by_items(std::vector<FrameData>* frames);

  std::unique_ptr<Ui> ui_;
  const SearchConfig config_;
  std::vector<FrameData> cur_frames_;
  std::vector<FrameData> next_frames_;
  std::vector<Machine> machines_;
  SetSetManager set_set_manager_;
  absl::flat_hash_map<uint64_t, SetSetId> hashes_;
  absl::flat_hash_map<uint64_t, uint32_t> microhashes_;
  absl::flat_hash_map<uint64_t, uint32_t> global_microhashes_;
  std::unique_ptr<SearchHook> hook_;
  std::optional<FrameData> current_best_;
  int skip_count_;
  std::mutex mutex_;
};