#pragma once
#include <glog/logging.h>

#include "emulator/input.h"
#include "misc/set-set-manager.h"

struct TempFrameData {
  uint64_t full_hash = 0;
  uint64_t minimal_hash = 0;
  uint64_t total_cpu_cycles = 0;
  Score static_score = 0;
  float dynamic_score = 0;
  float volatile_score = 0;
  int pos_dups = 0;
  int glob_pos_dups = 0;
  size_t parent_idx = 0;
  size_t worst_idx = 0;
  Set16 item_set;

  void debug_log() const {
    LOG(INFO) << "S=" << static_score.as_string() << " D=" << dynamic_score
              << " V=" << volatile_score << " LPD=" << pos_dups
              << " GPD=" << glob_pos_dups << " Pidx=" << parent_idx
              << " Widx=" << worst_idx << " items=" << item_set.set
              << " tstates=" << total_cpu_cycles;
  }
};

struct FrameData {
  float score;
  std::string snapshot;
  std::vector<InputState> inputs;
  TempFrameData debug;
  uint16_t source_idx;

  const bool operator<(const FrameData& other) const {
    if (score != other.score) return score > other.score;
    return debug.parent_idx < other.debug.parent_idx;
  }
};
