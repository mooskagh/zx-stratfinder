#pragma once

#include "search/search-config.h"

void dump_to_rzx(const SearchConfig& config,
                 const std::vector<InputState>& states, const std::string& dir,
                 const std::string& suffix = "", bool include_date_dir = false,
                 bool only_dump_text = false);