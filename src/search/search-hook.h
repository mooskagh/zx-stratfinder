#pragma once

#include "emulator/machine.h"

struct FrameData;

class SearchHook {
 public:
  virtual ~SearchHook() = default;

  virtual void on_new_frame(size_t idx) {}
  virtual void on_new_state(size_t idx) {}
  virtual void on_load_state(const Machine& machine, const InputState& input) {}
  virtual void on_loss(const Machine& machine) {}
  virtual void on_hash(uint64_t, const Machine& machine) {}
  virtual void on_duplicate(uint64_t, const Machine& machine) {}
  virtual void on_eviction(const FrameData& frame) {}
  virtual void on_insertion(const FrameData& frame, const Machine& machine) {}
  virtual void on_discarding(const FrameData& frame, const Machine& machine) {}
};