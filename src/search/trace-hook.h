#pragma once

#include "search/search-config.h"
#include "search/search-hook.h"

class TraceHook : public SearchHook {
 public:
  TraceHook(SearchConfig config, const std::vector<InputState>& states);
  void on_new_frame(size_t idx) override;
  void on_new_state(size_t idx) override;
  void on_loss(const Machine& machine) override;
  void on_duplicate(uint64_t, const Machine& machine) override;
  // void on_hash(uint64_t, const Machine& machine) override;
  void on_load_state(const Machine& machine, const InputState& input) override;
  void on_eviction(const FrameData& frame) override;
  void on_insertion(const FrameData& frame, const Machine& machine) override;
  void on_discarding(const FrameData& frame, const Machine& machine) override;

 private:
  void handle_event(const FrameData& frame, const char* op);

  size_t cur_frame_;
  size_t cur_state_;
  std::vector<uint64_t> full_hashes_;
  std::vector<uint64_t> minimal_hashes_;
  SearchConfig config_;
  std::vector<InputState> inputs_;
  bool full_matching_ = false;
  bool mini_matching_ = false;
};
