#include "search/state-descriptor.h"

#include <metrohash64/metrohash64.h>

#include <cstring>

void StateDescriptor::set_include_cpu_state(bool val) {
  if (include_cpu_state_) total_size_ -= sizeof(CpuState);
  include_cpu_state_ = val;
  if (include_cpu_state_) total_size_ += sizeof(CpuState);
}

void StateDescriptor::set_include_interrupt_counter(bool val) {
  if (include_cpu_state_) total_size_ -= sizeof(uint32_t);
  include_interrupt_counter_ = val;
  if (include_cpu_state_) total_size_ += sizeof(uint32_t);
}

std::string StateDescriptor::as_string(const Machine& machine) const {
  std::string res;
  res.reserve(total_size_);
  if (include_cpu_state_) {
    res.resize(sizeof(CpuState));
    machine.save_cpu_state(reinterpret_cast<CpuState*>(res.data()));
  }
  if (include_interrupt_counter_) {
    uint32_t tick = machine.frame_tick();
    res.append(reinterpret_cast<char*>(&tick), sizeof(tick));
  }
  const auto& memory = machine.memory();
  for (const auto& range : accessors_) res += memory[range];
  return res;
}

void StateDescriptor::restore_from_string(std::string_view str,
                                          Machine* machine) const {
  size_t offset = 0;
  if (include_cpu_state_) {
    machine->restore_cpu_state(reinterpret_cast<const CpuState*>(str.data()));
    offset += sizeof(CpuState);
  }
  if (include_interrupt_counter_) {
    uint32_t tick = *reinterpret_cast<const uint32_t*>(str.data() + offset);
    offset += sizeof(tick);
    machine->set_frame_tick(tick);
  }
  auto& memory = machine->memory();
  for (const auto& range : accessors_) {
    std::memcpy(memory.get_range_ptr(range), str.data() + offset, range.size());
    offset += range.size();
  }
}

uint64_t StateDescriptor::as_hash(const Machine& machine) {
  MetroHash64 hash(get_hash_seed(machine));
  const auto& memory = machine.memory();
  for (const auto& range : accessors_) {
    hash.Update(memory.get_range_ptr(range), range.size());
  }
  uint64_t result;
  hash.Finalize(reinterpret_cast<uint8_t*>(&result));
  return result;
}
