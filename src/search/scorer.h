#pragma once

#include <string>

#include "emulator/input.h"
#include "emulator/machine.h"

struct Score {
  enum Result { kLoss, kInProgress, kWin, kCandidate };

  float score = 0.0f;
  Result result = kInProgress;

  bool is_loss() const { return result == kLoss; }
  bool is_win() const { return result == kWin; }
  bool is_candidate() const { return result == kCandidate; }
  static Score Loss(float score = 0.0f) { return {score, kLoss}; }
  static Score Win(float score = 0.0f) { return {score, kWin}; }
  static Score Candidate(float score = 0.0f) { return {score, kCandidate}; }
  Score(float score, Result res = kInProgress) : score(score), result(res){};

  std::string as_string() const {
    std::string base;
    switch (result) {
      case kLoss:
        base = "Loss:";
        break;
      case kInProgress:
        break;
      case kWin:
        base = "Win:";
        break;
      case kCandidate:
        base = "Candidate:";
        break;
    }
    return base + std::to_string(score);
  }
};

class ScorerBase {
 public:
  virtual ~ScorerBase() = default;
  virtual Score score(const Machine& machine, int frame,
                      const std::vector<InputState>& history,
                      const InputState& input) const = 0;
  virtual float sure_win_score(int frame) const { return 0.0f; }
  virtual bool can_potentially_win(const Machine& machine, int frame,
                                   float win_score) {
    return true;
  }
};