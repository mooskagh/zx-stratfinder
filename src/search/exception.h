#pragma once

#include <optional>
#include <stdexcept>
#include <vector>

#include "search/types.h"

class SearchException : public std::runtime_error {
 public:
  SearchException(const std::string& text) : std::runtime_error(text) {}
  std::vector<InputState>& inputs() { return inputs_; }
  const std::vector<InputState>& inputs() const { return inputs_; }

  std::optional<FrameData>& frame() { return frame_; }
  const std::optional<FrameData>& frame() const { return frame_; }

  std::vector<uint16_t> stack() const { return stack_; }
  void push_stack(uint16_t addr) { stack_.push_back(addr); }

 private:
  std::vector<InputState> inputs_;
  std::optional<FrameData> frame_;
  std::vector<uint16_t> stack_;
};
