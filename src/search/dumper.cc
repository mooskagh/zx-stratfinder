#include "search/dumper.h"

#include "misc/config.h"
#include "misc/filebuffer.h"

void dump_to_rzx(const SearchConfig& config,
                 const std::vector<InputState>& states, const std::string& dir,
                 const std::string& suffix, bool include_date_dir,
                 bool only_dump_text) {
  write_string_to_file(get_filename(config.game_name, dir, config.run_name,
                                    suffix, true, "txt", include_date_dir),
                       sequence_as_string(states));
  if (only_dump_text) return;
  Machine machine;

  machine.set_rzx_writer(std::make_unique<RzxWriter>());
  config.init_func(&machine);
  for (const auto& state : states) {
    machine.input_state() = state;
    VLOG(1) << "State: " << state.as_string() << " Ticks: " << machine.run();
  }
  machine.input_state() = {};
  machine.set_event_mask(Machine::kEventInterrupt);
  for (int i = 0; i < 100; ++i) {
    VLOG(3) << "Padding ticks: " << machine.run();
  }
  machine.writer()->write(get_filename(config.game_name, dir, config.run_name,
                                       suffix, true, "rzx", include_date_dir));
}
