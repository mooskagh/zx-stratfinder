#include "search/trace-hook.h"

#include <glog/logging.h>

#include "misc/config.h"
#include "search/search.h"

TraceHook::TraceHook(SearchConfig config, const std::vector<InputState>& states)
    : config_(std::move(config)), inputs_(states) {
  Machine machine;
  config_.init_func(&machine);
  for (const auto& input : states) {
    full_hashes_.push_back(config_.hash_state->as_hash(machine));
    minimal_hashes_.push_back(
        config_.minimal_state ? config_.minimal_state->as_hash(machine) : -1);
    machine.input_state() = input;
    machine.run();
  }
}

void TraceHook::on_new_frame(size_t idx) { cur_frame_ = idx; }
void TraceHook::on_new_state(size_t idx) { cur_state_ = idx; }
void TraceHook::on_load_state(const Machine& machine, const InputState& input) {
  full_matching_ = false;
  mini_matching_ = false;
  if (cur_frame_ >= full_hashes_.size()) return;
  if (input != inputs_[cur_frame_]) return;
  if (config_.hash_state->as_hash(machine) == full_hashes_[cur_frame_]) {
    full_matching_ = true;
    LOG(INFO) << "Matching full hash at idx=" << cur_state_
              << " input=" << machine.input_state().as_string();
  } else if (config_.minimal_state->as_hash(machine) ==
             minimal_hashes_[cur_frame_]) {
    mini_matching_ = true;
    LOG(INFO) << "Matching minimal hash at idx=" << cur_state_
              << " input=" << machine.input_state().as_string();
  }
}
void TraceHook::on_eviction(const FrameData& frame) {
  handle_event(frame, "Evicted");
}
void TraceHook::on_insertion(const FrameData& frame, const Machine& machine) {
  handle_event(frame, "Inserted");
}
void TraceHook::on_discarding(const FrameData& frame, const Machine& machine) {
  handle_event(frame, "Discarded");
}

void TraceHook::handle_event(const FrameData& frame, const char* op) {
  if (cur_frame_ + 1 >= full_hashes_.size()) return;
  if (frame.debug.full_hash == full_hashes_[cur_frame_ + 1]) {
    LOG(INFO) << op << " full hash match with score " << frame.score;
  } else if (frame.debug.minimal_hash == minimal_hashes_[cur_frame_ + 1]) {
    LOG(INFO) << op << " minimal hash match with score " << frame.score;
  } else {
    return;
  }
  frame.debug.debug_log();
}

void TraceHook::on_loss(const Machine& machine) {
  if (!full_matching_ && !mini_matching_) return;
  LOG(INFO) << "Matched state was loss!";
}
void TraceHook::on_duplicate(uint64_t hash, const Machine& machine) {
  if (full_matching_) {
    LOG(INFO) << "FULL Matched state was duplicate! "
              << ((cur_frame_ + 1 >= full_hashes_.size())
                      ? "(replay has finished)"
                  : (full_hashes_[cur_frame_ + 1] == hash)
                      ? "(hash is correct)"
                      : "(hash is INCORRECT)");
  } else if (mini_matching_) {
    auto minihash = config_.minimal_state->as_hash(machine);
    LOG(INFO) << "Matched state was duplicate! "
              << ((cur_frame_ + 1 >= minimal_hashes_.size())
                      ? "(replay has finished)"
                  : (minimal_hashes_[cur_frame_ + 1] == minihash)
                      ? "(minihash is correct)"
                      : "(minihash is INCORRECT)");
    if (cur_frame_ + 1 < full_hashes_.size() &&
        hash == full_hashes_[cur_frame_ + 1]) {
      LOG(INFO) << "And full hash actually also matches!";
    }
  }
}
