#pragma once

#include <cmath>

class Sigmoid {
 public:
  Sigmoid(float x, float y, float x_mid = 0.0f, float y_min = 0.0f,
          float y_max = 1.0f)
      : k_(std::log((1.0f - y) / y) / (x - x_mid)),
        x0_(x_mid),
        y_scale_(std::abs(y_max - y_min)),
        y_offset_(std::min(y_min, y_max)) {}
  float operator()(float x) const {
    return y_scale_ / (1.0f + std::exp(k_ * (x - x0_))) + y_offset_;
  }

 private:
  const float k_;
  const float x0_;
  const float y_scale_;
  const float y_offset_;
};