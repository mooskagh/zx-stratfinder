#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <sys/stat.h>

#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iterator>
#include <limits>
#include <queue>
#include <string>

#include "absl/container/flat_hash_map.h"
#include "journey/lru_cache.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, graph, "", "graph file");
ABSL_FLAG(std::string, estimations, "", "estimations file");

namespace jet_set_willy {
namespace {

struct __attribute__((packed)) PackedNodeKey {
  uint64_t items;
  uint16_t node;

  bool operator==(const PackedNodeKey& other) const {
    return items == other.items && node == other.node;
  }
  template <typename H>
  friend H AbslHashValue(H h, const PackedNodeKey& x) {
    return H::combine(std::move(h), x.items, x.node);
  }
};

struct NodeKey {
  uint16_t node;
  uint64_t items;

  bool operator==(const NodeKey& other) const {
    return items == other.items && node == other.node;
  }
  template <typename H>
  friend H AbslHashValue(H h, const NodeKey& x) {
    return H::combine(std::move(h), x.items, x.node);
  }
};

struct __attribute__((packed)) Bound {
  uint64_t items;
  uint32_t node;
  uint32_t cost;
};

class Search {
 public:
  void LoadGraph(const graph::GraphDef& graph);
  void AddEstimations(uint16_t node,
                      std::vector<std::pair<uint64_t, uint32_t>> est);

  void Run(uint16_t start_node);

 private:
  uint32_t SearchRecursively(uint16_t current_node, uint64_t current_items,
                             uint32_t upper_bound, uint32_t distance_so_far,
                             bool need_solution_at_bound);

  void PrintRoute() const;

  uint32_t fetch_estimation(uint16_t node, uint64_t items);

  struct Edge {
    uint64_t items;
    uint32_t cost;
    uint16_t to_node;
    std::string label;
  };
  std::vector<std::vector<Edge>> edges_;
  struct Estimate {
    uint64_t items;
    uint32_t cost;
  };
  std::vector<std::vector<Estimate>> estimates_;
  FixedLRUCache<PackedNodeKey, uint32_t> cache_{200000000};
  FixedLRUCache<PackedNodeKey, uint32_t> unreacheable_cache_{200000000};

  struct Stats {
    uint64_t nodes = 0;
    uint64_t cache_hit = 0;
    uint64_t cache_miss = 0;
    uint64_t unreachable_cache_hit = 0;
    uint64_t unreachable_cache_miss = 0;
    uint64_t unreachable_cache_useful = 0;
    uint64_t unreachable_cache_useless = 0;
    uint8_t max_item_count = 0;
    std::vector<int> indices;
    std::vector<int> totals;

    void DebugPrint() const {
      LOG(INFO) << "nodes=" << nodes << " cache_hit=" << cache_hit
                << " cache_miss=" << cache_miss
                << " unreachable_cache_hit=" << unreachable_cache_hit
                << " unreachable_cache_miss=" << unreachable_cache_miss
                << " unreachable_cache_useful=" << unreachable_cache_useful
                << " unreachable_cache_useless=" << unreachable_cache_useless
                << " depth=" << totals.size()
                << " max_items=" << int(max_item_count);

      std::string progress;
      for (size_t i = 0; i < totals.size(); ++i) {
        progress +=
            " " + std::to_string(indices[i]) + "/" + std::to_string(totals[i]);
      }
      if (!progress.empty()) LOG(INFO) << "Progress:" << progress;
    }
  };
  Stats stats_;

  std::vector<uint16_t> end_node_;
  std::vector<uint16_t> path_;
  absl::flat_hash_set<NodeKey> visited_nodes_;
  uint16_t start_node_;
  absl::flat_hash_map<NodeKey, std::vector<uint32_t>> reachable_stacks_;
};

void Search::PrintRoute() const {
  LOG(INFO) << "====== Found route! length=" << path_.size();
  uint64_t total_cost = 0;
  std::string route;
  uint16_t node = start_node_;
  for (const auto& x : path_) {
    const auto& edge = edges_[node][x];
    total_cost += edge.cost;
    route += " " + edge.label;
    node = edge.to_node;
  }
  LOG(INFO) << "Path:" << route;
  LOG(INFO) << "TOTAL COST: " << total_cost;
}

void Search::Run(uint16_t start_node) {
  start_node_ = start_node;
  LOG(INFO) << "Starting search";
  auto result = SearchRecursively(start_node, 0, 4000000000, 0, false);
  LOG(INFO) << "Best solution: " << result;
  stats_.DebugPrint();
  LOG(INFO) << "Ended search";
}

uint32_t Search::SearchRecursively(uint16_t current_node,
                                   uint64_t current_items,
                                   uint32_t current_budget,
                                   uint32_t distance_so_far,
                                   bool need_solution_at_bound) {
  stats_.max_item_count =
      std::max(stats_.max_item_count,
               static_cast<uint8_t>(__builtin_popcountll(current_items)));
  visited_nodes_.insert(NodeKey{current_node, current_items});

  if (current_items == 0xFFFFFFFFFFFFFFFFull &&
      std::find(end_node_.begin(), end_node_.end(), current_node) !=
          end_node_.end()) {
    PrintRoute();
    return 0;
  }

  if (stats_.nodes++ % 65536 == 0) {
    stats_.DebugPrint();
    LOG(INFO) << "Items: " << std::bitset<64>(current_items)
              << " count=" << __builtin_popcountll(current_items)
              << " node=" << current_node << " budget=" << current_budget;
    LOG(INFO) << "cache=" << cache_.size()
              << " unreacheable_cache=" << unreacheable_cache_.size();
  }

  const auto& edges = edges_[current_node];

  struct Candidate {
    uint16_t edge_idx;
    uint16_t to_node;
    uint32_t lower_bound;
    uint32_t edge_cost;
    uint64_t items;
    bool is_exact;

    void DebugPrint() const {
      LOG(INFO) << "edge_idx=" << edge_idx << " to_node=" << to_node
                << " lower_bound=" << lower_bound << " edge_cost=" << edge_cost
                << " items=" << std::bitset<64>(items)
                << " is_exact=" << is_exact;
    }
  };
  std::vector<Candidate> candidates;
  candidates.reserve(edges.size());

  //  LOG(INFO) << "===== DEPTH: " << path_.size();
  uint16_t idx = 0;
  for (const auto& edge : edges) {
    Candidate candidate;
    candidate.edge_idx = idx++;
    if (edge.cost > current_budget) continue;

    candidate.to_node = edge.to_node;
    candidate.items = current_items | edge.items;

    auto iter = reachable_stacks_.find(NodeKey{edge.to_node, candidate.items});
    if (iter != reachable_stacks_.end() &&
        edge.cost + distance_so_far >= iter->second.back()) {
      continue;
    }

    candidate.edge_cost = edge.cost;
    auto remaining_budget = current_budget - edge.cost;
    auto cache_item =
        cache_.get(PackedNodeKey{candidate.items, candidate.to_node});
    if (cache_item) {
      ++stats_.cache_hit;
      candidate.lower_bound = *cache_item;
      candidate.is_exact = true;
    } else {
      ++stats_.cache_miss;
      auto unreacheable_cache_item = unreacheable_cache_.get(
          PackedNodeKey{candidate.items, candidate.to_node});
      if (unreacheable_cache_item) {
        ++stats_.unreachable_cache_hit;
        if (*unreacheable_cache_item <= remaining_budget) {
          ++stats_.unreachable_cache_useful;
          continue;
        } else {
          ++stats_.unreachable_cache_useless;
        }
      } else {
        ++stats_.unreachable_cache_miss;
      }
      candidate.lower_bound =
          fetch_estimation(candidate.to_node, ~candidate.items);
      candidate.is_exact = false;
    }
    if (candidate.lower_bound > remaining_budget ||
        (candidate.lower_bound == remaining_budget &&
         !need_solution_at_bound)) {
      // LOG(INFO) << "REJECTED " <<
      // edges_[current_node][candidate.edge_idx].label
      //           << " rem_budget=" << remaining_budget;
      // candidate.DebugPrint();
      continue;
    }
    // LOG(INFO) << "accepted " <<
    // edges_[current_node][candidate.edge_idx].label
    //           << " rem_budget=" << remaining_budget;
    // candidate.DebugPrint();

    reachable_stacks_[NodeKey{edge.to_node, candidate.items}].push_back(
        edge.cost + distance_so_far);
    candidates.push_back(candidate);
  }

  std::sort(
      candidates.begin(), candidates.end(), [](const auto& a, const auto& b) {
        if (__builtin_popcountll(a.items) != __builtin_popcountll(b.items)) {
          return __builtin_popcountll(a.items) > __builtin_popcountll(b.items);
        }
        return a.edge_cost + a.lower_bound < b.edge_cost + b.lower_bound;
      });

  uint32_t best_eval = std::numeric_limits<uint32_t>::max();
  stats_.totals.push_back(candidates.size());
  stats_.indices.push_back(-1);
  for (const auto& candidate : candidates) {
    // candidate.DebugPrint();
    ++stats_.indices.back();
    if (candidate.edge_cost >= best_eval) continue;
    path_.push_back(candidate.edge_idx);

    bool need_exact = need_solution_at_bound;
    auto remaining_budget = current_budget - candidate.edge_cost;
    if (!need_exact && candidate.is_exact &&
        candidate.lower_bound >= remaining_budget) {
      LOG(INFO) << "Found a solution of cost="
                << candidate.lower_bound + candidate.edge_cost +
                       remaining_budget + distance_so_far
                << "=" << candidate.lower_bound << "+" << candidate.edge_cost
                << "+" << remaining_budget << "+" << distance_so_far
                << ". Trying to restore the route.";
      need_exact = true;
    }

    if (candidate.lower_bound <= remaining_budget &&
        (candidate.lower_bound != remaining_budget || need_exact) &&
        !visited_nodes_.contains(NodeKey{candidate.to_node, candidate.items})) {
      auto cur_eval = SearchRecursively(
          candidate.to_node, candidate.items, remaining_budget,
          distance_so_far + candidate.edge_cost, need_exact);
      // LOG(INFO) << "Inserting " << candidate.items << " " <<
      // candidate.to_node
      //           << " " << cur_eval;
      if (cur_eval == std::numeric_limits<uint32_t>::max()) {
        unreacheable_cache_.insert(
            PackedNodeKey{candidate.items, candidate.to_node},
            remaining_budget);
      } else {
        cache_.insert(PackedNodeKey{candidate.items, candidate.to_node},
                      cur_eval);
      }
      if (best_eval - candidate.edge_cost > cur_eval) {
        best_eval = cur_eval + candidate.edge_cost;
      }
      if (cur_eval < remaining_budget) {
        current_budget -= remaining_budget - cur_eval;
      }
      // if (cur_eval == 0) {
      //   LOG(INFO) << current_budget << " " << best_eval;
      // }
    }
    path_.pop_back();
  }
  stats_.totals.pop_back();
  stats_.indices.pop_back();
  for (const auto& candidate : candidates) {
    auto iter =
        reachable_stacks_.find(NodeKey{candidate.to_node, candidate.items});
    iter->second.pop_back();
    if (iter->second.empty()) reachable_stacks_.erase(iter);
  }

  visited_nodes_.erase(NodeKey{current_node, current_items});
  return best_eval;
}

uint32_t Search::fetch_estimation(uint16_t node, uint64_t items) {
  for (const auto& x : estimates_[node]) {
    if ((x.items & items) == x.items) return x.cost;
  }
  return 0;
}

void Search::LoadGraph(const graph::GraphDef& graph) {
  edges_.resize(graph.node_size());
  estimates_.resize(graph.node_size());
  for (const auto& edge : graph.edge()) {
    edges_[edge.from_node()].push_back(
        Edge{edge.items(), edge.cost(), static_cast<uint16_t>(edge.to_node()),
             edge.label()});
  }
  for (const auto x : graph.end_node()) end_node_.push_back(x);
}

void Search::AddEstimations(uint16_t node,
                            std::vector<std::pair<uint64_t, uint32_t>> est) {
  auto& estimates = estimates_.at(node);
  std::transform(est.begin(), est.end(), std::back_inserter(estimates),
                 [](const std::pair<uint64_t, uint32_t>& b) {
                   return Estimate{b.first, b.second};
                 });
  std::sort(estimates.begin(), estimates.end(),
            [](const auto& a, const auto& b) { return a.cost > b.cost; });
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_graph)));
  Search search;
  search.LoadGraph(graph);

  {
    LOG(INFO) << "Loading estimations";
    FILE* file = fopen(absl::GetFlag(FLAGS_estimations).c_str(), "rb");
    CHECK(file != nullptr);
    Bound bound;
    uint32_t last_node_id = 0;
    std::vector<std::pair<uint64_t, uint32_t>> est;
    while (fread(&bound, sizeof(bound), 1, file)) {
      if (last_node_id != bound.node) {
        LOG(INFO) << "Node " << last_node_id;
        search.AddEstimations(last_node_id, std::move(est));
        est.clear();
        last_node_id = bound.node;
      }
      uint64_t items = bound.items;
      uint64_t cost = bound.cost;
      est.push_back(std::pair<uint64_t, uint32_t>(items, cost));
    }
    search.AddEstimations(last_node_id, std::move(est));
    fclose(file);
    LOG(INFO) << "Loaded estimations";
  }
  search.Run(graph.start_node(0));
}
}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
