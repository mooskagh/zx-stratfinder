#pragma once

#include <fcntl.h>
#include <glog/logging.h>
#include <sys/stat.h>

#include <vector>

namespace graph {

template <typename T>
class Dumper {
 public:
  Dumper(const std::string& filename) {
    buffer_.reserve(1000000000 / sizeof(T));
    fileno_ = open(filename.c_str(), O_RDWR | O_CREAT | O_TRUNC,
                   S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    CHECK_NE(fileno_, -1) << errno;
  }
  ~Dumper() { flush(); }
  void flush() {
    if (buffer_.empty()) return;
    const size_t bytes_to_write = sizeof(T) * buffer_.size();
    LOG(INFO) << "Flushing " << bytes_to_write << " bytes.";
    CHECK_EQ(bytes_to_write, write(fileno_, buffer_.data(), bytes_to_write));
    buffer_.clear();
  }
  void add(const T& val) {
    if (buffer_.size() == buffer_.capacity()) flush();
    buffer_.push_back(val);
  }

  T get(size_t idx) {
    flush();
    CHECK_NE(lseek(fileno_, idx * sizeof(T), SEEK_SET), -1) << errno;
    T res;
    CHECK_NE(read(fileno_, &res, sizeof(res)), -1) << errno;
    CHECK_NE(lseek(fileno_, 0, SEEK_END), -1) << errno;
    return res;
  }

 private:
  std::vector<T> buffer_;
  int fileno_;
};

}  // namespace graph