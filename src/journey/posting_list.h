#pragma once

#include <cstdint>
#include <functional>
#include <memory>
#include <queue>
#include <set>
#include <vector>

namespace posting_list {

class IIterator {
 public:
  virtual ~IIterator() = default;
  virtual uint32_t get() const = 0;
  virtual bool done() const = 0;
  virtual bool next() = 0;
  virtual bool skip_to(uint32_t) = 0;
  virtual bool seek_to(uint32_t) = 0;
  virtual void kill() = 0;
  virtual void insert(uint32_t) = 0;
  virtual void update(uint32_t) = 0;
};

class PostingList;

class PostingListIterator : public IIterator {
 public:
  PostingListIterator(PostingList* pl);
  ~PostingListIterator();

  bool done() const override;
  uint32_t get() const override;
  bool next() override;
  bool skip_to(uint32_t) override;
  bool seek_to(uint32_t) override;
  void kill() override;
  void insert(uint32_t) override;
  void update(uint32_t) override;

 private:
  void commit();

  std::vector<uint32_t>* const docs_;
  std::vector<uint32_t>::iterator write_iter_;
  std::vector<uint32_t>::iterator read_iter_;
};

class PostingList {
 public:
  size_t size() const { return documents_.size(); }
  void add(uint32_t val) { documents_.push_back(val); }
  std::unique_ptr<PostingListIterator> iter() {
    return std::make_unique<PostingListIterator>(this);
  }

 private:
  std::vector<uint32_t> documents_;
  friend PostingListIterator::PostingListIterator(PostingList*);
};

class IntersectionIterator : public IIterator {
 public:
  IntersectionIterator(std::vector<std::unique_ptr<IIterator>> iters);

  bool done() const override { return done_; }
  uint32_t get() const override { return cur_; }
  bool next() override {
    if (iters_.back()->next()) {
      cur_ = iters_.back()->get();
      done_ = !sync();
    } else {
      done_ = true;
    }
    return !done_;
  }
  bool skip_to(uint32_t val) override {
    cur_ = val;
    for (auto& it : iters_) it->skip_to(val);
    done_ = !sync();
    return !done_ && cur_ == val;
  }
  bool seek_to(uint32_t val) override {
    cur_ = val;
    for (auto& it : iters_) it->seek_to(val);
    done_ = !sync();
    return !done_ && cur_ == val;
  }
  void kill() override {
    for (auto& it : iters_) it->kill();
    done_ = !sync();
  }
  void insert(uint32_t val) override {
    cur_ = val;
    for (auto& it : iters_) it->insert(val);
  }
  void update(uint32_t val) override {
    cur_ = val;
    for (auto& it : iters_) it->update(val);
  }

 private:
  bool sync();

  std::vector<std::unique_ptr<IIterator>> iters_;
  bool done_ = false;
  uint32_t cur_ = 0;
};

class UnionIterator : public IIterator {
 public:
  UnionIterator(std::vector<std::unique_ptr<IIterator>> iters);

  bool done() const override { return next_.empty(); }
  uint32_t get() const override { return next_.top().val; }
  bool next() override;
  bool skip_to(uint32_t val) override {
    for (auto& it : iters_) it->skip_to(val);
    populate_next();
    return !done() && get() == val;
  }
  bool seek_to(uint32_t val) override {
    for (auto& it : iters_) it->seek_to(val);
    populate_next();
    return !done() && get() == val;
  }
  void kill() override {
    for (auto& it : iters_) {
      if (!it->done() && it->get() == get()) it->kill();
    }
    populate_next();
  }
  void insert(uint32_t val) override {
    for (auto& it : iters_) it->insert(val);
    populate_next();
  }
  void update(uint32_t val) override {
    for (auto& it : iters_) {
      if (!it->done() && it->get() == get()) it->update(val);
    }
  }

 private:
  void populate_next();

  std::vector<std::unique_ptr<IIterator>> iters_;
  bool done_ = false;
  struct IterPtr {
    uint32_t val;
    uint32_t idx;
    bool operator<(const IterPtr& other) const { return val > other.val; }
  };

  std::priority_queue<IterPtr> next_;
};

}  // namespace posting_list