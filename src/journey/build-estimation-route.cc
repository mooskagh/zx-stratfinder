#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <bitset>
#include <cstddef>
#include <cstdint>
#include <ios>
#include <queue>
#include <utility>

#include "absl/container/flat_hash_map.h"
#include "journey/bounds_estimator.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, graph, "", "graph");
ABSL_FLAG(std::string, estimations, "", "estimations");
ABSL_FLAG(uint64_t, items, 0xFFFFFFFFFFFFFFFFull, "items");
ABSL_FLAG(int, start_node, -1, "start_node");

namespace jet_set_willy {
namespace {

uint64_t TranslateItems(uint64_t items) {
  uint64_t res = 0;
if (items & 0x3c00ULL) res |= 0x1ULL;
if (items & 0xfULL) res |= 0x2ULL;
if (items & 0xe01c0ULL) res |= 0x4ULL;
if (items & 0x40000000001c000ULL) res |= 0x8ULL;
if (items & 0x8000000ULL) res |= 0x10ULL;
if (items & 0x1040000000ULL) res |= 0x20ULL;
if (items & 0x80000000000ULL) res |= 0x40ULL;
if (items & 0xc00000000000ULL) res |= 0x80ULL;
if (items & 0x1040000000000ULL) res |= 0x100ULL;
if (items & 0x3e2000800000000ULL) res |= 0x200ULL;
if (items & 0x800000000000000ULL) res |= 0x400ULL;
if (items & 0x1000000000000000ULL) res |= 0x800ULL;
if (items & 0x780000000ULL) res |= 0x1000ULL;
if (items & 0x1c000000000000ULL) res |= 0x2000ULL;
if (items & 0x100000ULL) res |= 0x4000ULL;
if (items & 0x2000000000000000ULL) res |= 0x8000ULL;
if (items & 0x200000ULL) res |= 0x10000ULL;
if (items & 0x30ULL) res |= 0x20000ULL;
if (items & 0x33e030000000ULL) res |= 0x40000ULL;
if (items & 0x7800000ULL) res |= 0x80000ULL;
if (items & 0x4000000000000000ULL) res |= 0x100000ULL;
if (items & 0x8000000000400200ULL) res |= 0x200000ULL;  
  return res;
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_graph)));
  graph::BoundsEstimator estimator(absl::GetFlag(FLAGS_estimations));

  std::vector<std::vector<graph::Edge>> edges(graph.node_size());
  for (auto edge : graph.edge()) {
    edge.set_items(TranslateItems(edge.items()));
    edges[edge.from_node()].push_back(edge);
  }

  int cur_node = absl::GetFlag(FLAGS_start_node);
  if (cur_node == -1) cur_node = graph.start_node(0);

  uint32_t cost = 0;
  uint64_t items = absl::GetFlag(FLAGS_items);

  /// uint32_t target_cost = estimator.GetCost(cur_node, items);

  while (true) {
    std::optional<graph::Edge> best;
    uint32_t best_cost_est = 0;
    for (const auto& candidate : edges[cur_node]) {
      auto new_cost = cost + candidate.cost();
      auto new_items = items & ~candidate.items();
      auto new_estimate = estimator.GetCost(candidate.to_node(), new_items);
      // LOG(INFO) << candidate.items() << " " << new_estimate << " " <<
      // new_items;
      if (!best || best_cost_est > new_cost + new_estimate) {
        best = candidate;
        best_cost_est = new_cost + new_estimate;
        continue;
      }
    }
    {
      auto new_cost = cost + best->cost();
      auto new_items = items & ~best->items();
      auto new_estimate = estimator.GetCost(best->to_node(), new_items);

      // LOG(INFO) << best->items();
      // LOG(INFO) << "target=" << target_cost << " new_cost=" << new_cost
      //           << " new_estimate=" << new_estimate
      //           << " candidate=" << best->label()
      //           << " cost+est=" << new_cost + new_estimate
      //           << " items=" << std::bitset<64>(new_items);
      std::cout << best->label() << " ";  // std::endl;
      cost = new_cost;
      items = new_items;
      cur_node = best->to_node();

      if (new_estimate == 0) break;
    }
  }
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
