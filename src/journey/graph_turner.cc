#include "journey/graph_turner.h"

#include <absl/flags/flag.h>
#include <bits/stdint-uintn.h>
#include <glog/logging.h>

ABSL_FLAG(std::string, dumpfile,
          "/home/crem/my/games/jsw-2022/tmp/journey.dump", "dump file");

ABSL_FLAG(uint32_t, width, 1000000, "eviction_width");
ABSL_FLAG(uint32_t, max_set, 50000000, "");

namespace graph {

GraphTurner::GraphTurner(const GraphDef& graph,
                         const BoundsEstimator& estimator)
    : end_items_(graph.end_items()),  // 9223372036854775808ull
      passed_states_(256),
      dumper_(absl::GetFlag(FLAGS_dumpfile)),
      bounds_estimator_(estimator) {
  for (const auto& node : graph.node()) node_labels_.push_back(node.label());

  edges_.resize(node_labels_.size());
  for (const auto& edge : graph.edge()) {
    CHECK_LT(edge.from_node(), edges_.size());
    edges_[edge.from_node()].push_back(NodeEdge{
        edge.to_node(), edge.cost(), ItemSet(edge.items()), edge.label()});
    // CHECK_EQ(edges_[edge.from_node()].size() - 1, edge.index_in_from_node());
  }
  for (const auto& node : graph.end_node()) end_nodes_.insert(node);

  for (const auto& node : graph.start_node()) {
    insert(node, ItemSet(graph.start_items()), 0, 0, 0);
  }
}

void GraphTurner::insert(uint16_t state, ItemSet items, uint64_t cost,
                         uint64_t prev, uint16_t edge) {
  const auto score = items.score();
  if (score < min_score_) return;
  const auto estimation = bounds_estimator_.GetCost(state, ~items.raw());
  if (passed_states_[score].contains(StateKey{state, items})) return;
  if (!minimums_.update_cost(state, items, cost + estimation)) return;
  ++stats_.queue_length[score];
  ++stats_.total_enqueued[score];
  queue_.push(QueueItem(cost, estimation, state, items, prev, edge));
}

namespace {
template <typename T>
std::string vector_to_string(const T& v, std::string delim = ", ",
                             bool add_index = false, int split_by = 52) {
  std::string result;
  int idx = 0;
  for (const auto& x : v) {
    if (split_by && idx % split_by == 0) {
      result += "\n" + std::to_string(idx) + ":   ";
    } else if (!result.empty()) {
      result += delim;
    }
    if (add_index) result += std::to_string(idx) + ":";
    result += std::to_string(x);
    idx++;
  }
  return result;
}
}  // namespace

void GraphTurner::write_log() const {
  LOG(INFO) << "==================================================";
  {
    std::vector<size_t> sizes;
    std::vector<size_t> capacities;
    for (const auto& x : passed_states_) {
      sizes.push_back(x.size());
      capacities.push_back(x.capacity());
    }
    LOG(INFO) << "passes_states_sizes = [" << vector_to_string(sizes) << "]";
    LOG(INFO) << "passes_states_capacities = [" << vector_to_string(capacities)
              << "]";
  }
  LOG(INFO) << "steps=" << stats_.steps << " fetches=" << stats_.fetches
            << " dups=" << stats_.duplicate_states << " QQQQ=" << queue_.size()
            << " TOP=" << queue_.top().debug_string();
  LOG(INFO) << "Total enqueued=[" << vector_to_string(stats_.total_enqueued)
            << "]";
  LOG(INFO) << "Queue sizes=[" << vector_to_string(stats_.queue_length) << "]";
  LOG(INFO) << "hash_size=" << minimums_.size()
            << " hash_overhead=" << minimums_.overhead();
  LOG(INFO) << "to_discard=" << stats_.queue_to_discard
            << " effective=" << queue_.size() - stats_.queue_to_discard
            << " discard_score=" << min_score_;
}

void GraphTurner::run() {
  while (!queue_.empty()) {
    if (queue_.size() - stats_.queue_to_discard > absl::GetFlag(FLAGS_width) ||
        passed_states_[min_score_].size() > absl::GetFlag(FLAGS_max_set)) {
      write_log();
      evict_laggers();
    } else if (stats_.steps % 1000000 == 0) {
      write_log();
      evict_passed();
    }
    if (do_one_step()) break;
    // if (stats_.steps >= 5000000) break;
  }
}

bool GraphTurner::do_one_step() {
  ++stats_.steps;
  QueueItem item = queue_.top();
  queue_.pop();
  auto key = item.key();
  const auto score = key.items.score();
  --stats_.queue_length[score];
  const bool is_duplicate =
      score >= min_score_ && !passed_states_[score].insert(key).second;
  if (score < min_score_) {
    --stats_.queue_to_discard;
  }
  if (is_duplicate) {
    ++stats_.duplicate_states;
    return false;
  }
  dumper_.add(item);
  if (key.items.is_superset_of(end_items_) && end_nodes_.contains(key.state)) {
    found(stats_.fetches);
    return true;
  }
  int edge_idx = 0;
  for (const auto& edge : edges_[key.state]) {
    insert(edge.to_node, ItemSet::combine(key.items, edge.items),
           item.cost() + edge.cost, stats_.fetches, edge_idx++);
  }

  ++stats_.fetches;
  return false;
};

void GraphTurner::found(size_t idx) {
  std::vector<QueueItem> frames;

  while (true) {
    auto frame = dumper_.get(idx);
    frames.push_back(frame);
    LOG(INFO) << "Recovered: idx=" << idx << " " << frame.debug_string(true);
    if (idx == 0) break;
    idx = frame.prev();
  };
  std::reverse(frames.begin(), frames.end());
  LOG(INFO) << "End state: " << node_labels_[frames.back().state()];
  std::string route;
  std::string player;
  for (size_t i = 0; i < frames.size() - 1; ++i) {
    if (!route.empty()) route += " ";
    if (!player.empty()) player += " ";
    LOG(INFO)
        << "[" << node_labels_[frames[i].state()] << "]  "
        << edges_[frames[i].state()][frames[i + 1].edge()].label
        << " cost=" << frames[i + 1].cost() - frames[i].cost() << " +items="
        << (frames[i + 1].key().items - frames[i].key().items).debug_string();
    route += edges_[frames[i].state()][frames[i + 1].edge()].label;
    player += "route:\"" +
              edges_[frames[i].state()][frames[i + 1].edge()].label + "\"";
  }
  LOG(INFO) << route;
  LOG(INFO) << "play{" << player << "}";
  LOG(INFO) << "Final cost: " << frames.back().cost();
}

void GraphTurner::evict_laggers() {
  if (min_score_ >= 255) return;
  LOG(INFO) << "ITEM COUNT EVICTION of score=" << min_score_
            << " at cost=" << queue_.top().cost_with_estimation();
  stats_.queue_to_discard += stats_.queue_length[min_score_];
  LOG(INFO) << "passed_states=" << passed_states_[min_score_].size();
  passed_states_[min_score_] = {};
  ++min_score_;
  LOG(INFO) << "Total evicted=" << minimums_.evict_worse_than(min_score_);
}

void GraphTurner::evict_passed() {
  LOG(INFO) << "By cost evicted="
            << minimums_.evict_outdated(queue_.top().cost_with_estimation());
}

}  // namespace graph
