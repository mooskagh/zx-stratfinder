#include "journey/bounds_estimator.h"

#include <glog/logging.h>

#include <algorithm>

namespace graph {

namespace {
struct __attribute__((packed)) Bound {
  uint64_t items;
  uint32_t node;
  uint32_t cost;
};
}  // namespace

BoundsEstimator::BoundsEstimator(const std::string& filename) {
  LOG(INFO) << "Loading estimations";
  if (filename.empty()) return;
  FILE* file = fopen(filename.c_str(), "r");
  CHECK(file) << filename;

  std::vector<Bound> buffer(1000000);

  while (auto records_read =
             fread(&buffer[0], sizeof(Bound), buffer.size(), file)) {
    for (size_t i = 0; i < records_read; ++i) {
      const auto& entry = buffer[i];
      if (node_to_estimations_.size() <= entry.node) {
        node_to_estimations_.resize(entry.node + 1);
      }
      node_to_estimations_[entry.node].push_back(
          Estimation{entry.items, entry.cost});
    }
  }
  fclose(file);

  LOG(INFO) << "Sorting estimations";
  for (auto& estimations : node_to_estimations_) {
    std::sort(estimations.begin(), estimations.end(),
              [](const auto& a, const auto& b) { return a.cost > b.cost; });
  }
  LOG(INFO) << "Estimations loaded";
}

}  // namespace graph