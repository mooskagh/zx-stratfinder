#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace graph {

class BoundsEstimator {
 public:
  BoundsEstimator(const std::string& filename);
  uint32_t GetCost(uint16_t node, uint64_t mask) const {
    if (node_to_estimations_.size() <= node) return 0;
    for (const auto& x : node_to_estimations_[node]) {
      if ((mask & x.mask) == x.mask) return x.cost;
    }
    return 0;
  }

 private:
  struct Estimation {
    uint64_t mask;
    uint32_t cost;
  };
  using Estimations = std::vector<Estimation>;
  std::vector<Estimations> node_to_estimations_;
};

}  // namespace graph