#pragma once

#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>

#include <cstdint>
#include <queue>
#include <string>
#include <vector>

#include "journey/bounds_estimator.h"
#include "journey/dumper.h"
#include "journey/items-min.h"
#include "proto/graph.pb.h"

namespace graph {

struct StateKey {
  uint16_t state;
  ItemSet items;

  bool operator==(const StateKey& o) const {
    return state == o.state && items == o.items;
  }

  template <typename H>
  friend H AbslHashValue(H s, const StateKey& o) {
    return H::combine(std::move(s), o.state, o.items);
  }
};

struct Stats {
  uint64_t steps = 0;
  uint64_t fetches = 0;
  uint64_t duplicate_states = 0;
  std::array<uint64_t, 256> total_enqueued{};
  std::array<uint32_t, 256> queue_length{};
  uint32_t queue_to_discard = 0;
};

class GraphTurner {
 public:
  GraphTurner(const GraphDef& graph, const BoundsEstimator& estimator);
  void run();

 private:
  bool do_one_step();
  void found(size_t idx);
  void evict_laggers();
  void evict_passed();

  static uint64_t hash_state(uint16_t state, ItemSet items) {
    return absl::Hash<StateKey>{}(StateKey{state, items});
  }
  void insert(uint16_t state, ItemSet items, uint64_t cost, uint64_t prev,
              uint16_t edge);
  void write_log() const;

  std::vector<std::string> node_labels_;

  struct NodeEdge {
    uint32_t to_node;
    uint32_t cost;
    ItemSet items;
    std::string label;

    std::string debug_string() const {
      return "[to=" + std::to_string(to_node) +
             ",cost=" + std::to_string(cost) +
             ",items=" + items.debug_string() + ",label=" + label + "]";
    }
  };
  using NodeEdges = std::vector<NodeEdge>;
  std::vector<NodeEdges> edges_;

  const ItemSet end_items_;
  absl::flat_hash_set<uint16_t> end_nodes_;

  struct QueueItem {
    QueueItem() = default;
    QueueItem(uint64_t cost, uint32_t estimation, uint16_t state, ItemSet items,
              uint64_t prev, uint16_t edge)
        : cost_and_state_((cost << 16) | state),
          items_{items},
          prev_and_edge_{(prev << 16) | edge},
          estimation_plus_cost_(cost + estimation) {}

    StateKey key() const { return {state(), items_}; }
    uint32_t cost() const { return cost_and_state_ >> 16; }
    uint16_t state() const { return uint16_t(cost_and_state_); }
    size_t prev() const { return prev_and_edge_ >> 16; }
    uint16_t edge() const { return prev_and_edge_ & 0xffff; }
    uint64_t estimation() const { return estimation_plus_cost_ - cost(); }
    uint64_t cost_with_estimation() const { return estimation_plus_cost_; }

    bool operator<(const QueueItem& other) const {
      return estimation_plus_cost_ > other.estimation_plus_cost_;
    }

    std::string debug_string(bool full = false) const {
      auto res = "cost=" + std::to_string(cost()) +
                 ",estimation=" + std::to_string(estimation()) +
                 ",cost+estimation=" + std::to_string(estimation_plus_cost_) +
                 ",state=" + std::to_string(key().state) +
                 ",i=" + items_.debug_string() +
                 ",ic=" + std::to_string(items_.count()) +
                 ",is=" + std::to_string(items_.score());
      if (full) {
        res += ",prev=" + std::to_string(prev()) +
               ",edge=" + std::to_string(edge());
      }
      return "[" + res + "]";
    }

   private:
    uint64_t cost_and_state_;
    ItemSet items_;
    uint64_t prev_and_edge_;  // Uppper 48 bits is prev, lower 16 is edge.
    uint64_t estimation_plus_cost_;
  };
  std::priority_queue<QueueItem> queue_;
  ItemsToMimimum minimums_;
  std::vector<absl::flat_hash_set<StateKey>> passed_states_;
  Stats stats_;
  Dumper<QueueItem> dumper_;
  int min_score_ = 0;
  const BoundsEstimator& bounds_estimator_;
};

}  // namespace graph