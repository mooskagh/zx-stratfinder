#include "journey/posting_list.h"

#include <glog/logging.h>

namespace posting_list {

namespace {
constexpr int kBinarySearchThreshold = 16;
}

PostingListIterator::PostingListIterator(PostingList* pl)
    : docs_(&pl->documents_),
      write_iter_(docs_->begin()),
      read_iter_(docs_->begin()) {}

PostingListIterator::~PostingListIterator() { commit(); }

void PostingListIterator::commit() {
  if (read_iter_ != write_iter_) docs_->erase(write_iter_, read_iter_);
}

uint32_t PostingListIterator::get() const { return *read_iter_; }
bool PostingListIterator::done() const { return read_iter_ == docs_->end(); }
bool PostingListIterator::next() {
  if (read_iter_ != write_iter_) *write_iter_ = *read_iter_;
  ++write_iter_;
  ++read_iter_;
  return read_iter_ != docs_->end();
}
bool PostingListIterator::skip_to(uint32_t val) {
  auto original_read_iter = read_iter_;
  if (docs_->end() - read_iter_ > kBinarySearchThreshold) {
    read_iter_ = std::lower_bound(read_iter_, docs_->end(), val);
  } else {
    while (read_iter_ != docs_->end() && *read_iter_ < val) ++read_iter_;
  }

  if (original_read_iter == read_iter_) return read_iter_ != docs_->end();

  if (original_read_iter == write_iter_) {
    write_iter_ = read_iter_;
  } else {
    write_iter_ = std::copy(original_read_iter, read_iter_, write_iter_);
  }

  return read_iter_ != docs_->end() && *read_iter_ == val;
}
bool PostingListIterator::seek_to(uint32_t val) {
  commit();
  write_iter_ = read_iter_ =
      std::lower_bound(docs_->begin(), docs_->end(), val);
  return read_iter_ != docs_->end() && *read_iter_ == val;
}
void PostingListIterator::kill() { ++read_iter_; }
void PostingListIterator::insert(uint32_t val) {
  if (write_iter_ != read_iter_) {
    *write_iter_ = val;
    ++write_iter_;
  } else {
    read_iter_ = write_iter_ = docs_->insert(write_iter_, val);
  }
}
void PostingListIterator::update(uint32_t val) { *read_iter_ = val; }

IntersectionIterator::IntersectionIterator(
    std::vector<std::unique_ptr<IIterator>> iters)
    : iters_(std::move(iters)) {
  done_ = !sync();
}

bool IntersectionIterator::sync() {
  if (iters_.empty()) return false;
  size_t agree = 0;
  while (true) {
    for (auto& iter : iters_) {
      if (iter->skip_to(cur_)) {
        if (++agree >= iters_.size()) return true;
      } else if (iter->done()) {
        return false;
      } else {
        cur_ = iter->get();
        agree = 1;
      }
    }
  }
}

UnionIterator::UnionIterator(std::vector<std::unique_ptr<IIterator>> iters)
    : iters_(std::move(iters)) {
  populate_next();
}

void UnionIterator::populate_next() {
  next_ = {};
  uint32_t idx = 0;
  for (const auto& it : iters_) {
    if (!it->done()) next_.push(IterPtr{it->get(), idx});
    ++idx;
  }
}

bool UnionIterator::next() {
  uint32_t cur = next_.top().val;
  while (!next_.empty() && next_.top().val <= cur) {
    auto idx = next_.top().idx;
    next_.pop();
    iters_[idx]->next();
    if (!iters_[idx]->done()) next_.push(IterPtr{iters_[idx]->get(), idx});
  }
  return !next_.empty();
}

}  // namespace posting_list