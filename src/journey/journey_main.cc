#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include "journey/bounds_estimator.h"
#include "journey/graph_turner.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, graphfile, "", "graph file");
ABSL_FLAG(std::string, estimations, "", "estimations file");

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  for (int i = 0; i < argc; ++i) {
    LOG(INFO) << "argv[" << i << "] " << argv[i];
  }
  absl::ParseCommandLine(argc, argv);

  CHECK(!absl::GetFlag(FLAGS_graphfile).empty());

  graph::GraphDef graph;
  LOG(INFO) << "Loading graph";
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_graphfile)));
  graph::BoundsEstimator estimations(absl::GetFlag(FLAGS_estimations));

  LOG(INFO) << "Creating turner";
  graph::GraphTurner turner(graph, estimations);
  turner.run();

  return 0;
}