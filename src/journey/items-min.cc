#include "journey/items-min.h"

#include <glog/logging.h>

#include <fstream>

namespace graph {
namespace {

const uint64_t kSimpleRuleMasks[] = {0x3e033e00040000f, 0x801c0000078001c0,
                                     0x100030,          0x2403c407f027fe00,
                                     0x4080800000000,   0x4000001008000000};

// const uint8_t kSimpleRuleCosts[] = {1, 2, 3, 4, 5, 6};

const uint64_t kComplexRuleMasks[] = {
    0x7e0000000000000, 0x1c000,           0x1c0,
    0x7e2000000000000, 0x800000000000000, 0x181cc00780000000,
    0x1040000003c0f};

// const uint8_t kComplexRuleCosts[] = {2, 3, 4, 5, 6, 7, 8};

}  // namespace

int ItemSet::score() const {
  int res = 0;
  {
    int cost_mul = 1;
    for (const uint64_t mask : kSimpleRuleMasks) {
      res += cost_mul++ * __builtin_popcountll(mask & set_);
    }
  }
  {
    int cost_mul = 2;
    for (const uint64_t mask : kSimpleRuleMasks) {
      if ((mask & set_) == mask) res += cost_mul;
      ++cost_mul;
    }
  }

  return res;
}

bool ItemsToMimimumV1::update_cost(ItemSet items, uint64_t cost) {
  auto lower_bound = std::lower_bound(
      minimums_.begin(), minimums_.end(), items,
      [](const auto& a, const auto& b) { return a.items.raw() > b.raw(); });

  if (lower_bound != minimums_.end() && lower_bound->items == items &&
      lower_bound->minimum_cost <= cost) {
    return false;
  }

  for (auto iter = minimums_.begin(); iter != lower_bound; ++iter) {
    if (iter->items.is_superset_of(items) && iter->minimum_cost <= cost) {
      return false;
    }
  }
  const bool need_insert =
      lower_bound == minimums_.end() || lower_bound->items != items;

  minimums_.erase(
      std::remove_if(lower_bound, minimums_.end(),
                     [&](const ItemMinimum& item) {
                       return items.is_strict_superset_of(item.items) &&
                              item.minimum_cost >= cost;
                     }),
      minimums_.end());

  if (need_insert) {
    minimums_.insert(lower_bound, ItemMinimum{items, cost});
  } else {
    lower_bound->minimum_cost = cost;
  }
  return true;
}

std::string ItemsToMimimumV1::debug_string() const {
  std::string res;
  for (const auto& x : minimums_) {
    if (!res.empty()) res += ",";
    res += x.debug_string();
  }
  return "[" + res + "]";
}

bool ItemsToMimimumV2::update_cost(uint16_t state, ItemSet items,
                                   uint64_t cost) {
  auto iter = minimums_.insert(std::pair(std::pair(state, items), cost));
  if (iter.second) {
    return true;
  }
  if (iter.first->second <= cost) return false;
  iter.first->second = cost;
  return true;
}

size_t ItemsToMimimumV2::evict_worse_than(int score) {
  const size_t before = minimums_.size();
  absl::erase_if(minimums_,
                 [&](const auto& v) { return v.first.second.score() < score; });
  return before - minimums_.size();
}

size_t ItemsToMimimumV2::evict_outdated(uint64_t cost) {
  const size_t before = minimums_.size();
  absl::erase_if(minimums_, [&](const auto& v) { return v.second < cost; });
  return before - minimums_.size();
}

void ItemsToMimimumV2::dump(const char* filename) {
  std::vector<std::pair<uint64_t, ItemSet>> tmp;

  for (const auto& x : minimums_) {
    tmp.push_back({x.second, x.first.second});
  }
  std::sort(tmp.begin(), tmp.end());
  std::ofstream fo(filename);
  for (const auto& x : tmp) {
    fo << x.first << ' ' << x.second.debug_string() << "\n";
  }
}

size_t ItemsToMimimumV3::evict_worse_than(int score) {
  std::vector<std::unique_ptr<posting_list::IIterator>> iters;
  iters.push_back(all_pl_.iter());
  for (auto& pl : positive_pl_) iters.push_back(pl.iter());
  for (auto& pl : negative_pl_) iters.push_back(pl.iter());

  size_t count = 0;
  posting_list::UnionIterator it(std::move(iters));
  while (!it.done()) {
    if (docs_[it.get()].items.score() < score) {
      it.kill();
      ++count;
    } else {
      it.next();
    }
  }
  return count;
}

bool ItemsToMimimumV3::update_cost(ItemSet items, uint64_t cost) {
  // LOG(INFO) << this << " D " << items.debug_string() << " " << cost << " : "
  //           << debug_string();
  {
    std::vector<std::unique_ptr<posting_list::IIterator>> iters;
    iters.push_back(all_pl_.iter());
    for (const auto& x : items.iter()) iters.push_back(positive_pl_[x].iter());
    for (posting_list::IntersectionIterator iter(std::move(iters));
         !iter.done(); iter.next()) {
      if (docs_[iter.get()].cost <= cost) {
        // LOG(INFO) << this << " F";
        return false;
      }
    }
  }
  {
    std::vector<std::unique_ptr<posting_list::IIterator>> iters;
    iters.push_back(all_pl_.iter());
    for (const auto& x : items.iter_zeros()) {
      // LOG(INFO) << this << " X " << x;
      iters.push_back(negative_pl_[x].iter());
    }
    std::vector<std::unique_ptr<posting_list::IIterator>> aux_iters;
    for (const auto& x : items.iter()) {
      aux_iters.push_back(positive_pl_[x].iter());
      aux_iters.push_back(negative_pl_[x].iter());
    }
    posting_list::IntersectionIterator it(std::move(iters));
    posting_list::UnionIterator aux_it(std::move(aux_iters));
    while (!it.done()) {
      // LOG(INFO) << this << " K " << it.get();
      if (docs_[it.get()].cost >= cost) {
        // LOG(INFO) << this << " K! " << it.get();
        if (aux_it.seek_to(it.get())) aux_it.kill();
        it.kill();
      } else {
        it.next();
      }
    }
  }
  insert_unchecked(items, cost);

  // LOG(INFO) << this << " T " << items.debug_string() << " " << cost << " : "
  //           << debug_string();
  return true;
}

std::string ItemsToMimimumV3::debug_string() {
  std::string res;
  for (auto iter = all_pl_.iter(); !iter->done(); iter->next()) {
    auto pl = iter->get();
    if (!res.empty()) res += " ";
    res += "[" + std::to_string(pl) + "]" + docs_[pl].items.debug_string() +
           ":" + std::to_string(docs_[pl].cost);
  }
  return res;
}

void ItemsToMimimumV3::insert_unchecked(ItemSet items, uint64_t cost) {
  size_t idx = docs_.size();
  docs_.emplace_back();
  docs_[idx] = ItemsAndCost{items, cost};
  all_pl_.add(idx);
  for (const auto& x : items.iter()) positive_pl_[x].add(idx);
  for (const auto& x : items.iter_zeros()) negative_pl_[x].add(idx);
}

}  // namespace graph