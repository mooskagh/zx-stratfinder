#pragma once

#include <absl/container/flat_hash_map.h>
#include <glog/logging.h>

#include <cstddef>
#include <cstdint>
#include <limits>
#include <optional>
#include <utility>
#include <vector>

template <typename Key, typename Value>
class FixedLRUCache {
 public:
  FixedLRUCache(size_t capacity) : capacity_(capacity), lru_list_(1) {
    lru_list_.reserve(capacity + 2);
  }

  void insert(const Key& key, const Value& value) {
    const auto [iter, inserted] =
        data_.insert(std::make_pair(key, ValueAndLruPtr{value, 0}));
    if (!inserted) {
      iter->second.value = value;
      return;
    }
    insert_at_head(iter);
    shrink_to_capacity();
  }

  std::optional<Value> get(const Key& key) {
    auto iter = data_.find(key);
    if (iter == data_.end()) return std::nullopt;
    auto lru_idx = iter->second.lru_ptr;
    auto next = lru_list_[lru_idx].next_idx;
    if (next == lru_sentinel_) {
      add_to_free_list(lru_sentinel_);
      lru_sentinel_ = lru_idx;
    } else {
      lru_list_[lru_idx] = lru_list_[next];
      data_[lru_list_[lru_idx].key].lru_ptr = lru_idx;
      add_to_free_list(next);
    }
    insert_at_head(iter);
    return iter->second.value;
  }

  size_t size() const { return data_.size(); }

 private:
  void shrink_to_capacity() {
    while (data_.size() > capacity_) {
      CHECK(lru_head_ != lru_sentinel_);
      ListItem item = lru_list_[lru_head_];
      data_.erase(item.key);
      add_to_free_list(lru_head_);
      lru_head_ = item.next_idx;
    }
  }
  void add_to_free_list(uint32_t idx) {
    lru_list_[idx].next_idx = free_list_head_;
    free_list_head_ = idx;
  }

  uint32_t get_free_lru_item() {
    if (free_list_head_ == kNoHead) {
      lru_list_.emplace_back();
      return lru_list_.size() - 1;
    }
    auto ret = free_list_head_;
    free_list_head_ = lru_list_[ret].next_idx;
    return ret;
  }

  struct ListItem {
    Key key;
    uint32_t next_idx;
  };
  struct ValueAndLruPtr {
    Value value;
    uint32_t lru_ptr;
  };
  constexpr static uint32_t kNoHead = std::numeric_limits<uint32_t>::max();
  const size_t capacity_;
  std::vector<ListItem> lru_list_;
  absl::flat_hash_map<Key, ValueAndLruPtr> data_;
  uint32_t free_list_head_ = kNoHead;
  uint32_t lru_head_ = 0;
  uint32_t lru_sentinel_ = 0;

  void insert_at_head(typename decltype(data_)::iterator iter) {
    uint32_t new_item = get_free_lru_item();
    lru_list_[lru_sentinel_].next_idx = new_item;
    lru_list_[lru_sentinel_].key = iter->first;
    iter->second.lru_ptr = lru_sentinel_;
    lru_sentinel_ = new_item;
  }
};