#pragma once
#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>

#include <cstdint>
#include <string>
#include <vector>

#include "journey/posting_list.h"
#include "misc/bititer.h"

namespace graph {

class ItemSet {
 public:
  ItemSet() = default;
  explicit ItemSet(uint64_t set) : set_(set) {}
  int score() const;
  int count() const { return __builtin_popcountll(set_); }

  bool is_superset_of(const ItemSet& other) const {
    return (set_ & other.set_) == other.set_;
  }
  bool is_strict_superset_of(const ItemSet& other) const {
    return set_ != other.set_ && is_superset_of(other);
  }
  static ItemSet combine(ItemSet a, ItemSet b) {
    return ItemSet(a.set_ | b.set_);
  }
  uint64_t raw() const { return set_; }

  bool operator==(const ItemSet& o) const { return set_ == o.set_; }
  bool operator!=(const ItemSet& o) const { return set_ != o.set_; }
  bool operator<(const ItemSet& o) const {
    if (count() != o.count()) return count() < o.count();
    return set_ < o.set_;
  }
  ItemSet operator-(const ItemSet& o) const { return ItemSet(set_ & ~o.set_); }
  ItemSet operator~() const { return ItemSet(~set_); }

  IterateBits iter() const { return {set_}; }
  IterateBits iter_zeros() const { return {~set_}; }

  std::string debug_string() const {
    std::string res;
    for (const auto& x : IterateBits(set_)) {
      if (!res.empty()) res += ",";
      res.append(std::to_string(x));
    }
    return "{" + res + "}";
  }

 private:
  uint64_t set_;

  template <typename H>
  friend H AbslHashValue(H s, const ItemSet& o) {
    return H::combine(std::move(s), o.set_);
  }
};

class ItemsToMimimumV1 {
 public:
  bool update_cost(ItemSet items, uint64_t cost);
  size_t size() const { return minimums_.size(); }

  std::string debug_string() const;

 private:
  struct ItemMinimum {
    ItemSet items;
    uint64_t minimum_cost;

    std::string debug_string() const {
      return items.debug_string() + ":" + std::to_string(minimum_cost);
    }
  };
  std::vector<ItemMinimum> minimums_;
};

class ItemsToMimimumV2 {
 public:
  bool update_cost(uint16_t state, ItemSet items, uint64_t cost);
  size_t size() const { return minimums_.size(); }
  size_t overhead() const { return minimums_.capacity() - minimums_.size(); }

  size_t evict_outdated(uint64_t cost);
  size_t evict_worse_than(int how_many);

  std::string debug_string() const;
  void dump(const char*);

 private:
  using Key = std::pair<uint16_t, ItemSet>;
  absl::flat_hash_map<Key, uint64_t> minimums_;
};

class ItemsToMimimumV3 {
 public:
  bool update_cost(ItemSet items, uint64_t cost);
  size_t size() const { return all_pl_.size(); }
  size_t overhead() const { return docs_.size() - all_pl_.size(); }
  size_t evict_worse_than(int how_many);
  size_t trim_shorter_than(uint64_t how_long);

  std::string debug_string();

 private:
  void insert_unchecked(ItemSet items, uint64_t cost);
  struct ItemsAndCost {
    ItemSet items;
    uint64_t cost;
  };

  std::vector<ItemsAndCost> docs_;
  posting_list::PostingList all_pl_;
  std::vector<posting_list::PostingList> positive_pl_{64};
  std::vector<posting_list::PostingList> negative_pl_{64};
};

using ItemsToMimimum = ItemsToMimimumV2;
}  // namespace graph