#pragma once

#include <absl/hash/hash.h>

#include "proto/jet-set-willy.pb.h"
#include "search/search-config.h"

namespace jet_set_willy {

class WillyPos {
 public:
  WillyPos() = default;
  WillyPos(const WillyPos& other) = default;

  static WillyPos from_vars(int x, int y, bool direction_left, bool is_moving,
                            int jumping_status, int falling_status, int rope);

  static WillyPos from_memory(const Memory& memory);
  static WillyPos from_proto(const WillyPosition& pos);
  WillyPosition as_proto() const;
  void to_memory(Memory& memory) const;
  void debug_print() const;

  bool operator==(const WillyPos& other) const {
    return std::memcmp(this, &other, sizeof(WillyPos)) == 0;
  }

 private:
  WillyPos(uint8_t y, uint8_t dir_mov, uint8_t animation, uint16_t location,
           uint8_t fall, uint8_t jumping, uint8_t rope = 0);
  uint16_t location_ = 0;
  uint8_t y_ = 0;
  uint8_t dir_mov_ = 0;
  uint8_t animation_ = 0;
  uint8_t fall_ = 0;
  uint8_t jumping_ = 0;
  uint8_t rope_ = 0;

  template <typename H>
  friend H AbslHashValue(H h, const WillyPos& c) {
    return H::combine(std::move(h), c.location_, c.y_, c.dir_mov_, c.animation_,
                      c.fall_, c.jumping_, c.rope_);
  }
};

class JswItemSet {
 public:
  JswItemSet(uint8_t room) {
    uint16_t addr = 0xa4ad;
    for (const auto& r : kItemRooms) {
      if (r == room) addrs_.push_back(addr);
      ++addr;
    }
  }

  Set16 operator()(const Machine& machine) const {
    uint16_t res = 0;
    const auto& memory = machine.memory();
    for (const auto addr : addrs_) {
      res <<= 1;
      if ((memory.read(addr) & 0x40) == 0) res |= 1;
    }
    return Set16{res};
  }

  int total() const { return addrs_.size(); }

  int count(const Machine& machine) const {
    const auto& memory = machine.memory();
    int res = 0;
    for (const auto addr : addrs_) {
      if ((memory.read(addr) & 0x40) == 0) ++res;
    }
    return res;
  }

 private:
  // Starts with address 0xA4AD.
  static constexpr uint8_t kItemRooms[] = {
      50, 50, 50, 50, 56, 56, 56, 56, 43, 43, 43, 43, 31, 18, 15, 16, 17,
      60, 59, 58, 37, 37, 37, 27, 57, 58, 11, 34, 25, 25, 25, 25, 51, 22,
      22, 40, 12, 12, 12, 9,  7,  46, 46, 38, 21, 21, 21, 21, 21, 14, 10,
      21, 21, 14, 13, 13, 44, 19, 3,  3,  3,  49, 49, 49, 49, 49, 49, 0,
      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  29, 30, 33};

  std::vector<uint16_t> addrs_;
};

static constexpr U8Accessor cur_room{0x8420};
static constexpr U8Accessor willy_y{0x85cf};
static constexpr U8Accessor willy_dir_mov{0x85d0};
static constexpr U8Accessor willy_animation{0x85d2};
static constexpr U16Accessor willy_location{0x85d3};
static constexpr U8Accessor willy_fall{0x85d1};
static constexpr U8Accessor willy_rope{0x85d6};
static constexpr U8Accessor willy_jumping{0x85d5};

static constexpr RangeAccessor room_definition{0x8000, 0x420};
static constexpr RangeAccessor items_collected_str{0x857C, 3};
static constexpr U8Accessor items_collected{0x85DE};
static constexpr RangeAccessor time_str{0x857F, 6};
static constexpr U8Accessor seconds_counter{0x85CB};
static constexpr U8Accessor willy_lives{0x85CC};
static constexpr U8Accessor inactivity{0x85E0};

static constexpr RangeAccessor room_attrs{0x5e00, 0x200};
static constexpr RangeAccessor room_pixels{0x7000, 0x1000};
static constexpr RangeAccessor item_table{0xA400, 0x200};

void set_willy_pos(Machine* machine, int room, int x, int y,
                   bool direction_left, bool is_moving, int jumping_status,
                   int falling_status);

class RoomResult {
 public:
  void add(const SearchConfig&, uint16_t source_idx, std::vector<InputState>&,
           Machine&);

  TaskResult as_proto(const TaskRequest& request) const;

 private:
  struct ExitKey {
    WillyPos willy_pos;
    uint8_t room;
    bool operator==(const ExitKey& other) const {
      return room == other.room && willy_pos == other.willy_pos;
    }

    template <typename H>
    friend H AbslHashValue(H h, const ExitKey& c) {
      return H::combine(std::move(h), c.willy_pos, c.room);
    }
  };

  struct RouteDef {
    uint16_t source_idx;
    Set16 items;
    std::vector<InputState> inputs;
  };

  absl::flat_hash_map<ExitKey, std::vector<RouteDef>> exits_;
  std::vector<RouteDef> life_losses_;
};

SearchConfig get_config(const TaskRequest& request, RoomResult* room_result);

}  // namespace jet_set_willy
