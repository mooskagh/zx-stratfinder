#include "emulator/memory.h"
#include "search/scorer.h"
#include "search/search-config.h"

namespace clive {

static constexpr U16Accessor level_init_ptr{40055};  // 49017 = level 1

static constexpr RangeAccessor screen{16384, 23296 - 16384};
static constexpr RangeAccessor kbd_buf{0x5c00, 9};

static constexpr RangeAccessor rng_seed{23670, 3};
static constexpr RangeAccessor attr_buf{26000, 800};
static constexpr U8Accessor collectible_colour{41053};
static constexpr U8Accessor collectible_frames{41054};
static constexpr U16Accessor unknown_0{42758};
static constexpr RangeAccessor unknown_1{42896, 104};
static constexpr U16Accessor timer{44792};
static constexpr U16Accessor score{44842};
static constexpr RangeAccessor cur_sprite{48767, 13};
static constexpr U8Accessor unknown_2{48810};
static constexpr U8Accessor train_coord{48813};
static constexpr U8Accessor train_timer{48814};
static constexpr U8Accessor unknown_3{48816};
static constexpr U8Accessor item_1_status{48818};
static constexpr U8Accessor item_2_status{48819};
static constexpr U8Accessor progress_status{48820};
static constexpr U8Accessor unknown_4{48821};
static constexpr U8Accessor unknown_5{48826};
static constexpr RangeAccessor clive_data{51861, 13};
static constexpr RangeAccessor grandom_data{51874, 65};
static constexpr RangeAccessor ghor_data{51939, 195};
static constexpr RangeAccessor gver_data{52134, 104};
static constexpr RangeAccessor gseek_data{52238, 52};
static constexpr RangeAccessor gcomm_data{52290, 153};
static constexpr RangeAccessor unknown_6{63488, 1023};

static constexpr RangeAccessor all_mem{16384, 65535 - 16384};

SearchConfig GetUniversalConfig(const std::string& level);

}  // namespace clive
