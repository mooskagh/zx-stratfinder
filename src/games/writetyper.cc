#include "games/writetyper.h"

#include <bits/stdint-uintn.h>
#include <glog/logging.h>

#include <algorithm>
#include <cstddef>
#include <iterator>

#include "emulator/input.h"
#include "emulator/memory.h"
#include "search/functions.h"

namespace writetyper {
namespace {
constexpr uint16_t kFrameAddr = 0x89ad;
constexpr uint16_t kPauseAddr = 0x8ad1;
constexpr uint16_t kDeathAddr = 0x8c01;
constexpr uint16_t kRoomChangeAddr = 0x8912;
constexpr uint16_t kGameOver = 0x8C4A;
}  // namespace

std::unique_ptr<StateDescriptor> CreateFullState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  descriptor->add(game_vars);
  descriptor->add(room_attrs);
  descriptor->add(room_pixels);
  descriptor->add(item_table);

  return descriptor;
}

class HashState : public StateDescriptor {
 private:
  uint64_t get_hash_seed(const Machine& machine) const override {
    const auto& memory = machine.memory();
    if (memory[willy_rope] == 1) return 0;
    uint64_t res = 1 + memory[willy_fall];
    if (memory[willy_fall] == 1) res += 1000 * memory[willy_jumping];
    return res;
  }
};

std::unique_ptr<StateDescriptor> CreateHashState() {
  auto descriptor = std::make_unique<HashState>();
  descriptor->add(willy_y);
  descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  // descriptor->add(willy_fall);
  descriptor->add(willy_rope);
  descriptor->add(willy_lives);
  descriptor->add(item_table);
  // descriptor->add(willy_jumping);
  descriptor->add(cur_room);
  return descriptor;
}

std::unique_ptr<StateDescriptor> CreateMinimalState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->add(willy_y);
  // descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  descriptor->add(willy_fall);
  // descriptor->add(willy_rope);
  descriptor->add(cur_room);
  return descriptor;
}

int items_collected_count(const Memory& memory) {
  int res = 0;
  for (uint16_t addr = 0xA4AD; addr < 0xA500; ++addr) {
    if ((memory.read(addr) & 0x40) == 0) ++res;
  }
  return res;
}

float get_item_area_score(const Memory& memory) {
  int dist_u = 16;
  int dist_d = 16;
  int dist_l = 32;
  int dist_r = 32;
  int dist_lu = 48;
  int dist_ru = 48;
  int dist_ld = 48;
  int dist_rd = 48;

  auto update = [](int& min_dist, int dist) {
    if (dist < min_dist) min_dist = dist;
  };

  for (int i = 0; i < 83; ++i) {
    uint8_t upper = memory.read(0xA4AD + i);
    uint8_t lower = memory.read(0xA5AD + i);
    if ((upper & 0x40) == 0) continue;
    int x = lower & 31;
    int y = ((lower >> 5) & 7) + 8 * ((upper & 0x80) != 0);
    update(dist_u, y);
    update(dist_d, 15 - y);
    update(dist_l, x);
    update(dist_r, 31 - x);
    update(dist_lu, x + y);
    update(dist_ru, 31 - x + y);
    update(dist_ld, 15 + x - y);
    update(dist_rd, 46 - x - y);
  }

  constexpr float kStrength = 0.5f;

  return (0.1f * std::pow(dist_u / 16.0f, kStrength) +
          0.1f * std::pow(dist_d / 16.0f, kStrength) +
          0.0f * std::pow(dist_l / 32.0f, kStrength) +
          0.2f * std::pow(dist_r / 32.0f, 0.25f) +
          0.1f * std::pow(dist_lu / 48.0f, kStrength) +
          0.2f * std::pow(dist_ru / 48.0f, kStrength) +
          0.1f * std::pow(dist_ld / 48.0f, kStrength) +
          0.2f * std::pow(dist_rd / 48.0f, kStrength) - 0.223661f) *
         (1.28097f);
}

class Scorer : public ScorerBase {
 public:
 private:
  Score score(const Machine& machine, int frame,
              const std::vector<InputState>& history,
              const InputState& input) const override {
    const auto& memory = machine.memory();
    if (machine.pc() == kGameOver || machine.pc() == kPauseAddr)
      return Score::Loss();
    if (memory[eog_status] == 2) return Score::Win();
    int lives = memory[willy_lives];
    int items = items_collected_count(memory);
    int wraps =
        input.has(InputState::kKey_9) +
        std::count_if(history.begin(), history.end(),
                      [](const auto& x) { return x.has(InputState::kKey_9); });
    return -0.02f + lives * (0.02f / 7.0f) +
           1.0f * std::pow(items / 83.0f, 2.0f) +
           4.0f * get_item_area_score(memory) + -0.001f * wraps;
  }
};

int get_rqm(const InputState& state) {
  return (int(state.has(InputState::kKey_Q)) << 2) |
         (int(state.has(InputState::kKey_R)) << 1) |
         (int(state.has(InputState::kKey_M)));
}

class JswValidator : public StateValidator {
 public:
 private:
  bool is_valid(const InputState& state, const Machine& machine, int frame,
                const std::vector<InputState>& history) override {
    CHECK_EQ(machine.pc(), kFrameAddr);

    if (state.empty()) return true;

    /* constexpr size_t kTeleportFreq = 2;
    constexpr size_t kBeforeTeleportLong = 12;
    constexpr size_t kAfterTeleport = 3;
    */

    constexpr size_t kTeleportFreq = 4;
    constexpr size_t kBeforeTeleportLong = 16;
    constexpr size_t kBeforeTeleportShort = 2;
    constexpr size_t kAfterTeleport = 4;

    auto rqm = get_rqm(state);

    if (state.has(InputState::kKey_9)) {
      for (auto i = (history.size() < kTeleportFreq)
                        ? 0
                        : (history.size() - kTeleportFreq);
           i != history.size(); ++i) {
        if (history[i].has(InputState::kKey_9)) return false;
      }
      for (auto i = (history.size() < kBeforeTeleportShort)
                        ? 0
                        : (history.size() - kBeforeTeleportShort);
           i != history.size(); ++i) {
        if (history[i].has(InputState::kKey_9)) return false;
      }

      bool seen_recent_teleport = false;
      bool seen_mismatch = false;
      for (int i = int((history.size() < kBeforeTeleportLong)
                           ? 0
                           : (history.size() - kBeforeTeleportLong));
           i != int(history.size()); ++i) {
        if (get_rqm(history[i]) != rqm) {
          seen_mismatch = true;
        }
        if (history[i].has(InputState::kKey_9)) {
          seen_recent_teleport = true;
        }
        if (seen_mismatch && seen_recent_teleport) return false;
      }
      return true;
    }

    bool had_teleport = false;
    for (auto i = (history.size() < kAfterTeleport)
                      ? 0
                      : (history.size() - kAfterTeleport);
         i != history.size(); ++i) {
      if (history[i].has(InputState::kKey_9)) had_teleport = true;
      if (had_teleport && get_rqm(history[i]) != rqm) return false;
    }
    return empty_was_valid_;
  }

  void here_what_happened(const InputState& state,
                          const Machine& machine) override {
    if (machine.pc() != kFrameAddr) {
      empty_was_valid_ = true;
    } else if (state.empty()) {
      empty_was_valid_ = (machine.memory()[willy_fall] == 0 ||
                          machine.memory()[willy_rope] != 0);
    }
  }

 private:
  bool empty_was_valid_ = false;
};

SearchConfig get_config() {
  SearchConfig config;
  config.game_name = "writetyper";
  config.run_name = "complete";

  config.full_state = CreateFullState();
  config.hash_state = CreateHashState();
  config.minimal_state = CreateMinimalState();
  config.scorer = std::make_unique<Scorer>();

  config.init_func = [](Machine* machine) {
    machine->set_event_mask(Machine::kEventBreakpoint);
    machine->memory().load_rom(get_data_dir() + "roms/48.rom");
    machine->load_snapshot(get_filename("writetyper", "szx", "writetyper"));
    // machine->memory()[writetyper_counter] = 10;
    machine->add_breakpoint(kFrameAddr);
    machine->add_breakpoint(kPauseAddr);
    // machine->add_breakpoint(kDeathAddr);
    // machine->add_breakpoint(kRoomChangeAddr);
    machine->add_breakpoint(kGameOver);
    machine->run();
  };

  config.mode_func = StdModeFunc({
      0x89FE, 0x89FF,          // LDIR
      0x8A2F, 0x8A30,          // LDIR
      0x8B10, 0x8B11,          // LDIR
      0x89ad, 0x89ae, 0x89af,  // animate willy along the bottom(graphics)
      0x89ef, 0x89f0, 0x89f1,  // animate conveyors(graphics)
      0x8a3a, 0x8a3b, 0x8a3c,  // print the time(text)
      0x8a46, 0x8a47, 0x8a48,  // print items(text)
      0x944e, 0x944f, 0x9450,  // animates graphic item/object
      0x8e20, 0x8e21, 0x8e23, 0x8e24,  // remove the jumping/falling  sfx
      // Room draw
      0x8963, 0x8964,          // LDIR
      0x896e, 0x896f, 0x8970,  // Call
      0x897a, 0x897b, 0x897c,  // Call
      0x8982, 0x8983,          // Out
      // Life loss
      0x8c03, 0x8c04, 0x8c05, 0x8c06, 0x8c07, 0x8c08, 0x8c09, 0x8c0a,
      0x8c0b,  //
      0x8c0c, 0x8c0d, 0x8c0e, 0x8c0f, 0x8c10, 0x8c11, 0x8c12, 0x8c13,
      0x8c14,  //
      0x8c15, 0x8c16, 0x8c17, 0x8c18, 0x8c19, 0x8c1a, 0x8c1b, 0x8c1c,
      0x8c1d,  //
      0x8c1e, 0x8c1f, 0x8c20, 0x8c21, 0x8c22, 0x8c23, 0x8c24, 0x8c25,
      0x8c26,  //
      0x8c27, 0x8c28, 0x8c29, 0x8c2a, 0x8c2b, 0x8c2c, 0x8c2d, 0x8c2e,
      0x8c2f,                  //
      0x8c30, 0x8c31, 0x8c32,  //
  });

  config.actions.emplace_back(InputState::kKey_None);
  config.actions.emplace_back(InputState::kKey_Q);  // Left
  config.actions.emplace_back(InputState::kKey_R);  // Right
  config.actions.emplace_back(InputState::kKey_M);  // Jump
  config.actions.emplace_back(InputState::kKey_Q |
                              InputState::kKey_M);  // Left + Jump
  config.actions.emplace_back(InputState::kKey_R |
                              InputState::kKey_M);  // Right + Jump
  config.actions.emplace_back(InputState::kKey_9);  // 9
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_9);  // 19
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_9);  // 29
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_9);                       // 129
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_9);  // 39
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_9);  // 139
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_9);  // 239
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_9);  // 1239
  config.actions.emplace_back(InputState::kKey_4 | InputState::kKey_9);  // 49
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_4 |
                              InputState::kKey_9);  // 149
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_9);  // 249
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_9);  // 1249
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 349
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_9);  // 1349
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_9);  // 2349
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 12349
  config.actions.emplace_back(InputState::kKey_5 | InputState::kKey_9);  // 59
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_5 |
                              InputState::kKey_9);  // 159
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_5 |
                              InputState::kKey_9);  // 259
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_9);  // 1259
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 359
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_9);  // 1359
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_9);  // 2359
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 12359
  config.actions.emplace_back(InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 459
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_9);  // 1459
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_9);  // 2459
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 12459
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_9);  // 3459
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 13459
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 23459
  config.actions.emplace_back(
      InputState::kKey_1 | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 123459
  config.actions.emplace_back(InputState::kKey_6 | InputState::kKey_9);  // 69
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_6 |
                              InputState::kKey_9);  // 169
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_6 |
                              InputState::kKey_9);  // 269
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_6 | InputState::kKey_9);  // 1269
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 369
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_6 | InputState::kKey_9);  // 1369
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_6 | InputState::kKey_9);  // 2369
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 12369
  config.actions.emplace_back(InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 469
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_4 |
                              InputState::kKey_6 | InputState::kKey_9);  // 1469
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_6 | InputState::kKey_9);  // 2469
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 12469
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_6 | InputState::kKey_9);  // 3469
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 13469
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 23469
  config.actions.emplace_back(
      InputState::kKey_1 | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 123469
  config.actions.emplace_back(InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 569
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 1569
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 2569
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 12569
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 3569
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 13569
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 23569
  config.actions.emplace_back(
      InputState::kKey_1 | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 123569
  config.actions.emplace_back(InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 4569
  config.actions.emplace_back(InputState::kKey_1 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 14569
  config.actions.emplace_back(InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 24569
  config.actions.emplace_back(
      InputState::kKey_1 | InputState::kKey_2 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 124569
  config.actions.emplace_back(InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 34569

  /*
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_9);  // 9
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_9);  // 19
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_9);  // 29
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_9);  // 129
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_9);  // 39
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_9);  // 139
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_9);  // 239
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_9);  // 1239
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_4 |
                              InputState::kKey_9);  // 49
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_9);  // 149
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_9);  // 249
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_9);  // 1249
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_9);  // 349
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 1349
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 2349
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_4 | InputState::kKey_9);  // 12349
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_5 |
                              InputState::kKey_9);  // 59
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_5 | InputState::kKey_9);  // 159
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_9);  // 259
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1259
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_9);  // 359
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1359
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 2359
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_5 | InputState::kKey_9);  // 12359
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_9);  // 459
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1459
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 2459
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 12459
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 3459
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 13459
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 23459
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 123459
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_6 |
                              InputState::kKey_9);  // 69
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_6 | InputState::kKey_9);  // 169
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_6 | InputState::kKey_9);  // 269
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1269
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_6 | InputState::kKey_9);  // 369
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1369
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2369
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_6 | InputState::kKey_9);  // 12369
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_4 |
                              InputState::kKey_6 | InputState::kKey_9);  // 469
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1469
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2469
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 12469
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 3469
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 13469
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 23469
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 123469
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 12569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 3569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 13569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 23569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 123569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 4569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_1 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 14569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_2 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 24569
  config.actions.emplace_back(InputState::kKey_Q | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 124569
  config.actions.emplace_back(
      InputState::kKey_Q | InputState::kKey_3 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 34569

  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_9);  // 9
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_9);  // 19
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_9);  // 29
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_9);  // 129
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_9);  // 39
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_9);  // 139
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_9);  // 239
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_9);  // 1239
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_4 |
                              InputState::kKey_9);  // 49
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_9);  // 149
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_9);  // 249
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_9);  // 1249
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_9);  // 349
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 1349
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_4 |
                              InputState::kKey_9);  // 2349
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_4 | InputState::kKey_9);  // 12349
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_5 |
                              InputState::kKey_9);  // 59
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_5 | InputState::kKey_9);  // 159
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_9);  // 259
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1259
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_9);  // 359
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1359
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_5 |
                              InputState::kKey_9);  // 2359
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_5 | InputState::kKey_9);  // 12359
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_9);  // 459
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 1459
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 2459
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 12459
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 3459
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 13459
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_5 | InputState::kKey_9);  // 23459
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_5 |
                              InputState::kKey_9);  // 123459
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_6 |
                              InputState::kKey_9);  // 69
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_6 | InputState::kKey_9);  // 169
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_6 | InputState::kKey_9);  // 269
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1269
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_6 | InputState::kKey_9);  // 369
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1369
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_3 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2369
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_3 | InputState::kKey_6 | InputState::kKey_9);  // 12369
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_4 |
                              InputState::kKey_6 | InputState::kKey_9);  // 469
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1469
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2469
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 12469
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 3469
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 13469
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_4 | InputState::kKey_6 | InputState::kKey_9);  // 23469
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_4 | InputState::kKey_6 |
                              InputState::kKey_9);  // 123469
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_5 |
                              InputState::kKey_6 | InputState::kKey_9);  // 569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 1569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_2 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 2569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_2 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 12569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 3569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_3 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 13569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_2 | InputState::kKey_3 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 23569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_3 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 123569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 4569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_1 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 14569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_2 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 24569
  config.actions.emplace_back(InputState::kKey_R | InputState::kKey_1 |
                              InputState::kKey_2 | InputState::kKey_4 |
                              InputState::kKey_5 | InputState::kKey_6 |
                              InputState::kKey_9);  // 124569
  config.actions.emplace_back(
      InputState::kKey_R | InputState::kKey_3 | InputState::kKey_4 |
      InputState::kKey_5 | InputState::kKey_6 | InputState::kKey_9);  // 34569
      */

  config.hash_is_per_frame = true;
  config.global_duplicate_score = Sigmoid(20, 0.98, 100, -1.0f, 0.0077135606f);
  config.local_duplicate_score = Sigmoid(20, 0.999, 500, -2.0f, 0.0015013522f);
  config.valid_state_func = std::make_unique<JswValidator>();
  config.itemset_func = [](Machine& machine) {
    return Set16(items_collected_count(machine.memory()));
  };

  config.ticks_limit = 3000000;
  config.show_every = 2000;
  config.search_width = 25000;
  config.num_threads = 5;
  return config;
}

}  // namespace writetyper
