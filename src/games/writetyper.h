#pragma once

#include <absl/hash/hash.h>

#include "emulator/memory.h"
#include "search/search-config.h"

namespace writetyper {

static constexpr U8Accessor cur_room{0x8420};
static constexpr U8Accessor willy_y{0x85cf};
static constexpr U8Accessor willy_dir_mov{0x85d0};
static constexpr U8Accessor willy_animation{0x85d2};
static constexpr U16Accessor willy_location{0x85d3};
static constexpr U8Accessor willy_fall{0x85d1};
static constexpr U8Accessor willy_rope{0x85d6};
static constexpr U8Accessor willy_jumping{0x85d5};

static constexpr RangeAccessor room_definition{0x8000, 0x420};
static constexpr RangeAccessor items_collected_str{0x857C, 3};
static constexpr U8Accessor items_collected{0x85DE};
static constexpr RangeAccessor time_str{0x857F, 6};
static constexpr U8Accessor seconds_counter{0x85CB};
static constexpr U8Accessor willy_lives{0x85CC};
static constexpr U8Accessor inactivity{0x85E0};
static constexpr U8Accessor eog_status{0x85DF};

static constexpr RangeAccessor game_vars{0x8000, 0x5e5};
static constexpr RangeAccessor room_attrs{0x5e00, 0x200};
static constexpr RangeAccessor room_pixels{0x7000, 0x1000};
static constexpr RangeAccessor item_table{0xA4AD, 83};
static constexpr RangeAccessor item_lower{0xA5AD, 83};

static constexpr U8Accessor writetyper_counter{0x85E3};

SearchConfig get_config();

}  // namespace writetyper
