#include "games/manic-miner.h"

#include "search/functions.h"

namespace manic_miner {
namespace {

constexpr bool kStrongKong = true;

constexpr uint16_t kFrameAddr_BB = 0x870e;
constexpr uint16_t kFrameAddr_SP = 0x8714;
constexpr uint16_t kDeathAddr_BB = 0x8908;
constexpr uint16_t kDeathAddr_SP = 0x890e;
constexpr uint16_t kWinAddr_BB = 0x902b;
constexpr uint16_t kWinAddr_SP = 0x9036;

std::unique_ptr<StateDescriptor> CreateFullState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  descriptor->add(cavern_attrs);
  descriptor->add(cavern_pixels);
  descriptor->add(willy_y);
  descriptor->add(willy_animation);
  descriptor->add(willy_movement);
  descriptor->add(willy_fall);
  descriptor->add(willy_location);
  descriptor->add(willy_jumping);
  descriptor->add(last_item_drawn_attr);
  descriptor->add(item1_attr);
  descriptor->add(item2_attr);
  descriptor->add(item3_attr);
  descriptor->add(item4_attr);
  descriptor->add(item5_attr);
  descriptor->add(kong_status);
  descriptor->add(eugene_y);
  descriptor->add(remaining_air);
  descriptor->add(game_clock);
  descriptor->add(guardians);
  descriptor->add(screen_flash);
  descriptor->add(portal_attr);
  descriptor->add(score);
  // descriptor->add(all_mem);

  return descriptor;
}

std::unique_ptr<StateDescriptor> CreateHashState(int level) {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  descriptor->add(cavern_attrs);
  descriptor->add(cavern_pixels);
  descriptor->add(willy_y);
  descriptor->add(willy_animation);
  descriptor->add(willy_movement);
  descriptor->add(willy_fall);
  descriptor->add(willy_location);
  descriptor->add(willy_jumping);
  descriptor->add(item1_attr);
  descriptor->add(item2_attr);
  descriptor->add(item3_attr);
  descriptor->add(item4_attr);
  descriptor->add(item5_attr);
  descriptor->add(kong_status);
  descriptor->add(eugene_y);
  descriptor->add(guardians);
  descriptor->add(portal_attr);
  if (level == 19) {
    descriptor->add(remaining_air);
    descriptor->add(game_clock);
  }

  return descriptor;
}

class MinimalState : public StateDescriptor {
 public:
  void set_level(int level) { level_ = level; }

 private:
  uint64_t get_hash_seed(const Machine& machine) const override {
    uint64_t res = 0;
    const auto& memory = machine.memory();
    if (level_ != 12 && memory[item1_attr] == 0) res |= 1;
    if (level_ != 12 && memory[item2_attr] == 0) res |= 2;
    if (memory[item3_attr] == 0) res |= 4;
    if (memory[item4_attr] == 0) res |= 8;
    if (memory[item5_attr] == 0) res |= 16;
    if (kStrongKong && (level_ == 8 || level_ == 12)) {
      res |= memory[kong_status] * 256;
    }
    return res;
  }
  int level_ = 0;
};

std::unique_ptr<MinimalState> CreateMinimalState(int level) {
  auto descriptor = std::make_unique<MinimalState>();

  descriptor->add(willy_y);
  descriptor->add(willy_animation);
  descriptor->add(willy_movement);
  descriptor->add(willy_fall);
  descriptor->add(willy_location);
  descriptor->add(willy_jumping);
  descriptor->set_level(level);

  return descriptor;
}

class Scorer : public ScorerBase {
 public:
  Scorer(int level, bool is_sp) : level_(level), is_sp_(is_sp) {}

 private:
  Score score(const Machine& machine, int frame, const std::vector<InputState>&,
              const InputState&) const override {
    const auto& memory = machine.memory();
    if (machine.pc() == (is_sp_ ? kDeathAddr_SP : kDeathAddr_BB)) {
      return Score::Loss();
    }
    if (machine.pc() == (is_sp_ ? kWinAddr_SP : kWinAddr_BB)) {
      if (kStrongKong && (level_ == 8 || level_ == 12) &&
          memory[kong_status] == 0) {
        return Score::Loss();
      }
      return Score::Win((memory[remaining_air] * 256 + memory[game_clock]) /
                        4.0f);
    }
    int items_collected = 0;
    if (level_ != 12 && memory[item1_attr] == 0) ++items_collected;
    if (level_ != 12 && memory[item2_attr] == 0) ++items_collected;
    if (memory[item3_attr] == 0) items_collected += level_ == 17 ? 2 : 1;
    if (memory[item4_attr] == 0) ++items_collected;
    if (memory[item5_attr] == 0) ++items_collected;
    if (level_ == 5 && items_collected == 5 && memory[eugene_y] == 87) {
      return Score::Loss();
    }
    if (level_ == 12 && memory[switch_src] != memory[switch_dst]) {
      ++items_collected;
    }
    if (kStrongKong && (level_ == 8 || level_ == 12) &&
        memory[kong_status] != 0) {
      ++items_collected;
    }
    float score = 1.0f;
    score += static_cast<float>(items_collected);
    score *= 0.1f;
    if (level_ == 19) {
      auto air = (memory[remaining_air] * 256 + memory[game_clock]) / 4;
      auto excess_air_usage = 4091 - frame - air;
      if (excess_air_usage > 256) return Score::Loss();
      if (air < 3127) return Score::Loss();
      score -= 0.0004f * excess_air_usage;
    }
    return score;
  }

  float sure_win_score(int frame) const override {
    return level_ == 19 ? (4091.0f - frame) : 0.0f;
  }

  bool can_potentially_win(const Machine& machine, int,
                           float win_score) override {
    if (level_ != 19) return true;
    const auto& memory = machine.memory();
    float cur_air = (memory[remaining_air] * 256 + memory[game_clock]) / 4.0f;
    return cur_air > win_score;
  }

  int level_ = 0;
  const bool is_sp_;
};

std::unique_ptr<Scorer> CreateScorer(int level, bool is_sp) {
  return std::make_unique<Scorer>(level, is_sp);
}

class ManicValidator : public StateValidator {
 public:
  ManicValidator(bool is_sp) : is_sp_(is_sp) {}

 private:
  bool is_valid(const InputState& state, const Machine& machine, int,
                const std::vector<InputState>&) override {
    return state.empty() || empty_was_valid_ ||
           machine.memory()[willy_fall] == 0;
  }
  void here_what_happened(const InputState& state,
                          const Machine& machine) override {
    if (machine.pc() != (is_sp_ ? kFrameAddr_SP : kFrameAddr_BB)) {
      empty_was_valid_ = true;
    } else if (state.empty()) {
      empty_was_valid_ = (machine.memory()[willy_fall] == 0);
    }
  }

 private:
  bool empty_was_valid_ = false;
  const bool is_sp_;
};

}  // namespace

SearchConfig GetUniversalConfig(const std::string& level) {
  const bool is_sp = level.find("-sp") != std::string::npos;
  SearchConfig config;
  int level_num = 0;
  if (level.find("lvl5") == 0) level_num = 5;
  if (level.find("lvl8") == 0) level_num = 8;
  if (level.find("lvl12") == 0) level_num = 12;
  if (level.find("lvl17") == 0) level_num = 17;
  if (level.find("lvl19") == 0) level_num = 19;
  LOG(INFO) << "Level: " << level_num;
  config.game_name = "manic-miner";
  config.run_name = level;
  config.full_state = CreateFullState();
  config.hash_state = CreateHashState(level_num);
  config.minimal_state = CreateMinimalState(level_num);
  config.scorer = CreateScorer(level_num, is_sp);
  config.init_func = [is_sp, level](Machine* machine) {
    machine->set_event_mask(Machine::kEventBreakpoint);
    machine->memory().load_rom(get_data_dir() + "roms/48.rom");
    machine->load_snapshot(get_filename("manic-miner", "szx", level));
    machine->add_breakpoint(is_sp ? kFrameAddr_SP : kFrameAddr_BB);  // Frame.
    machine->add_breakpoint(is_sp ? kDeathAddr_SP : kDeathAddr_BB);  // Death.
    machine->add_breakpoint(is_sp ? kWinAddr_SP : kWinAddr_BB);      // Win.
  };
  config.actions.emplace_back(InputState::kKey_None);
  config.actions.emplace_back(InputState::kKey_Q);  // Left
  config.actions.emplace_back(InputState::kKey_R);  // Right
  config.actions.emplace_back(InputState::kKey_M);  // Jump
  config.actions.emplace_back(InputState::kKey_Q |
                              InputState::kKey_M);  // Left + Jump
  config.actions.emplace_back(InputState::kKey_R |
                              InputState::kKey_M);  // Right + Jump
  config.hash_is_per_frame = true;
  config.dedup_by_minimal_hash = level_num == 19;
  config.global_duplicate_score =
      Sigmoid(20, 0.999, level_num == 19 ? 200 : 50, -1.5f, 0.0f);
  config.local_duplicate_score = Sigmoid(10, 0.8, 41, -4.0f, 0.63f);
  config.valid_state_func = std::make_unique<ManicValidator>(is_sp);
  config.search_width = 50000;
  // config.search_width = 30000;

  return config;
}

}  // namespace manic_miner
