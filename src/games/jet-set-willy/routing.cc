#include "games/jet-set-willy/routing.h"

#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <bits/stdint-uintn.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include <fstream>
#include <iostream>
#include <sstream>

#include "misc/bititer.h"
#include "misc/disjoint-sets.h"
#include "misc/filebuffer.h"

ABSL_FLAG(std::string, data_dir, "/home/crem/dev/zx-stratfinder/tmp/data",
          "Data dir with all the things");

namespace {
std::string rooms_path() {
  return absl::GetFlag(FLAGS_data_dir) + "/rooms.bin";
}
}  // namespace

namespace jet_set_willy {

Routing::Routing() {
  LOG(INFO) << "Reading state file.";
  rooms_.ParseFromString(read_file_to_string(rooms_path()));
}

void Routing::commit() {
  LOG(INFO) << "Committing rooms";
  write_string_to_file(rooms_path(), rooms_.SerializeAsString());
  LOG(INFO) << "Committed";
}

void Routing::dump_rooms() {
  LOG(INFO) << "Dumping rooms to rooms_dump.txt";
  std::string out;
  CHECK(google::protobuf::TextFormat::PrintToString(rooms_, &out));
  write_string_to_file("rooms_dump.txt", out);
}

namespace {
struct UsedExit {
  uint32_t room;
  uint32_t entryway;
  uint32_t items_collected;
  uint32_t frames;
  bool operator==(const UsedExit& other) const {
    return room == other.room && entryway == other.entryway &&
           items_collected == other.items_collected && frames == other.frames;
  }
  template <typename H>
  friend H AbslHashValue(H h, const UsedExit& c) {
    return H::combine(std::move(h), c.room, c.entryway, c.items_collected,
                      c.frames);
  }
};

}  // namespace

Room& Routing::room_by_idx(int idx) {
  auto& res = *rooms_.mutable_room(idx);
  CHECK_EQ(res.id(), idx);
  return res;
}
Entryway& Routing::entryway_by_idx(int room_idx, int idx) {
  auto& res = *room_by_idx(room_idx).mutable_entryway(idx);
  CHECK_EQ(res.id(), idx);
  return res;
}

Route& Routing::route_by_addr(const RouteAddress& addr) {
  auto& entryway = entryway_by_idx(addr.room(), addr.entryway());
  return *entryway.mutable_entrypoint(addr.entrypoint())
              ->mutable_route(addr.route_id());
}

namespace {
/* bool waypoints_are_close(const WillyPosition& x, const WillyPosition& y) {
  if (std::abs(int(x.x() - y.x())) > 16) return false;
  if (std::abs(int(x.y() - y.y())) > 16) return false;
  return true;
} */

std::string addr_to_str(const RouteAddress& addr) {
  std::string res =
      std::to_string(addr.room()) + ":" + std::to_string(addr.entryway());
  if (addr.has_entrypoint()) res += "/" + std::to_string(addr.entrypoint());
  if (addr.has_route_id()) res += "." + std::to_string(addr.route_id());
  return res;
}
}  // namespace

void Routing::task_request(const TaskRequestCmd& req) {
  CHECK(!req.filename().empty());

  Entryway* entryway = nullptr;
  uint32_t best_room = 0;

  for (auto& room : *rooms_.mutable_room()) {
    for (auto& ew : *room.mutable_entryway()) {
      if (ew.status() == Entryway::PENDING && ew.high_priority()) {
        best_room = room.id();
        entryway = &ew;
        break;
      }
    }
  }

  if (!entryway) {
    int total_pending = 0;
    absl::flat_hash_map<uint32_t, std::pair<int, int>> rooms;
    absl::flat_hash_map<uint32_t, uint32_t> room_to_entryway;
    for (const auto& room : rooms_.room()) {
      for (const auto& entryway : room.entryway()) {
        switch (entryway.status()) {
          case Entryway::PENDING:
            ++rooms[room.id()].second;
            ++total_pending;
            if (!room_to_entryway.contains(room.id())) {
              room_to_entryway[room.id()] = entryway.id();
            }
            break;
          case Entryway::DONE:
          case Entryway::RUNNING:
            --rooms[room.id()].first;
            break;
          default:
            break;
        }
      }
    }
    LOG(INFO) << "Total " << total_pending << " tasks pending in "
              << room_to_entryway.size() << " rooms.";

    for (auto& room : rooms) {
      if (room.second.second == 0) room.second.first = -10000;
    }

    best_room =
        std::max_element(rooms.begin(), rooms.end(),
                         [](const std::pair<uint32_t, std::pair<int, int>>& x,
                            const std::pair<uint32_t, std::pair<int, int>>& y) {
                           return x.second < y.second;
                         })
            ->first;
    entryway = &entryway_by_idx(best_room, room_to_entryway.at(best_room));
  }

  int task_id = rooms_.tasks_started();
  rooms_.set_tasks_started(1 + task_id);
  entryway->set_status(Entryway::RUNNING);
  entryway->set_task_id(task_id);
  LOG(INFO) << "Task ID: " << task_id;

  TaskRequest request;
  request.set_id(task_id);
  request.set_room_id(best_room);
  request.set_entryway_id(entryway->id());
  for (const auto& entrypoint : entryway->entrypoint()) {
    *request.add_entrypoint() = entrypoint.position();
  }
  request.set_search_width(500000);
  request.set_max_frames(5000);
  request.set_max_no_progress(250);

  std::string out;
  CHECK(google::protobuf::TextFormat::PrintToString(request, &out));
  LOG(INFO) << "Task proto:\n" << out;
  LOG(INFO) << "Writing task file to " << req.filename();
  write_string_to_file(req.filename(), request.SerializeAsString());
  std::cout << task_id << std::endl;
}

void Routing::apply_result(const ApplyCmd& resp) {
  CHECK(!resp.filename().empty());
  TaskResult result;
  result.ParseFromString(read_file_to_string(resp.filename()));

  LOG(INFO) << "Parsing response for Entryway " << result.request().room_id()
            << ":" << result.request().entryway_id() << " TASK "
            << result.request().id();
  auto& src_entryway = entryway_by_idx(result.request().room_id(),
                                       result.request().entryway_id());
  if (src_entryway.status() == Entryway::IGNORE) {
    LOG(INFO) << "Entryway status IGNORE, doing nothing.";
    return;
  }
  CHECK(src_entryway.status() == Entryway::RUNNING);

  LOG(INFO) << "Populating entryway with newly found information.";
  absl::flat_hash_map<int, std::vector<Route*>> room_to_routes;
  src_entryway.set_status(Entryway::DONE);
  src_entryway.set_total_frames(result.frames_used());
  src_entryway.set_duration_seconds(result.duration_seconds());
  src_entryway.set_task_id(result.request().id());

  std::vector<Route> routes;
  for (const auto& route : result.route()) {
    routes.push_back(route);
  }
  std::sort(routes.begin(), routes.end(), [](const auto& x, const auto& y) {
    return x.input_length() < y.input_length();
  });
  for (const auto& route : routes) {
    // for (const auto& route : result.route()) {
    auto* entrypoint = src_entryway.mutable_entrypoint(route.entrypoint_id());
    CHECK_EQ(entrypoint->id(), route.entrypoint_id());
    entrypoint->set_used(true);
    auto new_route_id = entrypoint->route_size();
    auto* r = entrypoint->add_route();
    *r = route;
    r->set_id(new_route_id);
    LOG(INFO) << "New route id " << result.request().room_id() << ":"
              << result.request().entryway_id() << "/" << r->entrypoint_id()
              << "." << new_route_id;
    if (r->is_death()) {
      auto* dest = r->mutable_destination_addr();
      dest->set_room(result.request().room_id());
      dest->set_entryway(result.request().entryway_id());
      dest->set_entrypoint(r->entrypoint_id());
      LOG(INFO) << "Add life loss route immediately, dst_addr="
                << addr_to_str(*dest);
    } else {
      room_to_routes[r->exit_room()].push_back(r);
    }
  }

  LOG(INFO) << "Clustering new exits. " << room_to_routes.size()
            << " exit rooms";
  for (const auto& [room_id, routes] : room_to_routes) {
    LOG(INFO) << "Processing room " << room_id;
    auto& room = room_by_idx(room_id);
    DisjointSets sets(routes.size());
    for (size_t i = 0; i < routes.size(); ++i) {
      auto* x = routes[i];
      for (size_t j = i + 1; j < routes.size(); ++j) {
        auto* y = routes[j];
        if (x->input_length() != y->input_length()) continue;
        if (x->items_collected() != y->items_collected()) continue;
        // if (waypoints_are_close(x->exit_pos(), y->exit_pos())) sets.unite(i,
        // j);
      }
    }

    absl::flat_hash_map<size_t, std::vector<Route*>> clusters;
    for (size_t i = 0; i < routes.size(); ++i) {
      clusters[sets.get_leader(i)].push_back(routes[i]);
    }

    LOG(INFO) << clusters.size() << " clusters";
    auto cluster_idx = 0;
    for (auto& kv : clusters) {
      LOG(INFO) << "Processing cluster " << cluster_idx++;
      auto& routes = kv.second;
      bool intersects = false;
      for (const auto& entryway : room.entryway()) {
        for (const auto& entrypoint : entryway.entrypoint()) {
          auto iter =
              std::find_if(routes.begin(), routes.end(), [&](const Route* x) {
                if (x->has_destination_addr()) return false;
                return x->exit_pos().SerializeAsString() ==
                       entrypoint.position().SerializeAsString();
              });
          if (iter == routes.end()) continue;
          intersects = true;
          auto* addr = (*iter)->mutable_destination_addr();
          addr->set_room(room_id);
          addr->set_entryway(entryway.id());
          addr->set_entrypoint(entrypoint.id());
          LOG(INFO) << "Route " << result.request().room_id() << ":"
                    << result.request().entryway_id() << "/"
                    << (*iter)->entrypoint_id() << "." << (*iter)->id()
                    << " leads to existing entryway " << addr_to_str(*addr);
          routes.erase(iter);
        }
      }
      if (routes.empty()) {
        LOG(INFO) << "No fresh waypoints.";
        continue;
      }
      auto new_entryway_id = room.entryway_size();
      LOG(INFO) << "Creating new entryway " << room_id << ":" << new_entryway_id
                << " with " << (intersects ? "NEEDS_REVIEW" : "GOOD")
                << " status";
      auto* new_entryway = room.add_entryway();
      new_entryway->set_id(new_entryway_id);
      new_entryway->set_status(intersects ? Entryway::NEEDS_REVIEW
                                          : Entryway::PENDING);
      std::sort(routes.begin(), routes.end(),
                [](const Route* x, const Route* y) {
                  const auto& a = x->exit_pos();
                  const auto& b = y->exit_pos();
                  if (a.fall() != b.fall()) return a.fall() < b.fall();
                  if (a.jump() != b.jump()) return a.jump() < b.jump();
                  if (a.moving() != b.moving()) return a.moving() < b.moving();
                  if (a.rope() != b.rope()) return a.rope() < b.rope();
                  if (a.dir_left() != b.dir_left()) {
                    return a.dir_left() < b.dir_left();
                  }
                  if (a.x() != b.x()) return a.x() < b.x();
                  if (a.y() != b.y()) return a.y() < b.y();
                  return false;
                });
      size_t idx = 0;
      for (auto* r : routes) {
        auto* new_entrypoint = new_entryway->add_entrypoint();
        new_entrypoint->set_id(idx);
        *new_entrypoint->mutable_position() = r->exit_pos();
        auto* addr = r->mutable_destination_addr();
        addr->set_room(room_id);
        addr->set_entryway(new_entryway_id);
        addr->set_entrypoint(idx);
        LOG(INFO) << "Route id " << result.request().room_id() << ":"
                  << result.request().entryway_id() << "/" << r->entrypoint_id()
                  << "." << r->id() << " leads to " << addr_to_str(*addr);
        ++idx;
      }
    }
  }

  /* while (extend_journeys()) {
  } */
  LOG(INFO) << "Done.";
}

void Routing::dump_rooms_nicely(const DumpCmd& cmd) {
  std::ofstream off("rooms.txt");
  for (const auto& room : rooms_.room()) {
    off << "\n\n\n\nRoom " << room.id() << " ";
    for (const auto& item : room.item()) off << "+" << item;
    off << "  " << room.name();
    off << "\n";
    absl::flat_hash_set<uint32_t> known_destinations;
    for (const auto& entryway : room.entryway()) {
      off << "\n  Entryway " << room.id() << ":" << entryway.id() << " "
          << Entryway::Status_Name(entryway.status());
      if (entryway.has_total_frames())
        off << " " << entryway.total_frames() << "f";
      if (entryway.has_duration_seconds())
        off << " " << entryway.duration_seconds() << "s";
      if (entryway.has_task_id()) off << " t=" << entryway.task_id();
      if (entryway.high_priority()) off << " !important";
      off << "\n";
      for (const auto& entrypoint : entryway.entrypoint()) {
        const auto& position = entrypoint.position();
        off << "    " << room.id() << ":" << entryway.id() << "/"
            << entrypoint.id();
        off << " (" << position.x() << "," << position.y() << ")";
        off << "\n";
        std::vector<Route> routes;
        for (const auto& route : entrypoint.route()) {
          routes.push_back(route);
        }
        std::sort(routes.begin(), routes.end(),
                  [](const auto& x, const auto& y) {
                    return x.input_length() < y.input_length();
                  });
        for (const auto& route : routes) {
          if (cmd.one_per_destination() &&
              (route.is_death() ||
               known_destinations.contains(route.destination_addr().room()))) {
            continue;
          }
          known_destinations.insert(route.destination_addr().room());

          off << "      " << room.id() << ":" << entryway.id() << "/"
              << entrypoint.id() << "." << route.id();
          off << " (" << route.input_length() << ")";
          if (route.items_collected() != 0) {
            off << " [";
            bool first = true;
            for (const auto& x : IterateBits(route.items_collected())) {
              if (!first) off << " ";
              first = false;
              off << room.item(x);
            }
            off << "]";
          }
          off << " -> ";
          if (route.is_death()) {
            off << "D";
          } else {
            const auto& pos = route.exit_pos();
            off << addr_to_str(route.destination_addr());
            off << " (" << pos.x() << "," << pos.y() << ")";
          }
          if (cmd.include_routes()) {
            off << " " << route.inputs();
          }
          off << "\n";
        }
      }
    }
  }
}

namespace {
RouteAddress parse_address(const std::string& str) {
  std::istringstream iss(str);
  enum Element { kRoom, kEntryway, kEntrypoint, kRoute };
  Element cur_element = kRoom;

  RouteAddress result;
  uint32_t val;
  while (iss >> val) {
    switch (cur_element) {
      case kRoom:
        result.set_room(val);
        break;
      case kEntryway:
        result.set_entryway(val);
        break;
      case kEntrypoint:
        result.set_entrypoint(val);
        break;
      case kRoute:
        result.set_route_id(val);
        break;
    }

    char c;
    if (!(iss >> c)) break;
    switch (c) {
      case ':':
        cur_element = kEntryway;
        break;
      case '/':
        cur_element = kEntrypoint;
        break;
      case '.':
        cur_element = kRoute;
        break;
      default:
        LOG(FATAL) << "Unexpected character [" << c << "]";
    }
  }

  return result;
}

void reset_room(Room* room, const ResetCmd& cmd) {
  for (auto& entryway : *room->mutable_entryway()) {
    bool clear = false;
    if (cmd.running() && entryway.status() == Entryway::RUNNING) {
      LOG(INFO) << "Entryway " << room->id() << ":" << entryway.id()
                << " RUNNING";
      clear = true;
    } else if (cmd.needs_review() &&
               entryway.status() == Entryway::NEEDS_REVIEW) {
      LOG(INFO) << "Entryway " << room->id() << ":" << entryway.id()
                << " NEEDS_REVIEW";
      clear = true;
    } else if (cmd.done() && entryway.status() == Entryway::DONE) {
      LOG(INFO) << "Entryway " << room->id() << ":" << entryway.id() << " DONE";
      clear = true;
    }
    if (clear) {
      entryway.set_status(Entryway::PENDING);
      entryway.clear_task_id();
      entryway.clear_duration_seconds();
      entryway.clear_total_frames();
      for (Entrypoint& point : *entryway.mutable_entrypoint()) {
        point.clear_used();
        point.clear_route();
      }
    }
  }
}
}  // namespace

void Routing::split_entryways(const SplitCmd& cmd) {
  absl::flat_hash_set<std::pair<uint32_t, uint32_t>> entryway_ids;
  for (const auto& entryway : cmd.entryway()) {
    auto addr = parse_address(entryway);
    entryway_ids.emplace(addr.room(), addr.entryway());
  }
  LOG(INFO) << entryway_ids.size() << " entryways to cleanup.";
  for (const auto& [ro, ew] : entryway_ids) {
    LOG(INFO) << "Processing entryway " << ro << ":" << ew;
    auto& room = room_by_idx(ro);
    auto& entryway = entryway_by_idx(ro, ew);
    LOG(INFO) << entryway.entrypoint_size() << " points";
    entryway.set_status(Entryway::PENDING);
    entryway.clear_task_id();
    entryway.clear_duration_seconds();
    entryway.clear_total_frames();
    for (Entrypoint& point : *entryway.mutable_entrypoint()) {
      point.clear_used();
      point.clear_route();
    }

    for (int i = 1; i < entryway.entrypoint_size(); ++i) {
      auto new_entryway_id = room.entryway_size();
      auto* new_entryway = room.add_entryway();
      new_entryway->set_id(new_entryway_id);
      new_entryway->set_status(Entryway::PENDING);
      auto* new_entrypoint = new_entryway->add_entrypoint();
      *new_entrypoint = entryway.entrypoint(i);
      new_entrypoint->set_id(0);
      LOG(INFO) << "Created entryway " << ro << ":" << new_entryway_id;
    }

    while (entryway.entrypoint_size() > 1) {
      entryway.mutable_entrypoint()->RemoveLast();
    }
  }

  LOG(INFO) << "Clearing dependent routes.";
  size_t total = 0;
  for (auto& room : *rooms_.mutable_room()) {
    for (auto& entryway : *room.mutable_entryway()) {
      if (entryway.status() != Entryway::DONE) continue;
      bool should_clear = false;
      for (const auto& entrypoint : entryway.entrypoint()) {
        for (const auto& route : entrypoint.route()) {
          if (route.is_death()) continue;
          auto ew = std::pair<uint32_t, uint32_t>(
              route.destination_addr().room(),
              route.destination_addr().entryway());
          if (entryway_ids.contains(ew)) {
            should_clear = true;
            break;
          }
        }
        if (should_clear) break;
      }
      if (should_clear) {
        ++total;
        LOG(INFO) << "Clearing " << room.id() << ":" << entryway.id();
        entryway.set_status(Entryway::PENDING);
        entryway.clear_task_id();
        entryway.clear_duration_seconds();
        entryway.clear_total_frames();
        for (Entrypoint& point : *entryway.mutable_entrypoint()) {
          point.clear_used();
          point.clear_route();
        }
      }
    }
  }
  LOG(INFO) << "Cleared " << total << " entryways.";
}

void Routing::reset_status(const ResetCmd& cmd) {
  if (cmd.room_size() == 0) {
    for (auto& room : *rooms_.mutable_room()) {
      reset_room(&room, cmd);
    }
  } else {
    for (const auto& room_id : cmd.room()) {
      reset_room(rooms_.mutable_room(room_id), cmd);
    }
  }
  LOG(INFO) << "Done.";
}

void Routing::promote(const PromoteCmd& cmd) {
  for (const auto& x : cmd.entryway()) {
    auto addr = parse_address(x);
    auto& entryway = entryway_by_idx(addr.room(), addr.entryway());
    entryway.set_high_priority(true);
  }
  LOG(INFO) << "Done";
}

void Routing::item_stats() {
  for (const auto& room : rooms_.room()) {
    if (room.item_size() == 0) continue;
    std::cout << "Room " << room.id() << " ";
    std::cout << "  " << room.name();
    std::cout << "\n";

    std::vector<uint16_t> items;
    const uint16_t mask = (1 << room.item_size()) - 1;
    for (int i = 0; i < room.item_size(); ++i) items.push_back(mask);

    absl::flat_hash_set<uint32_t> known_destinations;
    for (const auto& entryway : room.entryway()) {
      for (const auto& entrypoint : entryway.entrypoint()) {
        for (const auto& route : entrypoint.route()) {
          if (route.items_collected() != 0) {
            for (const auto& x : IterateBits(route.items_collected())) {
              items[x] &= route.items_collected();
            }
          }
        }
      }
    }
    for (int i = 0; i < room.item_size(); ++i) {
      std::cout << "  " << room.item(i) << " ->";
      for (const auto& j : IterateBits(items[i])) {
        std::cout << " " << room.item(j);
      }
      std::cout << "\n";
    }
  }
}

namespace {
bool SameEntrypoint(const RouteAddress& a, const RouteAddress& b) {
  return a.room() == b.room() && a.entryway() == b.entryway() &&
         a.entrypoint() == b.entrypoint();
}
}  // namespace

void Routing::fix_route(const FixRouteCmd& cmd) {
  std::vector<RouteAddress> route;
  for (const auto& x : cmd.route()) route.push_back(parse_address(x));
  std::reverse(route.begin(), route.end());
  RouteAddress current_end = route_by_addr(route.front()).destination_addr();
  LOG(INFO) << "End: " << addr_to_str(current_end);

  for (const auto& x : route) {
    const auto& base_route = route_by_addr(x);
    RouteAddress new_end;
    const auto& entryway = entryway_by_idx(x.room(), x.entryway());
    for (const auto& entrypoint : entryway.entrypoint()) {
      for (const auto& route : entrypoint.route()) {
        if (!SameEntrypoint(route.destination_addr(), current_end)) continue;
        if (route.input_length() > base_route.input_length()) {
          RouteAddress tmp;
          tmp.set_room(x.room());
          tmp.set_entryway(x.entryway());
          tmp.set_entrypoint(entrypoint.id());
          tmp.set_route_id(route.id());
          LOG(INFO) << "Too long " << addr_to_str(tmp) << " "
                    << route.input_length() << " vs "
                    << base_route.input_length();
          continue;
        }
        if ((route.items_collected() & base_route.items_collected()) !=
            base_route.items_collected()) {
          RouteAddress tmp;
          tmp.set_room(x.room());
          tmp.set_entryway(x.entryway());
          tmp.set_entrypoint(entrypoint.id());
          tmp.set_route_id(route.id());
          LOG(INFO) << "Items collected mismatch " << addr_to_str(tmp) << " "
                    << route.items_collected() << " vs "
                    << base_route.items_collected();
          continue;
        }
        if (route.input_length() < base_route.input_length()) {
          LOG(INFO) << "Even shorter! " << route.input_length() << " vs "
                    << base_route.input_length();
        }
        if (route.items_collected() != base_route.items_collected()) {
          LOG(INFO) << "Even beefier! " << route.items_collected() << " vs "
                    << base_route.items_collected();
        }
        new_end.set_room(x.room());
        new_end.set_entryway(x.entryway());
        new_end.set_entrypoint(entrypoint.id());
        new_end.set_route_id(route.id());
        LOG(INFO) << addr_to_str(new_end);
      }
    }
    if (new_end.ByteSizeLong() == 0) {
      LOG(INFO) << "DISCONTINUITY! No way to get from " << x.room() << ":"
                << x.entryway() << " to " << addr_to_str(current_end);
      current_end = x;
    } else {
      current_end = new_end;
    }
    LOG(INFO) << "======== Expected " << addr_to_str(x) << "   actual "
              << addr_to_str(current_end);
  }
}

void Routing::run() {
  std::string command_line;

  while (std::getline(std::cin, command_line)) {
    Command cmd;
    if (!(google::protobuf::TextFormat::MergeFromString(command_line, &cmd))) {
      std::cout << "[" << command_line << "]?\n";
      continue;
    }
    switch (cmd.command_case()) {
      case Command::kExit:
      case Command::kQuit:
        return;
      case Command::kCommit:
        commit();
        break;
      case Command::kDump:
        dump_rooms_nicely(cmd.dump());
        break;
      case Command::kTaskRequest:
        task_request(cmd.task_request());
        break;
      case Command::kApply:
        apply_result(cmd.apply());
        break;
      case Command::kPlay:
        void play_route(const PlayCmd& cmd, const RoomState& rooms);
        play_route(cmd.play(), rooms_);
        break;
      case Command::kReset:
        reset_status(cmd.reset());
        break;
      case Command::kPromote:
        promote(cmd.promote());
        break;
      case Command::kItemstats:
        item_stats();
        break;
      case Command::kFixRoute:
        fix_route(cmd.fix_route());
        break;
      case Command::kSplit:
        split_entryways(cmd.split());
        break;
      case Command::kMeasureRoute:
        void measure_route(const MeasureRouteCmd& cmd, const RoomState& rooms);
        measure_route(cmd.measure_route(), rooms_);
        break;
      case Command::kPopulateArcDurations:
        void populate_arc_durations(const PopulateArcDurations& cmd,
                                    RoomState* rooms);
        populate_arc_durations(cmd.populate_arc_durations(), &rooms_);
        break;
      case Command::COMMAND_NOT_SET:
        continue;
    }
  }
}

}  // namespace jet_set_willy

/*
To split:

 13:0/18
 28:33/5
 31:30/3
 37:28/1
 42:56/5
 43:5/4
 52:1/1
 54:12/7
 60:11/2


split {entryway: "13:0" entryway: "28:33" entryway: "31:30" entryway: "37:28"
entryway: "42:56" entryway: "43:5" entryway: "52:1" entryway: "54:12" entryway:
"60:11"}


*/
