#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstddef>
#include <cstdint>
#include <fstream>
#include <ios>
#include <queue>
#include <utility>

#include "games/jet-set-willy/journey/item-set-enumerator.h"
#include "games/jet-set-willy/journey/item_permutations.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "estimations file to write");
ABSL_FLAG(int, full_depth, 3, "Number of bits to handle");
ABSL_FLAG(uint64_t, force_mask, 0ULL, "Mask to force include into states");
ABSL_FLAG(bool, cluster_items, false, "Cluster items");

// ABSL_FLAG(float, relative_tolerance, 1.20f, "Relative tolerance");
// ABSL_FLAG(int, absolute_tolerance, 1200, "Absolute tolerance");

namespace jet_set_willy {
namespace {

using NodeAndItems = std::pair<int, uint64_t>;
using Edges = std::set<NodeAndItems>;

struct __attribute__((packed)) Bound {
  uint64_t items;
  uint32_t node;
  uint32_t cost;
};

uint64_t TranslateItems(uint64_t items) {
  if (!absl::GetFlag(FLAGS_cluster_items)) return items;
  uint64_t res = 0;
if (items & 0x3c00ULL) res |= 0x1ULL;
if (items & 0xfULL) res |= 0x2ULL;
if (items & 0xe01c0ULL) res |= 0x4ULL;
if (items & 0x40000000001c000ULL) res |= 0x8ULL;
if (items & 0x8000000ULL) res |= 0x10ULL;
if (items & 0x1040000000ULL) res |= 0x20ULL;
if (items & 0x80000000000ULL) res |= 0x40ULL;
if (items & 0xc00000000000ULL) res |= 0x80ULL;
if (items & 0x1040000000000ULL) res |= 0x100ULL;
if (items & 0x3e2000800000000ULL) res |= 0x200ULL;
if (items & 0x800000000000000ULL) res |= 0x400ULL;
if (items & 0x1000000000000000ULL) res |= 0x800ULL;
if (items & 0x780000000ULL) res |= 0x1000ULL;
if (items & 0x1c000000000000ULL) res |= 0x2000ULL;
if (items & 0x100000ULL) res |= 0x4000ULL;
if (items & 0x2000000000000000ULL) res |= 0x8000ULL;
if (items & 0x200000ULL) res |= 0x10000ULL;
if (items & 0x30ULL) res |= 0x20000ULL;
if (items & 0x33e030000000ULL) res |= 0x40000ULL;
if (items & 0x7800000ULL) res |= 0x80000ULL;
if (items & 0x4000000000000000ULL) res |= 0x100000ULL;
if (items & 0x8000000000400200ULL) res |= 0x200000ULL;
  return res;
}

class RecordKeeper {
 public:
  RecordKeeper(size_t total_nodes) : total_nodes_(total_nodes) {}

  bool UpdateRecord(size_t node_idx, uint64_t items_mask, uint32_t record) {
    size_t idx =
        node_idx + total_nodes_ * enumerator_.GetItemsIndex(items_mask);
    if (idx >= records_.size())
      records_.resize(idx + 1, std::numeric_limits<uint32_t>::max());
    if (record < records_[idx]) {
      if (records_[idx] == std::numeric_limits<uint32_t>::max()) ++num_non_inf_;
      records_[idx] = record;
      return true;
    }
    return false;
  }

  uint32_t GetRecord(size_t node_idx, uint64_t items_mask) const {
    size_t idx =
        node_idx + total_nodes_ * enumerator_.GetItemsIndex(items_mask);
    if (idx >= records_.size()) return std::numeric_limits<uint32_t>::max();
    return records_[idx];
  }

  size_t total_count() const { return records_.size(); }
  size_t non_inf_count() const { return num_non_inf_; }

  const ItemSetEnumerator& enumerator() const { return enumerator_; }

 private:
  const size_t total_nodes_;
  ItemSetEnumerator enumerator_;
  std::vector<uint32_t> records_;
  size_t num_non_inf_ = 0;
};

struct NodeAndCost {
  uint64_t items;
  uint32_t node_idx;
  uint32_t cost;
  bool operator<(const NodeAndCost& other) const { return cost > other.cost; }
};

class EdgeKeeper {
 public:
  struct Edge {
    uint32_t to;
    uint32_t cost;
    uint64_t items;
  };
  using Edges = std::vector<Edge>;

  void AddEdge(uint32_t from, uint32_t to, uint64_t items, uint32_t cost) {
    if (edges_.size() <= from) edges_.resize(from + 1);
    edges_[from].push_back(Edge{to, cost, items});
  }

  const Edges& From(uint32_t from) { return edges_[from]; }

 private:
  std::vector<Edges> edges_;
};

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  RecordKeeper keeper(graph.node_size());

  std::priority_queue<NodeAndCost> nodes_and_costs;
  for (const auto& end_node : graph.end_node()) {
    keeper.UpdateRecord(end_node, 0, 0);
    nodes_and_costs.push(NodeAndCost{0, end_node, 0});
  }

  EdgeKeeper edges;
  for (const auto& edge : graph.edge()) {
    edges.AddEdge(edge.to_node(), edge.from_node(),
                  TranslateItems(edge.items()), edge.cost());
  }

  const int kDepth = absl::GetFlag(FLAGS_full_depth);
  const int kForceDepth = __builtin_popcountll(absl::GetFlag(FLAGS_force_mask));
  const uint64_t kForceMask = absl::GetFlag(FLAGS_force_mask);
  while (!nodes_and_costs.empty()) {
    auto node_and_cost = nodes_and_costs.top();
    if (node_and_cost.cost == std::numeric_limits<uint32_t>::max()) {
      break;
    }
    nodes_and_costs.pop();
    if (keeper.GetRecord(node_and_cost.node_idx, node_and_cost.items) !=
        node_and_cost.cost) {
      continue;
    }
    LOG_EVERY_N(INFO, 10000)
        << "Queue=" << nodes_and_costs.size()
        << " solutions=" << keeper.non_inf_count() << "/"
        << keeper.total_count() << " cost=" << node_and_cost.cost
        << " items=" << __builtin_popcountll(node_and_cost.items);
    for (const auto& edge : edges.From(node_and_cost.node_idx)) {
      absl::flat_hash_set<uint64_t> masks;
      for (int num_bits = 0; num_bits <= std::max(kDepth, kForceDepth);
           ++num_bits) {
        for (BitPermutation perm(edge.items, num_bits); !perm.done();
             perm.next()) {
          uint64_t mask = perm.get() | node_and_cost.items;
          if (__builtin_popcountll(mask) <= kDepth ||
              (mask & kForceMask) == mask) {
            masks.insert(mask);
          }
        }
      }
      uint32_t cost = node_and_cost.cost + edge.cost;
      for (const auto mask : masks) {
        if (keeper.UpdateRecord(edge.to, mask, cost)) {
          nodes_and_costs.push(NodeAndCost{mask, edge.to, cost});
        }
      }
    }
  }

  // Output.
  std::ofstream of(absl::GetFlag(FLAGS_output).c_str(), std::ios_base::binary);

  auto masks = keeper.enumerator().masks();
  std::sort(masks.begin(), masks.end(), [](uint64_t a, uint64_t b) {
    if (__builtin_popcountll(a) != __builtin_popcountll(b)) {
      return __builtin_popcountll(a) < __builtin_popcountll(b);
    }
    return a < b;
  });

  size_t count_total = 0;
  uint32_t node_idx = 0;
  LOG(INFO) << "Writing...";
  for (const auto& node : graph.node()) {
    LOG(INFO) << "====== " << node.label();
    for (auto mask : masks) {
      ++count_total;
      const auto cost = keeper.GetRecord(node_idx, mask);
      Bound bound{mask, node_idx, cost};
      of.write(reinterpret_cast<const char*>(&bound), sizeof(bound));
    }
    ++node_idx;
  }

  LOG(INFO) << "Total: " << count_total << " ("
            << float(count_total) / graph.node_size() << " per node)";
  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
