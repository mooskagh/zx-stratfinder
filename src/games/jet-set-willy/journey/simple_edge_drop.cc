#include <absl/container/flat_hash_map.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstdint>
#include <limits>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "journey/items-min.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");

namespace jet_set_willy {
namespace {

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();

  LOG(INFO) << "Initializing edges";
  absl::flat_hash_map<std::pair<uint32_t, uint32_t>, std::vector<graph::Edge>>
      edges;
  for (const auto& edge : graph.edge()) {
    edges[{edge.from_node(), edge.to_node()}].push_back(edge);
  }

  graph.clear_edge();
  size_t edges_dropped = 0;
  for (auto& [key, value] : edges) {
    std::sort(value.begin(), value.end(), [](const auto& a, const auto& b) {
      graph::ItemSet isa(a.items());
      graph::ItemSet isb(b.items());
      if (isa.count() != isb.count()) {
        return isa.count() < isb.count();
      }
      return a.cost() > b.cost();
    });
    for (auto iter = value.begin(); iter != value.end(); ++iter) {
      if (std::any_of(iter + 1, value.end(), [&iter](const auto& edge) {
            return edge.cost() <= iter->cost() &&
                   (edge.items() & iter->items()) == iter->items();
          })) {
        ++edges_dropped;
        continue;
      }
      *graph.add_edge() = *iter;
    }
  }

  LOG(INFO) << "edges_dropped=" << edges_dropped;
  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    LOG(INFO) << "nodes=" << graph.node_size();
    LOG(INFO) << "edges=" << graph.edge_size();
    write_string_to_file(absl::GetFlag(FLAGS_output),
                         graph.SerializeAsString());
  }

  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
