#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstdint>
#include <limits>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"
#include "journey/items-min.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");
ABSL_FLAG(std::vector<std::string>, drop, {}, "nodes to drop");

namespace jet_set_willy {
namespace {
void Run() {
  std::vector<uint32_t> indices_to_drop;
  for (const auto& index : absl::GetFlag(FLAGS_drop)) {
    indices_to_drop.push_back(std::stoi(index));
  }
  std::sort(indices_to_drop.rbegin(), indices_to_drop.rend());
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();
  size_t kept_edges = 0;
  size_t removed_edges = 0;
  size_t filtered_edges = 0;
  size_t spawned_edges = 0;
  for (const auto& index : indices_to_drop) {
    LOG(INFO) << "Dropping node " << index << ": " << graph.node(index).label();

    std::vector<std::optional<graph::Edge>> edges{graph.edge().begin(),
                                                  graph.edge().end()};

    size_t in_edge_count = 0;
    size_t out_edge_count = 0;
    for (auto& edge : edges) {
      if (edge->from_node() == index) {
        ++out_edge_count;
        continue;
      }
      if (edge->to_node() == index) {
        ++in_edge_count;
        continue;
      }
    }
    if (in_edge_count * out_edge_count > 2000000) {
      LOG(INFO) << "Warning, in=" << in_edge_count << " out=" << out_edge_count
                << " total=" << in_edge_count * out_edge_count;
    }
    if (in_edge_count * out_edge_count > 500000 && spawned_edges > 0) {
      LOG(INFO) << "Too many edges, breaking";
      break;
    }

    graph.clear_edge();
    absl::flat_hash_map<uint32_t, std::vector<graph::Edge>> in_edgeses;
    absl::flat_hash_map<uint32_t, std::vector<graph::Edge>> out_edgeses;
    for (auto& edge : edges) {
      if (edge->from_node() == index) {
        out_edgeses[edge->to_node()].push_back(*edge);
        edge.reset();
        ++removed_edges;
        continue;
      }
      if (edge->to_node() == index) {
        in_edgeses[edge->from_node()].push_back(*edge);
        edge.reset();
        ++removed_edges;
        ;
        continue;
      }
      ++kept_edges;
    }
    for (const auto& in_edges : in_edgeses) {
      for (const auto& out_edges : out_edgeses) {
        std::vector<graph::Edge> new_edges;
        for (const auto& in : in_edges.second) {
          for (const auto& out : out_edges.second) {
            graph::Edge new_edge;
            new_edge.set_from_node(in.from_node());
            new_edge.set_to_node(out.to_node());
            new_edge.set_items(in.items() | out.items());
            new_edge.set_cost(in.cost() + out.cost());
            new_edge.set_label(in.label() + " " + out.label());
            new_edges.push_back(new_edge);
          }
        }

        std::sort(new_edges.begin(), new_edges.end(),
                  [](const auto& a, const auto& b) {
                    graph::ItemSet isa(a.items());
                    graph::ItemSet isb(b.items());
                    if (isa.count() != isb.count()) {
                      return isa.count() < isb.count();
                    }
                    return a.cost() > b.cost();
                  });
        for (auto iter = new_edges.begin(); iter != new_edges.end(); ++iter) {
          if (std::any_of(iter + 1, new_edges.end(), [&iter](const auto& edge) {
                return edge.cost() <= iter->cost() &&
                       (edge.items() & iter->items()) == iter->items();
              })) {
            ++filtered_edges;
            continue;
          }
          ++spawned_edges;
          *graph.add_edge() = *iter;
        }
      }
    }
    for (const auto& edge : edges) {
      if (edge) *graph.add_edge() = *edge;
    }
    LOG(INFO) << "Added so far: " << spawned_edges;
    if (spawned_edges > 2000000) {
      LOG(INFO) << "Too many edges, breaking";
      break;
    }
  }
  LOG(INFO) << "kept_edges=" << kept_edges;
  LOG(INFO) << "removed_edges=" << removed_edges;
  LOG(INFO) << "filtered_edges=" << filtered_edges;
  LOG(INFO) << "spawned_edges=" << spawned_edges;
  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();
  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    std::string serialized = graph.SerializeAsString();
    CHECK(!serialized.empty());
    write_string_to_file(absl::GetFlag(FLAGS_output), serialized);
  }

  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
