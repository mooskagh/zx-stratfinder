#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <algorithm>
#include <bitset>
#include <limits>
#include <queue>
#include <string>
#include <vector>

#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "graph file to write");

namespace jet_set_willy {
namespace {

struct NodeCosts {
  std::vector<uint32_t> incoming;
  std::vector<uint32_t> outgoing;
  uint32_t base_cost = 0;
};

struct NodeKey {
  uint16_t node;
  uint64_t items;

  bool operator<(const NodeKey& other) const {
    if (node != other.node) return node < other.node;
    return items < other.items;
  }
  bool operator==(const NodeKey& other) const {
    return node == other.node && items == other.items;
  }

  std::string DebugString() const {
    return std::to_string(node) + " " + std::to_string(items);
  }
  std::string DebugString(const graph::GraphDef& graph) const {
    return graph.node(node).label() + " " + std::bitset<64>(items).to_string();
  }
};

struct EdgeSet {
  std::vector<NodeKey> outgoing;
  std::vector<NodeKey> incoming;
  bool operator<(const EdgeSet& other) const {
    if (outgoing != other.outgoing) return outgoing < other.outgoing;
    return incoming < other.incoming;
  }
};

size_t GetNodeKeyIdx(const std::vector<NodeKey>& edges, NodeKey key) {
  auto iter = std::lower_bound(edges.begin(), edges.end(), key);
  CHECK(iter != edges.end());
  return iter - edges.begin();
}

void EnsureSize(std::vector<uint32_t>& vec, size_t sz) {
  if (vec.size() < sz) vec.resize(sz);
}

int64_t GetMaxDiff(const std::vector<uint32_t>& base,
                   const std::vector<uint32_t>& expt) {
  if (base.empty()) return 0;
  int64_t diff = std::numeric_limits<int64_t>::min();
  for (size_t i = 0; i < base.size(); ++i) {
    diff = std::max(diff, static_cast<int64_t>(expt[i]) - base[i]);
  }
  return diff;
}

absl::flat_hash_set<uint16_t> GetRedundantNodes(const graph::GraphDef& graph) {
  std::vector<EdgeSet> node_to_edgeset(graph.node_size());

  LOG(INFO) << "Building node_to_edgeset";
  for (const auto& edge : graph.edge()) {
    node_to_edgeset[edge.from_node()].outgoing.push_back(
        NodeKey{static_cast<uint16_t>(edge.to_node()), edge.items()});
    node_to_edgeset[edge.to_node()].incoming.push_back(
        NodeKey{static_cast<uint16_t>(edge.from_node()), edge.items()});
  }

  // LOG(INFO) << graph.node(0).label();
  // for (const auto& n : node_to_edgeset[0].incoming)
  //   LOG(INFO) << "Incoming: " << n.DebugString(graph);
  // for (const auto& n : node_to_edgeset[1].outgoing)
  //   LOG(INFO) << "Outgoing: " << n.DebugString(graph);

  LOG(INFO) << "Sorting edges";
  for (auto& edgeset : node_to_edgeset) {
    std::sort(edgeset.incoming.begin(), edgeset.incoming.end());
    std::sort(edgeset.outgoing.begin(), edgeset.outgoing.end());
  }

  LOG(INFO) << "Building node_costs";
  std::vector<NodeCosts> node_costs(graph.node_size());
  for (const auto& edge : graph.edge()) {
    {
      auto& vec = node_to_edgeset[edge.from_node()].outgoing;
      auto idx = GetNodeKeyIdx(
          vec, NodeKey{static_cast<uint16_t>(edge.to_node()), edge.items()});
      EnsureSize(node_costs[edge.from_node()].outgoing, idx + 1);
      node_costs[edge.from_node()].outgoing[idx] = edge.cost();
    }

    {
      auto& vec = node_to_edgeset[edge.to_node()].incoming;
      auto idx = GetNodeKeyIdx(
          vec, NodeKey{static_cast<uint16_t>(edge.from_node()), edge.items()});
      EnsureSize(node_costs[edge.to_node()].incoming, idx + 1);
      node_costs[edge.to_node()].incoming[idx] = edge.cost();
    }
  }

  LOG(INFO) << "Extracting base costs";
  for (auto& costs : node_costs) {
    if (!costs.outgoing.empty()) {
      auto base =
          *std::min_element(costs.outgoing.begin(), costs.outgoing.end());
      std::for_each(costs.outgoing.begin(), costs.outgoing.end(),
                    [&](auto& x) { x -= base; });
      costs.base_cost += base;
    }
    if (!costs.incoming.empty()) {
      auto base =
          *std::min_element(costs.incoming.begin(), costs.incoming.end());
      std::for_each(costs.incoming.begin(), costs.incoming.end(),
                    [&](auto& x) { x -= base; });
      costs.base_cost += base;
    }
  }

  LOG(INFO) << "Building edgeset_to_nodes";
  std::map<EdgeSet, std::vector<uint16_t>> edgeset_to_nodes;
  for (size_t i = 0; i < node_to_edgeset.size(); ++i) {
    edgeset_to_nodes[node_to_edgeset[i]].push_back(i);
  }

  LOG(INFO) << "Going through edgesets";
  absl::flat_hash_set<uint16_t> nodes_to_remove;
  for (const auto& kv : edgeset_to_nodes) {
    if (kv.second.size() < 2) continue;
    // LOG(INFO) << "Candidate has " << kv.second.size()
    //           << " nodes. incoming=" << kv.first.incoming.size()
    //           << " outgoing=" << kv.first.outgoing.size();
    // for (const auto& n : kv.second) LOG(INFO) << graph.node(n).label();
    // for (const auto& n : kv.first.incoming)
    //   LOG(INFO) << "Incoming: " << n.DebugString(graph);
    // for (const auto& n : kv.first.outgoing)
    //   LOG(INFO) << "Outgoing: " << n.DebugString(graph);
    size_t removed_count = 0;
    for (auto i : kv.second) {
      const auto& base = node_costs[i];
      bool removed = false;
      for (auto j : kv.second) {
        if (i == j) continue;
        if (nodes_to_remove.contains(j)) continue;
        const auto& expt = node_costs[j];
        // expt=212 base=134
        // LOG(INFO) << graph.node(j).label() << " "
        //           << GetMaxDiff(base.incoming, expt.incoming) << " "
        //           << GetMaxDiff(base.outgoing, expt.outgoing) << " "
        //           << expt.base_cost << " " << base.base_cost;
        const bool base_is_better =
            GetMaxDiff(base.incoming, expt.incoming) +
                GetMaxDiff(base.outgoing, expt.outgoing) + expt.base_cost -
                base.base_cost >
            0;
        if (!base_is_better) {
          LOG(INFO) << "    Removing " << graph.node(i).label();
          ++removed_count;
          nodes_to_remove.insert(i);
          removed = true;
          break;
        }
      }
      // if (!removed) LOG(INFO) << "     Kept " << graph.node(i).label();
    }
    // LOG(INFO) << "To remove in cluster: " << removed_count;
  }

  LOG(INFO) << "Done building GetRedundantNodes, to_remove="
            << nodes_to_remove.size();
  return nodes_to_remove;
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();

  absl::flat_hash_set<uint16_t> not_deletable_nodes;
  for (const auto& x : graph.start_node()) not_deletable_nodes.insert(x);
  for (const auto& x : graph.end_node()) not_deletable_nodes.insert(x);

  std::vector<int> num_out_edges(graph.node_size());
  std::vector<int> num_in_edges(graph.node_size());

  for (const auto& edge : graph.edge()) {
    ++num_in_edges[edge.to_node()];
    ++num_out_edges[edge.from_node()];
  }

  std::queue<size_t> nodes_to_remove;
  for (const auto& x : GetRedundantNodes(graph)) nodes_to_remove.push(x);
  absl::flat_hash_set<size_t> removed_nodes;

  for (size_t i = 0; i < num_out_edges.size(); ++i) {
    if (num_out_edges[i] == 0 || num_in_edges[i] == 0) {
      LOG(INFO) << "Adding " << i << " " << graph.node(i).label()
                << " as it has no edges";
      nodes_to_remove.push(i);
    }
  }

  while (!nodes_to_remove.empty()) {
    // LOG(INFO) << num_in_edges[90] << " " << num_out_edges[90];
    auto node = nodes_to_remove.front();
    nodes_to_remove.pop();
    LOG(INFO) << "Node: " << node << " " << graph.node(node).label();
    if (not_deletable_nodes.contains(node)) continue;
    if (!removed_nodes.insert(node).second) continue;
    for (const auto& edge : graph.edge()) {
      if (edge.from_node() == node && --num_in_edges[edge.to_node()] == 0) {
        LOG(INFO) << "  Enqueueing: " << edge.to_node() << " "
                  << graph.node(edge.to_node()).label();
        nodes_to_remove.push(edge.to_node());
      }
      if (edge.to_node() == node && --num_out_edges[edge.from_node()] == 0) {
        LOG(INFO) << "  Enqueueing: " << edge.from_node() << " "
                  << graph.node(edge.from_node()).label();
        nodes_to_remove.push(edge.from_node());
      }
    }
  }
  LOG(INFO) << "Total: " << removed_nodes.size() << "/" << graph.node_size()
            << " (" << removed_nodes.size() * 100.0f / graph.node_size() << "%";

  if (!absl::GetFlag(FLAGS_output).empty()) {
    size_t idx = 0;
    std::vector<int> new_indices(graph.node_size(), -1);
    for (int i = 0; i < graph.node_size(); ++i) {
      if (removed_nodes.contains(i)) continue;
      new_indices[i] = idx++;
    }

    graph::GraphDef out = graph;
    out.clear_node();
    for (int i = 0; i < graph.node_size(); ++i) {
      if (new_indices[i] == -1) continue;
      *out.add_node() = graph.node(i);
    }
    out.clear_edge();
    for (int i = 0; i < graph.edge_size(); ++i) {
      auto edge = graph.edge(i);
      if (new_indices[edge.from_node()] == -1 ||
          new_indices[edge.to_node()] == -1) {
        continue;
      }
      edge.set_from_node(new_indices[edge.from_node()]);
      edge.set_to_node(new_indices[edge.to_node()]);
      *out.add_edge() = edge;
    }
    for (int i = 0; i < graph.start_node_size(); ++i) {
      out.set_start_node(i, new_indices[graph.start_node(i)]);
    }
    for (int i = 0; i < graph.end_node_size(); ++i) {
      out.set_end_node(i, new_indices[graph.end_node(i)]);
    }
    LOG(INFO) << "Writing graph.";
    LOG(INFO) << "nodes=" << out.node_size();
    LOG(INFO) << "edges=" << out.edge_size();
    write_string_to_file(absl::GetFlag(FLAGS_output), out.SerializeAsString());
  }
  LOG(INFO) << "Done";
};

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
