#pragma once

#include <absl/container/flat_hash_map.h>

#include <vector>

namespace jet_set_willy {

class ItemSetEnumerator {
 public:
  int GetItemsIndex(uint64_t items) const {
    auto iter = itemset_to_index_.find(items);
    if (iter != itemset_to_index_.end()) return iter->second;
    const int ret = index_to_itemset_.size();
    itemset_to_index_[items] = ret;
    index_to_itemset_.push_back(items);
    return ret;
  }

  uint64_t GetIndexItems(int index) const { return index_to_itemset_[index]; }
  const std::vector<uint64_t>& masks() const { return index_to_itemset_; }

 private:
  mutable std::vector<uint64_t> index_to_itemset_;
  mutable absl::flat_hash_map<uint64_t, int> itemset_to_index_;
};

}  // namespace jet_set_willy