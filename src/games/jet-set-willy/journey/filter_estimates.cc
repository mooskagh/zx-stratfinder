#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstddef>
#include <cstdint>
#include <ios>
#include <queue>
#include <utility>

#include "absl/container/flat_hash_map.h"
#include "misc/filebuffer.h"

ABSL_FLAG(std::string, input, "", "estimations file to write");
ABSL_FLAG(std::string, output, "", "estimations file to write");

namespace jet_set_willy {
namespace {

struct __attribute__((packed)) Bound {
  uint64_t items;
  uint32_t node;
  uint32_t cost;
};

class Cleaner {
 public:
  Cleaner(const std::string& filename) : file_(fopen(filename.c_str(), "wb")) {}
  void Add(const Bound& bound);
  void FlushAndClear();
  ~Cleaner() { fclose(file_); }

 private:
  FILE* file_;
  std::map<int, std::vector<Bound>> bounds_;
};

void Cleaner::Add(const Bound& bound) {
  auto& bounds = bounds_[bound.cost];
  for (const auto& x : bounds) {
    CHECK(bound.node == x.node);
    if ((bound.items & x.items) == x.items) return;
  }
  bounds.push_back(bound);
}

void Cleaner::FlushAndClear() {
  for (const auto& kv : bounds_) {
    fwrite(&kv.second[0], sizeof(Bound), kv.second.size(), file_);
  }
  bounds_.clear();
}

void Run() {
  FILE* file = fopen(absl::GetFlag(FLAGS_input).c_str(), "rb");
  Cleaner cleaner(absl::GetFlag(FLAGS_output));

  Bound bound;
  uint32_t last_node_id = 0;
  while (fread(&bound, sizeof(bound), 1, file)) {
    if (last_node_id != bound.node) {
      LOG(INFO) << "Flushing node " << last_node_id;
      cleaner.FlushAndClear();
      last_node_id = bound.node;
    }
    cleaner.Add(bound);
  }
  cleaner.FlushAndClear();
  LOG(INFO) << "Done.";
  fclose(file);
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
