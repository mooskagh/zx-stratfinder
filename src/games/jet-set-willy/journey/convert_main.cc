
#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include "misc/bititer.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"
#include "proto/jet-set-willy.pb.h"

ABSL_FLAG(std::string, input,
          "/home/crem/dev/zx-stratfinder/tmp/data/rooms.bin", "Room.bin file");

ABSL_FLAG(bool, maxlives, false, "Maxlives mode");
ABSL_FLAG(bool, warpless, false, "Nowarp mode");
ABSL_FLAG(bool, rtc, false, "Real time clock instead of IGT");

ABSL_FLAG(std::string, output_ascii, "", "ASCII protobuf of a graph");
ABSL_FLAG(std::string, output, "", "graph file");

namespace jet_set_willy {
namespace {

const uint8_t kItems[] = {
    0,  0xff,                                                    // 0
    1,  0xff,                                                    // 1
    2,  0xff,                                                    // 2
    3,  0xff,                                                    // 3
    4,  6,    7,    0xff,                                        // 4
    5,  0xff,                                                    // 5
    8,  0xff,                                                    // 6
    9,  11,   0xff,                                              // 7
    10, 0xff,                                                    // 8
    12, 0xff,                                                    // 9
    13, 0xff,                                                    // 10
    14, 0xff,                                                    // 11
    15, 0xff,                                                    // 12
    16, 0xff,                                                    // 13
    17, 0xff,                                                    // 14
    18, 0xff,                                                    // 15
    19, 25,   0xff,                                              // 16
    20, 0xff,                                                    // 17
    21, 0xff,                                                    // 18
    22, 0xff,                                                    // 19
    23, 0xff,                                                    // 20
    24, 0xff,                                                    // 21
    27, 0xff,                                                    // 22
    28, 0xff,                                                    // 23
    29, 0xff,                                                    // 24
    30, 0xff,                                                    // 25
    31, 0xff,                                                    // 26
    32, 0xff,                                                    // 27
    33, 0xff,                                                    // 28
    34, 0xff,                                                    // 29
    35, 0xff,                                                    // 30
    36, 0xff,                                                    // 31
    37, 38,   0xff,                                              // 32
    39, 0xff,                                                    // 33
    40, 0xff,                                                    // 34
    41, 42,   0xff,                                              // 35
    43, 0xff,                                                    // 36
    44, 0xff,                                                    // 37
    45, 0xff,                                                    // 38
    46, 0xff,                                                    // 39
    47, 0xff,                                                    // 40
    48, 0xff,                                                    // 41
    49, 53,   0xff,                                              // 42
    50, 0xff,                                                    // 43
    51, 0xff,                                                    // 44
    52, 0xff,                                                    // 45
    54, 0xff,                                                    // 46
    55, 0xff,                                                    // 47
    56, 0xff,                                                    // 48
    57, 0xff,                                                    // 49
    58, 0xff,                                                    // 50
    59, 0xff,                                                    // 51
    60, 0xff,                                                    // 52
    61, 0xff,                                                    // 53
    62, 0xff,                                                    // 54
    63, 0xff,                                                    // 55
    64, 0xff,                                                    // 56
    65, 0xff,                                                    // 57
    66, 0xff,                                                    // 58
    67, 68,   69,   70,   71, 72, 73, 74, 75, 76, 77, 78, 0xff,  // 59
    79, 0xff,                                                    // 60
    80, 0xff,                                                    // 61
    81, 0xff,                                                    // 62
    82, 0xff,                                                    // 63
};

RoomState load_graph() {
  RoomState rooms;
  rooms.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));
  return rooms;
}

struct EdgeKey {
  int destination = 0;
  uint64_t items;
  bool is_death;

  template <typename H>
  friend H AbslHashValue(H h, const EdgeKey& c) {
    return H::combine(std::move(h), c.destination, c.items, c.is_death);
  }

  bool operator==(const EdgeKey& o) const {
    return destination == o.destination && items == o.items &&
           is_death == o.is_death;
  }
};

struct EdgeData {
  int igt_cost;
  std::vector<int> rtc_cost;
  std::string label;
};

uint64_t get_items_val(const Room& room, const uint64_t in_room_mask) {
  if (!in_room_mask) return 0;
  absl::flat_hash_set<int> items;
  for (const auto& x : IterateBits(in_room_mask)) items.insert(room.item(x));
  int cur_idx = 0;
  bool matches = true;
  uint64_t res = 0;
  for (const auto x : kItems) {
    if (x == 0xff) {
      if (matches) res |= (1ULL << cur_idx);
      matches = true;
      ++cur_idx;
      continue;
    }
    if (!items.contains(x)) matches = false;
  }
  return res;
}

graph::GraphDef extract_graph(const RoomState& rooms) {
  using RoomEntry = std::pair<int, int>;
  absl::flat_hash_map<RoomEntry, int> room_entry_to_node;
  std::vector<RoomEntry> node_to_room_entry;

  for (const auto& room : rooms.room()) {
    for (const auto& entryway : room.entryway()) {
      if (entryway.status() != Entryway::DONE) continue;
      int id = room_entry_to_node.size();
      room_entry_to_node[RoomEntry(room.id(), entryway.id())] = id;
      node_to_room_entry.push_back(RoomEntry(room.id(), entryway.id()));
    }
  }

  graph::GraphDef graph;
  const int lives = absl::GetFlag(FLAGS_maxlives) ? 1 : 7;
  for (int i = 0; i <= lives; ++i) {
    for (size_t j = 0; j < node_to_room_entry.size(); ++j) {
      const auto& entry = node_to_room_entry[j];
      auto* node = graph.add_node();
      node->set_label(std::to_string(entry.first) + ":" +
                      std::to_string(entry.second) +
                      " Lives:" + std::to_string(7 - i));
    }
  }

  const int total_nodes = room_entry_to_node.size();
  for (const auto& room : rooms.room()) {
    for (const auto& entryway : room.entryway()) {
      if (entryway.status() != Entryway::DONE) continue;
      const int node_idx =
          room_entry_to_node.at(RoomEntry(room.id(), entryway.id()));
      absl::flat_hash_map<EdgeKey, EdgeData> edges;
      for (const auto& entrypoint : entryway.entrypoint()) {
        for (const auto& route : entrypoint.route()) {
          LOG(INFO) << (std::to_string(room.id()) + ":" +
                        std::to_string(entryway.id()) + "/" +
                        std::to_string(entrypoint.id()) + "." +
                        std::to_string(route.id()));
          EdgeKey key;
          key.is_death = route.is_death();
          key.items = get_items_val(room, route.items_collected());
          if (route.is_death()) {
            if (absl::GetFlag(FLAGS_maxlives) &&
                (key.items & (1ull << 6)) == 0) {
              continue;
            }
          } else {
            key.destination = room_entry_to_node.at(
                RoomEntry(route.destination_addr().room(),
                          route.destination_addr().entryway()));
            if (absl::GetFlag(FLAGS_warpless) &&
                ((room.id() == 14 && route.destination_addr().room() == 20) ||
                 (room.id() == 18 && route.destination_addr().room() == 18) ||
                 (room.id() == 50 && route.destination_addr().room() == 0) ||
                 (room.id() == 58 && route.destination_addr().room() == 58))) {
              continue;
            }
          }

          int igt_cost = static_cast<int>(route.input_length());
          if (!edges.contains(key) || igt_cost < edges[key].igt_cost) {
            edges[key] = EdgeData{
                igt_cost,
                {route.length_tstates().begin(), route.length_tstates().end()},
                std::to_string(room.id()) + ":" +
                    std::to_string(entryway.id()) + "/" +
                    std::to_string(entrypoint.id()) + "." +
                    std::to_string(route.id())};
          }
        }
      }
      for (int i = 0; i <= lives; ++i) {
        for (const auto& e : edges) {
          if (i == lives && e.first.is_death) continue;
          auto* edge = graph.add_edge();
          edge->set_from_node(i * total_nodes + node_idx);
          edge->set_to_node(e.first.is_death
                                ? ((i + 1) * total_nodes + node_idx)
                                : (i * total_nodes + e.first.destination));
          edge->set_items(e.first.items);
          if (absl::GetFlag(FLAGS_rtc)) {
            size_t live_idx = 7 - i;
            CHECK(e.second.rtc_cost.size() > live_idx);
            edge->set_cost(e.second.rtc_cost[live_idx]);
          } else {
            edge->set_cost(e.second.igt_cost);
          }
          edge->set_label(e.second.label);
        }
      }
    }
  }

  graph.add_start_node(room_entry_to_node.at(RoomEntry(33, 0)));
  graph.set_start_items(0);
  graph.set_end_items(0xffffffffffffffffULL);
  for (int i = 1; i <= lives; ++i) {
    for (const auto& entryway : rooms.room(35).entryway()) {
      auto entryway_id = room_entry_to_node.at(RoomEntry(35, entryway.id()));
      graph.add_end_node(i * total_nodes + entryway_id);
    }
  }
  return graph;
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);

  auto graph = jet_set_willy::extract_graph(jet_set_willy::load_graph());

  if (!absl::GetFlag(FLAGS_output).empty()) {
    write_string_to_file(absl::GetFlag(FLAGS_output),
                         graph.SerializeAsString());
  }
  if (!absl::GetFlag(FLAGS_output_ascii).empty()) {
    std::string out;
    CHECK(google::protobuf::TextFormat::PrintToString(graph, &out));
    write_string_to_file(absl::GetFlag(FLAGS_output_ascii), out);
  }

  return 0;
}
