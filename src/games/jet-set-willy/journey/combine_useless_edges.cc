#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstdint>
#include <limits>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");

namespace jet_set_willy {
namespace {
constexpr uint32_t kInf = std::numeric_limits<uint32_t>::max();

void FindMinimumDistances(std::vector<std::vector<uint32_t>>& dist,
                          std::vector<std::vector<uint16_t>>& next) {
  const auto N = dist.size();
  for (size_t k = 0; k < N; ++k) {
    LOG_EVERY_N(INFO, 1000) << "Iteration " << k;
    for (size_t i = 0; i < N; ++i) {
      if (dist[i][k] == kInf) continue;
      for (size_t j = 0; j < N; ++j) {
        if (dist[k][j] == kInf) continue;
        if (dist[i][j] > dist[i][k] + dist[k][j]) {
          dist[i][j] = dist[i][k] + dist[k][j];
          next[i][j] = next[i][k];
        }
      }
    }
  }
}

std::vector<uint16_t> BuildNewIndices(size_t total_count,
                                      const std::vector<uint16_t>& indices) {
  std::vector<uint16_t> result(total_count,
                               std::numeric_limits<uint16_t>::max());
  size_t idx = 0;
  for (const auto& index : indices) result[index] = idx++;
  return result;
}

std::vector<uint16_t> BuildOldIncides(
    const absl::flat_hash_set<uint16_t>& indices) {
  std::vector<uint16_t> result(indices.begin(), indices.end());
  std::sort(result.begin(), result.end());
  return result;
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();

  LOG(INFO) << "distances";
  std::vector<std::vector<uint32_t>> distances(
      graph.node_size(),
      std::vector<uint32_t>(graph.node_size(),
                            std::numeric_limits<uint32_t>::max()));
  LOG(INFO) << "next";
  std::vector<std::vector<uint16_t>> next(
      graph.node_size(),
      std::vector<uint16_t>(graph.node_size(),
                            std::numeric_limits<uint16_t>::max()));
  LOG(INFO) << "Edges";
  absl::flat_hash_map<int, absl::flat_hash_map<int, int>> edge_refs;

  absl::flat_hash_set<uint16_t> final_nodes;
  for (const auto node : graph.start_node()) final_nodes.insert(node);
  for (const auto node : graph.end_node()) final_nodes.insert(node);

  LOG(INFO) << "Initializing nodes";
  for (int i = 0; i < graph.node_size(); ++i) {
    distances[i][i] = 0;
    next[i][i] = i;
  }

  LOG(INFO) << "Initializing edges";
  std::vector<std::vector<int>> reverse_edges(graph.node_size());
  absl::flat_hash_set<int> edges_to_keep;
  for (int e = 0; e < graph.edge_size(); ++e) {
    const auto& edge = graph.edge(e);
    auto i = edge.from_node();
    auto j = edge.to_node();
    auto& cost = distances[i][j];
    if (edge.cost() < cost) {
      cost = edge.cost();
      next[i][j] = j;
      edge_refs[i][j] = e;
    }
    for (const auto& end_node : graph.end_node()) {
      if (j == end_node) {
        edges_to_keep.insert(e);
        break;
      }
    }
    if (edge.items() != 0) final_nodes.insert(j);
    reverse_edges[j].push_back(e);
  }
  for (auto& x : reverse_edges) std::sort(x.begin(), x.end());
  LOG(INFO) << "Final graph will contain nodes=" << final_nodes.size();
  for (const auto x : final_nodes) {
    LOG(INFO) << x << " " << graph.node(x).label();
  }
  FindMinimumDistances(distances, next);
  auto new_to_old_node = BuildOldIncides(final_nodes);
  auto old_to_new_node = BuildNewIndices(graph.node_size(), new_to_old_node);

  LOG(INFO) << "Building output";
  graph::GraphDef out = graph;
  out.clear_start_node();
  out.clear_end_node();
  for (const auto& node : graph.start_node()) {
    out.add_start_node(old_to_new_node[node]);
  }
  for (const auto& node : graph.end_node()) {
    out.add_end_node(old_to_new_node[node]);
  }
  LOG(INFO) << "Building nodes";
  out.clear_node();
  for (const auto node_idx : new_to_old_node) {
    *out.add_node() = graph.node(node_idx);
  }
  LOG(INFO) << "Checking which of empty edges are required";
  for (auto i : new_to_old_node) {
    LOG(INFO) << "From_node " << i;
    for (auto j : new_to_old_node) {
      std::set<uint16_t> ks;
      for (auto e : reverse_edges[j]) {
        const auto& edge = graph.edge(e);
        if (distances[i][edge.from_node()] == kInf) continue;
        if (edge.items() == 0) continue;
        ks.insert(edge.from_node());
      }
      for (auto k : ks) {
        uint16_t u = i;
        while (u != k) {
          const auto n = next[u][k];
          if (final_nodes.contains(n)) {
            edges_to_keep.insert(edge_refs[u][n]);
          }
          u = n;
        }
      }
    }
  }

  LOG(INFO) << "Reconstructing edges";
  out.clear_edge();
  size_t dropped_edges = 0;
  for (auto i : new_to_old_node) {
    LOG(INFO) << "From_node " << i << " " << graph.node(i).label();
    for (auto j : new_to_old_node) {
      // LOG(INFO) << "  To_node " << j << " " << graph.node(j).label();
      struct EdgeData {
        uint32_t cost;
        uint64_t items;
        std::string label;
      };
      std::vector<EdgeData> new_edges;
      for (auto e : reverse_edges[j]) {
        const auto& edge = graph.edge(e);
        if (distances[i][edge.from_node()] == kInf) continue;
        if (edge.items() == 0 && !edges_to_keep.contains(e)) continue;
        uint32_t cost = distances[i][edge.from_node()] + edge.cost();
        uint64_t items = edge.items();
        std::string label;
        uint16_t u = i;
        bool drop_node = false;
        while (u != edge.from_node()) {
          const auto n = next[u][edge.from_node()];
          if (final_nodes.contains(n)) {
            drop_node = true;
            break;
          }
          label += graph.edge(edge_refs[u][n]).label() + " ";
          u = n;
        }
        if (drop_node) {
          ++dropped_edges;
          continue;
        }
        label += edge.label();
        bool need_insert = true;
        new_edges.erase(
            std::remove_if(
                new_edges.begin(), new_edges.end(),
                [&](const EdgeData& data) {
                  if (data.cost < cost && (data.items & items) == items) {
                    need_insert = false;
                  }
                  if (data.cost >= cost && (data.items & items) == data.items) {
                    ++dropped_edges;
                    return true;
                  }
                  return false;
                }),
            new_edges.end());
        if (need_insert) {
          new_edges.push_back(EdgeData{cost, items, label});
        }
      }
      for (const auto& edge : new_edges) {
        /* LOG(INFO) << "    F:" << graph.node(i).label()
                  << " T:" << graph.node(j).label() << " Adding " << edge.items
                  << " " << edge.cost << " " << edge.label; */
        auto* e = out.add_edge();
        e->set_from_node(old_to_new_node[i]);
        e->set_to_node(old_to_new_node[j]);
        e->set_items(edge.items);
        e->set_cost(edge.cost);
        e->set_label(edge.label);
      }
    }
  }
  LOG(INFO) << "Edges in the end=" << out.edge_size()
            << " dropped=" << dropped_edges;

  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    write_string_to_file(absl::GetFlag(FLAGS_output), out.SerializeAsString());
    LOG(INFO) << "nodes=" << out.node_size();
    LOG(INFO) << "edges=" << out.edge_size();
  }

  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
