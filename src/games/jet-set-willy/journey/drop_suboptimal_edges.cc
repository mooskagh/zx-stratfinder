#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstdint>
#include <limits>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");

namespace jet_set_willy {
namespace {

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();

  LOG(INFO) << "Initializing edges";
  std::vector<std::vector<int>> reverse_edges(graph.node_size());
  absl::flat_hash_set<int> edges_to_keep;
  for (int e = 0; e < graph.edge_size(); ++e) {
    const auto& edge = graph.edge(e);
    for (const auto& end_node : graph.end_node()) {
      if (edge.to_node() == end_node) {
        edges_to_keep.insert(e);
        break;
      }
    }
    reverse_edges[edge.to_node()].push_back(e);
  }
  for (auto& x : reverse_edges) std::sort(x.begin(), x.end());

  LOG(INFO) << "Checking which of non-empty edges are required";
  for (int j = 0; j < graph.node_size(); ++j) {
    LOG(INFO) << "To_node: " << j << " " << graph.node(j).label()
              << " size=" << reverse_edges[j].size();
    struct EdgeData {
      int first_edge;
      int second_edge;
      uint32_t cost;
      uint64_t items;
    };
    absl::flat_hash_map<uint16_t, std::vector<EdgeData>> edge_datas;
    for (auto ek : reverse_edges[j]) {
      const auto& edge_k = graph.edge(ek);
      // if (edge_k.items() == 0) continue;
      int k = edge_k.from_node();
      for (auto ei : reverse_edges[k]) {
        const auto& edge_i = graph.edge(ei);
        uint32_t cost = edge_k.cost() + edge_i.cost();
        uint64_t items = edge_k.items() + edge_i.items();
        auto& edge_data = edge_datas[edge_i.from_node()];
        bool need_insert = true;
        edge_data.erase(
            std::remove_if(
                edge_data.begin(), edge_data.end(),
                [&](const EdgeData& data) {
                  if (data.cost < cost && (data.items & items) == items) {
                    need_insert = false;
                  }

                  if (data.cost >= cost && (data.items & items) == data.items) {
                    return true;
                  }
                  return false;
                }),
            edge_data.end());
        if (need_insert) {
          edge_data.push_back(EdgeData{ei, ek, cost, items});
        }
      }
    }
    for (const auto& edge_data : edge_datas) {
      for (const auto& x : edge_data.second) {
        edges_to_keep.insert(x.first_edge);
      }
    }
  }

  LOG(INFO) << edges_to_keep.size() << " edges to keep";
  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    graph::GraphDef out = graph;
    out.clear_edge();
    for (int i = 0; i < graph.edge_size(); ++i) {
      if (edges_to_keep.contains(i)) {
        *out.add_edge() = graph.edge(i);
      }
    }
    LOG(INFO) << "nodes=" << out.node_size();
    LOG(INFO) << "edges=" << out.edge_size();
    write_string_to_file(absl::GetFlag(FLAGS_output), out.SerializeAsString());
  }

  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
