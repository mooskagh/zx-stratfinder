#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <algorithm>
#include <cstdint>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "misc/bititer.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");
ABSL_FLAG(int, nodes_to_kill, 0, "nodes_to_kill");
ABSL_FLAG(int, max_items, 10, "max_items");
ABSL_FLAG(int, mode, 0, "0=only remove selfwrap");

// 0 only remove selfwrap
// 1 remove -1,-1
// 2 remove incoming -1
// 3 remove outgoing -1
// 4 remove x,x
// 5 remove max(x,y)
// 6 remove in-milicluster nodes.
// 7 remove in-cluster nodes.
// 8 remove in-megacluster nodes.

namespace jet_set_willy {
namespace {

constexpr uint64_t kClustersV1[] = {
    (1ull << 4) | (1ull << 5),
    (1ull << 14) | (1ull << 15) | (1ull << 16),
    (1ull << 54) | (1ull << 55) | (1ull << 56) | (1ull << 57) | (1ull << 58) |
        (1ull << 49) | (1ull << 35),
    (1ull << 33) | (1ull << 34) | (1ull << 60),
    (1ull << 31) | (1ull << 32) | (1ull << 46) | (1ull << 47),
    (1ull << 50) | (1ull << 51) | (1ull << 52),
    (1ull << 0) | (1ull << 1) | (1ull << 2) | (1ull << 3),
    (1ull << 10) | (1ull << 12) | (1ull << 13),
    (1ull << 11) | (1ull << 42) | (1ull << 48),
    (1ull << 6) | (1ull << 7) | (1ull << 8),
    (1ull << 17) | (1ull << 18) | (1ull << 19),
    (1ull << 23) | (1ull << 24) | (1ull << 25) | (1ull << 26),
    (1ull << 37) | (1ull << 38) | (1ull << 39) | (1ull << 40) | (1ull << 41) |
        (1ull << 44) | (1ull << 45),
    (1ull << 28) | (1ull << 29),
    (1ull << 30) | (1ull << 36)};

constexpr uint64_t kClustersV2[] = {
    (1ull << 4) | (1ull << 5),
    (1ull << 14) | (1ull << 15) | (1ull << 16),
    (1ull << 54) | (1ull << 55) | (1ull << 56) | (1ull << 57) | (1ull << 58) |
        (1ull << 49) | (1ull << 35),
    (1ull << 31) | (1ull << 32) | (1ull << 33) | (1ull << 34) | (1ull << 46) |
        (1ull << 47) | (1ull << 50) | (1ull << 51) | (1ull << 52) |
        (1ull << 60),
    (1ull << 0) | (1ull << 1) | (1ull << 2) | (1ull << 3) | (1ull << 10) |
        (1ull << 11) | (1ull << 12) | (1ull << 13) | (1ull << 42) |
        (1ull << 48),
    (1ull << 6) | (1ull << 7) | (1ull << 8) | (1ull << 17) | (1ull << 18) |
        (1ull << 19),
    (1ull << 23) | (1ull << 24) | (1ull << 25) | (1ull << 26),
    (1ull << 37) | (1ull << 38) | (1ull << 39) | (1ull << 40) | (1ull << 41) |
        (1ull << 44) | (1ull << 45),
    (1ull << 28) | (1ull << 29),
    (1ull << 30) | (1ull << 36)};

constexpr uint64_t kClustersV3[] = {
    (1ull << 0) | (1ull << 1) | (1ull << 2) | (1ull << 3) | (1ull << 10) |
        (1ull << 11) | (1ull << 12) | (1ull << 13) | (1ull << 42) |
        (1ull << 48),
    (1ull << 31) | (1ull << 32) | (1ull << 33) | (1ull << 34) | (1ull << 46) |
        (1ull << 47) | (1ull << 50) | (1ull << 51) | (1ull << 52) |
        (1ull << 59) | (1ull << 60),
    (1ull << 27) | (1ull << 53) | (1ull << 54) | (1ull << 55) | (1ull << 56) |
        (1ull << 57) | (1ull << 58) | (1ull << 49) | (1ull << 35) |
        (1ull << 14) | (1ull << 15) | (1ull << 16),
    (1ull << 23) | (1ull << 24) | (1ull << 25) | (1ull << 26) | (1ull << 9) |
        (1ull << 62) | (1ull << 20) | (1ull << 63) | (1ull << 22) |
        (1ull << 61) | (1ull << 28) | (1ull << 29),
    (1ull << 37) | (1ull << 38) | (1ull << 39) | (1ull << 40) | (1ull << 41) |
        (1ull << 43) | (1ull << 44) | (1ull << 45) | (1ull << 30) |
        (1ull << 36),
    (1ull << 4) | (1ull << 5) | (1ull << 6) | (1ull << 7) | (1ull << 8) |
        (1ull << 17) | (1ull << 18) | (1ull << 19) | (1ull << 21)};

class EdgeSet {
 public:
  const std::vector<graph::Edge>& get() const { return edges_; }
  void add(const graph::Edge& edge_to_insert) {
    if ((edge_to_insert.items() & items_) == edge_to_insert.items()) {
      if (edge_to_insert.cost() >= min_cost_) {
        ++dropped_;
        return;
      }
      if (items_ == edge_to_insert.items()) {
        min_cost_ = edge_to_insert.cost();
      }
    } else {
      items_ |= edge_to_insert.items();
      min_cost_ = (items_ == edge_to_insert.items())
                      ? edge_to_insert.cost()
                      : std::numeric_limits<uint32_t>::max();
    }

    bool need_insert = true;
    edges_.erase(
        std::remove_if(edges_.begin(), edges_.end(),
                       [&](const graph::Edge& existing_edge) {
                         if (existing_edge.cost() < edge_to_insert.cost() &&
                             (existing_edge.items() & edge_to_insert.items()) ==
                                 edge_to_insert.items()) {
                           need_insert = false;
                         }

                         if (existing_edge.cost() >= edge_to_insert.cost() &&
                             (existing_edge.items() & edge_to_insert.items()) ==
                                 existing_edge.items()) {
                           ++dropped_;
                           return true;
                         }
                         return false;
                       }),
        edges_.end());
    if (need_insert) {
      edges_.push_back(edge_to_insert);
    } else {
      ++dropped_;
    }
  };

  size_t dropped() const { return dropped_; }
  size_t inserted() const { return edges_.size(); }

 private:
  std::vector<graph::Edge> edges_;
  size_t dropped_ = 0;
  uint64_t items_ = 0;
  uint32_t min_cost_ = std::numeric_limits<uint32_t>::max();
};

class EdgeCollector {
 public:
  EdgeCollector(const std::vector<graph::Edge>& self_edges,
                const std::vector<graph::Edge>& outgoing_edges)
      : outgoing_edges_(outgoing_edges) {
    for (const auto& edge : self_edges) {
      all_items_ |= edge.items();
    }
    absl::flat_hash_map<uint64_t, graph::Edge> best_edges;
    for (const auto& edge : self_edges_) {
      auto iter = best_edges.find(edge.items());
      if (iter == best_edges.end()) {
        best_edges.emplace(edge.items(), edge);
      } else if (iter->second.cost() > edge.cost()) {
        iter->second = edge;
      }
    }
    for (const auto& kv : best_edges) self_edges_.push_back(kv.second);
  }

  void Run() {
    if (outgoing_edges_.empty()) return;
    graph::Edge edge;
    edge.set_from_node(outgoing_edges_[0].from_node());
    RunInternal(edge, 0);
  }

  const absl::flat_hash_map<int, EdgeSet>& get() const {
    return to_node_to_edges_;
  }

  size_t dropped() const {
    size_t res = 0;
    for (const auto& x : to_node_to_edges_) res += x.second.dropped();
    return res;
  }

  size_t inserted() const {
    size_t res = 0;
    for (const auto& x : to_node_to_edges_) res += x.second.inserted();
    return res;
  }

 private:
  void RunInternal(graph::Edge edge, size_t idx) {
    if (idx == self_edges_.size() || edge.items() == all_items_) {
      TryAppend(edge);
      return;
    }
    RunInternal(edge, idx + 1);
    edge.set_items(edge.items() | self_edges_[idx].items());
    edge.set_cost(edge.cost() + self_edges_[idx].cost());
    edge.set_label(edge.label() + self_edges_[idx].label() + " ");
    RunInternal(edge, idx + 1);
  }

  void TryAppend(const graph::Edge& prefix) {
    LOG_EVERY_N(INFO, 100000) << count_ << "/" << (1ULL << self_edges_.size());
    ++count_;
    for (auto edge : outgoing_edges_) {
      edge.set_from_node(prefix.from_node());
      edge.set_items(edge.items() | prefix.items());
      edge.set_cost(edge.cost() + prefix.cost());
      edge.set_label(prefix.label() + edge.label());
      to_node_to_edges_[edge.to_node()].add(edge);
    }
  }

  std::vector<graph::Edge> self_edges_;
  const std::vector<graph::Edge>& outgoing_edges_;
  absl::flat_hash_map<int, EdgeSet> to_node_to_edges_;
  size_t count_ = 0;
  uint64_t all_items_ = 0;
};

void UnwrapLoops(graph::GraphDef& graph, std::optional<uint32_t> which = {}) {
  LOG(INFO) << "Loading edges";
  std::vector<std::vector<graph::Edge>> node_to_edges(graph.node_size());
  std::vector<std::vector<graph::Edge>> node_to_self_edges(graph.node_size());
  for (const auto& edge : graph.edge()) {
    if (edge.from_node() != edge.to_node()) {
      node_to_edges[edge.from_node()].push_back(edge);
    } else if (edge.items() != 0 && (!which || *which == edge.from_node())) {
      node_to_self_edges[edge.from_node()].push_back(edge);
    }
  }

  LOG(INFO) << "Merging self-loops.";
  graph.clear_edge();
  std::vector<absl::flat_hash_map<int, EdgeSet>> node_to_node_to_edges(
      graph.node_size());
  for (int i = 0; i < graph.node_size(); ++i) {
    const auto& edges = node_to_self_edges[i];
    if (edges.size() > 0) {
      LOG(INFO) << i << " " << graph.node(i).label() << " " << edges.size();
    }
    for (const auto& edge : edges) {
      std::string bitset = "[";
      bool first = true;
      for (const auto& x : IterateBits(edge.items())) {
        if (!first) bitset += " ";
        first = false;
        bitset += std::to_string(x);
      }
      bitset += "]";

      LOG(INFO) << "    items=" << bitset << " ("
                << __builtin_popcountll(edge.items())
                << ") cost=" << edge.cost() << " label=" << edge.label();
    }
    EdgeCollector collector(node_to_self_edges[i], node_to_edges[i]);
    collector.Run();
    if (collector.dropped() != 0 ||
        node_to_edges[i].size() != collector.inserted()) {
      LOG(INFO) << " before=" << node_to_edges[i].size()
                << " after=" << collector.inserted()
                << " dropped=" << collector.dropped();
    }
    for (const auto& x : collector.get()) {
      for (const auto& y : x.second.get()) *graph.add_edge() = y;
    }
  }
}

void RemoveOneNode(graph::GraphDef& graph, int node_idx) {
  auto new_node_idx = [node_idx](int idx) {
    CHECK(idx != node_idx);
    if (idx > node_idx) return idx - 1;
    return idx;
  };

  LOG(INFO) << "Removing node " << node_idx << " "
            << graph.node(node_idx).label();

  graph::GraphDef out = graph;
  out.clear_node();
  for (int i = 0; i < graph.node_size(); ++i) {
    if (i != node_idx) *out.add_node() = graph.node(i);
  }
  absl::flat_hash_map<int, std::vector<graph::Edge>>
      source_node_to_incoming_edges;
  std::vector<graph::Edge> outgoing_edges;
  out.clear_edge();
  for (auto edge : graph.edge()) {
    if (static_cast<int>(edge.to_node()) == node_idx) {
      CHECK(static_cast<int>(edge.from_node()) != node_idx);
      source_node_to_incoming_edges[edge.from_node()].push_back(edge);
    } else if (static_cast<int>(edge.from_node()) == node_idx) {
      outgoing_edges.push_back(edge);
    } else {
      edge.set_from_node(new_node_idx(edge.from_node()));
      edge.set_to_node(new_node_idx(edge.to_node()));
      *out.add_edge() = edge;
    }
  }

  size_t inserted = 0;
  size_t dropped = 0;
  for (const auto& kv : source_node_to_incoming_edges) {
    absl::flat_hash_map<int, EdgeSet> destination_to_edges;
    for (const auto& in_edge : kv.second) {
      for (const auto& out_edge : outgoing_edges) {
        graph::Edge edge;
        edge.set_from_node(new_node_idx(in_edge.from_node()));
        edge.set_to_node(new_node_idx(out_edge.to_node()));
        edge.set_items(in_edge.items() | out_edge.items());
        edge.set_cost(in_edge.cost() + out_edge.cost());
        edge.set_label(in_edge.label() + " " + out_edge.label());
        destination_to_edges[out_edge.to_node()].add(edge);
      }
    }
    for (const auto& kvv : destination_to_edges) {
      for (const auto& edge : kvv.second.get()) {
        *out.add_edge() = edge;
      }
      inserted += kvv.second.inserted();
      dropped += kvv.second.dropped();
    }
  }
  LOG(INFO) << "   While deleting edge, inserted=" << inserted
            << " dropped=" << dropped;

  out.clear_start_node();
  for (auto node : graph.start_node()) out.add_start_node(new_node_idx(node));
  out.clear_end_node();
  for (auto node : graph.end_node()) out.add_end_node(new_node_idx(node));

  graph = std::move(out);
}

std::vector<int> GetNodesToRemove(const graph::GraphDef& graph, int mode) {
  std::vector<int> item_to_set;
  for (int i = 0; i < 64; ++i) item_to_set.push_back(i);
  auto get_parent = [&](int i) {
    if (i < 0) return i;
    while (item_to_set[i] != i) i = item_to_set[i];
    return i;
  };

  std::vector<std::vector<graph::Edge>> incoming_edges(graph.node_size());
  std::vector<std::vector<graph::Edge>> outgoing_edges(graph.node_size());
  std::vector<uint64_t> incoming_edge_mask(graph.node_size());
  std::vector<uint64_t> outgoing_edge_mask(graph.node_size());
  for (const auto& edge : graph.edge()) {
    incoming_edge_mask[edge.to_node()] |= edge.items();
    outgoing_edge_mask[edge.from_node()] |= edge.items();
    incoming_edges[edge.to_node()].push_back(edge);
    outgoing_edges[edge.from_node()].push_back(edge);
  }

  std::vector<int> res;
  absl::flat_hash_set<uint16_t> final_nodes;
  for (const auto node : graph.start_node()) final_nodes.insert(node);
  for (const auto node : graph.end_node()) final_nodes.insert(node);

  if (mode == 6) {
    for (size_t i = 0; i < incoming_edge_mask.size(); ++i) {
      if (final_nodes.contains(i)) continue;
      const uint64_t imask = incoming_edge_mask[i];
      const uint64_t omask = outgoing_edge_mask[i];
      if (imask == 0 || omask == 0) continue;
      for (uint64_t cluster : kClustersV1) {
        if ((imask | omask) == ((imask | omask) & cluster)) {
          res.push_back(i);
          break;
        }
      }
    }
    return res;
  }

  if (mode == 7) {
    for (size_t i = 0; i < incoming_edge_mask.size(); ++i) {
      if (final_nodes.contains(i)) continue;
      const uint64_t imask = incoming_edge_mask[i];
      const uint64_t omask = outgoing_edge_mask[i];
      if (imask == 0 || omask == 0) continue;
      for (uint64_t cluster : kClustersV2) {
        if ((imask | omask) == ((imask | omask) & cluster)) {
          res.push_back(i);
          break;
        }
      }
    }
    return res;
  }

  if (mode == 8) {
    for (size_t i = 0; i < incoming_edge_mask.size(); ++i) {
      if (final_nodes.contains(i)) continue;
      const uint64_t imask = incoming_edge_mask[i];
      const uint64_t omask = outgoing_edge_mask[i];
      if (imask == 0 || omask == 0) continue;
      for (uint64_t cluster : kClustersV3) {
        if ((imask | omask) == ((imask | omask) & cluster)) {
          res.push_back(i);
          break;
        }
      }
    }
    return res;
  }

  auto gather_edges = [&](const std::vector<std::vector<graph::Edge>>& edges,
                          std::vector<int>& sets) {
    for (size_t i = 0; i < sets.size(); ++i) {
      int parent = -1;
      for (const auto& edge : edges[i]) {
        for (auto bit : IterateBits(edge.items())) {
          if (parent == -1) {
            parent = get_parent(bit);
            continue;
          }
          if (get_parent(bit) == parent) continue;
        }
      }
      sets[i] = parent;
    }
  };
  std::vector<int> node_incoming_set(graph.node_size(), -1);
  std::vector<int> node_outgoing_set(graph.node_size(), -1);
  gather_edges(incoming_edges, node_incoming_set);
  gather_edges(outgoing_edges, node_outgoing_set);

  absl::flat_hash_map<std::pair<int, int>, int> sets_to_counts;
  for (size_t i = 0; i < node_incoming_set.size(); ++i) {
    auto x = get_parent(node_incoming_set[i]);
    auto y = get_parent(node_outgoing_set[i]);
    ++sets_to_counts[std::make_pair(std::min(x, y), std::max(x, y))];
    if (final_nodes.contains(i)) continue;
    switch (mode) {
      case 1:
        if (x == -1 && y == -1) res.push_back(i);
        break;
      case 2:
        if (x == -1) res.push_back(i);
        break;
      case 3:
        if (y == -1) res.push_back(i);
        break;
      case 4:
        if (x != -1 || x == y) res.push_back(i);
        break;
    }
  }

  if (mode == 5) {
    int max = -1;
    auto max_val = std::make_pair(-1, -1);
    for (const auto& kv : sets_to_counts) {
      LOG(INFO) << kv.first.first << "," << kv.first.second << "  -> "
                << kv.second;
      if (kv.second > max) {
        max = kv.second;
        max_val = kv.first;
      }
    }

    for (size_t i = 0; i < node_incoming_set.size(); ++i) {
      auto x = get_parent(node_incoming_set[i]);
      auto y = get_parent(node_outgoing_set[i]);
      if (final_nodes.contains(i)) continue;
      if (max_val == std::make_pair(std::min(x, y), std::max(x, y))) {
        res.push_back(i);
      }
    }
  }

  return res;
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));
  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();
  UnwrapLoops(graph);

  std::vector<int> nodes_to_kill =
      GetNodesToRemove(graph, absl::GetFlag(FLAGS_mode));
  LOG(INFO) << nodes_to_kill.size() << " nodes to remove from "
            << graph.node_size();

  std::sort(nodes_to_kill.rbegin(), nodes_to_kill.rend());

  for (int node : nodes_to_kill) {
    LOG(INFO) << "Killing node " << node << " " << graph.node(node).label();
    LOG(INFO) << "nodes=" << graph.node_size();
    LOG(INFO) << "edges=" << graph.edge_size();
    UnwrapLoops(graph, node);
    RemoveOneNode(graph, node);
  }
  UnwrapLoops(graph);

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();
  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    write_string_to_file(absl::GetFlag(FLAGS_output),
                         graph.SerializeAsString());
  }
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
