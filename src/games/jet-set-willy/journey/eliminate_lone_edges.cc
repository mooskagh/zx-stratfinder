
#include <absl/container/flat_hash_set.h>
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include <cstdint>
#include <limits>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "misc/filebuffer.h"
#include "proto/graph.pb.h"

ABSL_FLAG(std::string, input, "", "graph file");
ABSL_FLAG(std::string, output, "", "output file to write");
ABSL_FLAG(bool, only_dead_ends, false, "only remove dead ends");

namespace jet_set_willy {
namespace {

std::vector<uint16_t> BuildOldIncides(
    size_t total_count, const absl::flat_hash_set<uint16_t>& indices_to_drop) {
  std::vector<uint16_t> result;
  for (size_t i = 0; i < total_count; ++i) {
    if (indices_to_drop.contains(i)) continue;
    result.push_back(i);
  }
  return result;
}

std::vector<uint16_t> BuildNewIndices(size_t total_count,
                                      const std::vector<uint16_t>& indices) {
  std::vector<uint16_t> result(total_count,
                               std::numeric_limits<uint16_t>::max());
  size_t idx = 0;
  for (const auto& index : indices) result[index] = idx++;
  return result;
}

void Run() {
  graph::GraphDef graph;
  graph.ParseFromString(read_file_to_string(absl::GetFlag(FLAGS_input)));

  LOG(INFO) << "nodes=" << graph.node_size();
  LOG(INFO) << "edges=" << graph.edge_size();

  absl::flat_hash_set<uint16_t> indices_to_drop;

  LOG(INFO) << "Counting edges";
  std::vector<int> in_edges(graph.node_size());
  std::vector<int> out_edges(graph.node_size());
  std::vector<int> in_edge_idx(graph.node_size());
  std::vector<int> out_edge_idx(graph.node_size());
  for (int i = 0; i < graph.edge_size(); ++i) {
    const auto& edge = graph.edge(i);
    ++in_edges[edge.to_node()];
    ++out_edges[edge.from_node()];
    in_edge_idx[edge.to_node()] = i;
    out_edge_idx[edge.from_node()] = i;
  }
  for (int i = 0; i < graph.node_size(); ++i) {
    if (absl::GetFlag(FLAGS_only_dead_ends)) {
      if (in_edges[i] < 1 || out_edges[i] < 1) indices_to_drop.insert(i);
    } else {
      if (in_edges[i] < 2 || out_edges[i] < 2) indices_to_drop.insert(i);
    }
  }
  for (const auto& edge : graph.edge()) {
    if (indices_to_drop.contains(edge.from_node()) &&
        indices_to_drop.contains(edge.to_node())) {
      indices_to_drop.erase(edge.to_node());
    }
  }
  for (const auto node : graph.start_node()) indices_to_drop.erase(node);
  for (const auto node : graph.end_node()) indices_to_drop.erase(node);
  auto new_to_old_node = BuildOldIncides(graph.node_size(), indices_to_drop);
  auto old_to_new_node = BuildNewIndices(graph.node_size(), new_to_old_node);

  LOG(INFO) << "Removing edges";
  graph::GraphDef out = graph;
  out.clear_start_node();
  out.clear_end_node();
  for (const auto& node : graph.start_node()) {
    out.add_start_node(old_to_new_node[node]);
  }
  for (const auto& node : graph.end_node()) {
    out.add_end_node(old_to_new_node[node]);
  }
  out.clear_node();
  for (const auto node_idx : new_to_old_node) {
    *out.add_node() = graph.node(node_idx);
  }

  out.clear_edge();
  for (const auto& edge : graph.edge()) {
    if (indices_to_drop.contains(edge.from_node()) &&
        out_edges[edge.from_node()] == 1) {
      LOG(INFO) << "Dropping F " << edge.label() << " -> "
                << graph.node(edge.to_node()).label();
      CHECK(!indices_to_drop.contains(edge.to_node()));
      continue;
    }
    if (indices_to_drop.contains(edge.to_node()) &&
        in_edges[edge.to_node()] == 1) {
      LOG(INFO) << "Dropping T " << edge.label() << " -> "
                << graph.node(edge.to_node()).label();
      CHECK(!indices_to_drop.contains(edge.from_node()));
      continue;
    }
    auto* new_edge = out.add_edge();
    *new_edge = edge;
    if (indices_to_drop.contains(edge.to_node()) &&
        out_edges[edge.to_node()] == 1) {
      const auto& aux_edge = graph.edge(out_edge_idx[edge.to_node()]);
      new_edge->set_to_node(aux_edge.to_node());
      new_edge->set_items(aux_edge.items() | edge.items());
      new_edge->set_cost(aux_edge.cost() + edge.cost());
      new_edge->set_label(edge.label() + " " + aux_edge.label());
    }
    if (indices_to_drop.contains(edge.from_node()) &&
        in_edges[edge.from_node()] == 1) {
      const auto& aux_edge = graph.edge(in_edge_idx[edge.from_node()]);
      new_edge->set_from_node(aux_edge.from_node());
      new_edge->set_items(aux_edge.items() | edge.items());
      new_edge->set_cost(aux_edge.cost() + edge.cost());
      new_edge->set_label(aux_edge.label() + " " + edge.label());
    }
    new_edge->set_from_node(old_to_new_node[new_edge->from_node()]);
    new_edge->set_to_node(old_to_new_node[new_edge->to_node()]);
  }

  LOG(INFO) << "nodes=" << out.node_size();
  LOG(INFO) << "edges=" << out.edge_size();
  if (!absl::GetFlag(FLAGS_output).empty()) {
    LOG(INFO) << "Writing result";
    write_string_to_file(absl::GetFlag(FLAGS_output), out.SerializeAsString());
  }

  LOG(INFO) << "Done.";
}

}  // namespace
}  // namespace jet_set_willy

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);
  jet_set_willy::Run();

  return 0;
}
