#pragma once

#include <glog/logging.h>

#include <array>
#include <bitset>
#include <cstdint>
#include <vector>

#include "src/misc/bititer.h"

namespace jet_set_willy {

class BitPermutation {
 public:
  BitPermutation(uint64_t mask, size_t num_bits) {
    for (auto bit : IterateBits(mask)) {
      bits_.push_back(bit);
    }
    if (bits_.size() < num_bits) {
      done_ = true;
      return;
    }
    for (size_t i = 0; i < num_bits; ++i) {
      offsets_.push_back(i);
      cur_val_ |= (1ULL << bits_[i]);
    }
  }

  uint64_t get() const { return cur_val_; }

  void next() {
    if (bits_.empty()) {
      done_ = true;
      return;
    }
    int idx = static_cast<int>(offsets_.size()) - 1;
    int bit_from_back = 1;
    while (idx >= 0 && offsets_[idx] >= bits_.size() - bit_from_back) {
      cur_val_ ^= (1ULL << bits_[offsets_[idx]]);
      --idx;
      ++bit_from_back;
    }
    if (idx < 0) {
      done_ = true;
      return;
    }
    cur_val_ ^= (1ULL << bits_[offsets_[idx]]);
    ++offsets_[idx];
    cur_val_ ^= (1ULL << bits_[offsets_[idx]]);
    for (size_t i = idx + 1; i < offsets_.size(); ++i) {
      offsets_[i] = offsets_[i - 1] + 1;
      cur_val_ ^= (1ULL << bits_[offsets_[i]]);
    }
  }

  bool done() const { return done_; }

 private:
  uint64_t cur_val_ = 0;
  std::vector<uint8_t> bits_;
  std::vector<uint8_t> offsets_;
  bool done_ = false;
};

}  // namespace jet_set_willy