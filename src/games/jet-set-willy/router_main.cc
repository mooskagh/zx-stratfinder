#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>

#include "games/jet-set-willy/routing.h"

int main(int argc, char* argv[]) {
  absl::ParseCommandLine(argc, argv);
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;

  jet_set_willy::Routing routing;
  routing.run();
  return 0;
}