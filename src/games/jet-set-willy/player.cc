#include <fstream>
#include <memory>
#include <set>
#include <sstream>

#include "games/jet-set-willy.h"
#include "proto/jet-set-willy.pb.h"
#include "search/search.h"
namespace jet_set_willy {
namespace {

const char* kRoomNames[] = {"The Off Licence",
                            "The Bridge ",
                            "Under the MegaTree ",
                            "At the Foot of the MegaTree",
                            "The Drive",
                            "The Security Guard ",
                            "Entrance to Hades",
                            "Cuckoo's Nest",
                            "Inside the MegaTrunk ",
                            "On a Branch Over the Drive ",
                            "The Front Door ",
                            "The Hall ",
                            "Tree Top ",
                            "Out on a limb",
                            "Rescue Esmerelda ",
                            "I'm sure I've seen this before.. ",
                            "We must perform a Quirkafleeg",
                            "Up on the Battlements",
                            "On the Roof",
                            "The Forgotten Abbey",
                            "Ballroom East",
                            "Ballroom West",
                            "To the Kitchens    Main Stairway ",
                            "The Kitchen",
                            "West of Kitchen",
                            "Cold Store ",
                            "East Wall Base ",
                            "The Chapel ",
                            "First Landing",
                            "The Nightmare Room ",
                            "The Banyan Tree",
                            "Swimming Pool",
                            "Halfway up the East Wall ",
                            "The Bathroom ",
                            "Top Landing",
                            "Master Bedroom ",
                            "A bit of tree",
                            "Orangery ",
                            "Priests' Hole",
                            "Emergency Generator",
                            "Dr Jones will never believe this ",
                            "The Attic",
                            "Under the Roof ",
                            "Conservatory Roof",
                            "On top of the house",
                            "Under the Drive",
                            "Tree Root",
                            "[",
                            "Nomen Luni ",
                            "The Wine Cellar",
                            "Watch Tower",
                            "Tool  Shed ",
                            "Back Stairway",
                            "Back Door",
                            "West  Wing ",
                            "West Bedroom ",
                            "West Wing Roof ",
                            "Above the West Bedroom ",
                            "The Beach",
                            "The Yacht",
                            "The Bow"};

constexpr uint16_t kFrameAddr = 0x89ad;
constexpr uint16_t kPauseAddr = 0x8ad1;
constexpr uint16_t kDeathAddr = 0x8c01;
constexpr uint16_t kRoomChangeAddr = 0x8912;

RouteAddress parse_address(const std::string& str) {
  std::istringstream iss(str);
  enum Element { kRoom, kEntryway, kEntrypoint, kRoute };
  Element cur_element = kRoom;

  RouteAddress result;
  uint32_t val;
  while (iss >> val) {
    switch (cur_element) {
      case kRoom:
        result.set_room(val);
        break;
      case kEntryway:
        result.set_entryway(val);
        break;
      case kEntrypoint:
        result.set_entrypoint(val);
        break;
      case kRoute:
        result.set_route_id(val);
        break;
    }

    char c;
    if (!(iss >> c)) break;
    switch (c) {
      case ':':
        cur_element = kEntryway;
        break;
      case '/':
        cur_element = kEntrypoint;
        break;
      case '.':
        cur_element = kRoute;
        break;
      default:
        LOG(FATAL) << "Unexpected character [" << c << "]";
    }
  }

  return result;
}

std::string addr_to_str(const RouteAddress& addr) {
  std::string res =
      std::to_string(addr.room()) + ":" + std::to_string(addr.entryway());
  if (addr.has_entrypoint()) res += "/" + std::to_string(addr.entrypoint());
  if (addr.has_route_id()) res += "." + std::to_string(addr.route_id());
  return res;
}

struct RouteType {
  bool is_death;
  uint32_t items_collected;
  uint32_t input_length;
  bool operator==(const RouteType& o) const {
    return is_death == o.is_death && items_collected == o.items_collected &&
           input_length == o.input_length;
  }
  template <typename H>
  friend H AbslHashValue(H h, const RouteType& c) {
    return H::combine(std::move(h), c.is_death, c.items_collected,
                      c.input_length);
  }
};

struct CsvFrame {
  // Room
  uint16_t room_id;
  uint16_t total_room_items;

  // Route
  bool is_death;
  uint16_t route_length_frames;
  uint16_t total_collected_items;
  std::string route_id;

  uint16_t num_lives;

  // Frame
  uint16_t frame_idx;
  uint32_t frame_duration_cycles;
  enum FrameType { INITIAL, NORMAL, LAST };
  FrameType frame_type;
  uint16_t collected_items_so_far;
  WillyPos willy_pos;
  InputState input;
  uint8_t items_collected_this_frame;
};

const char* boolstr(bool x) { return x ? "TRUE" : "FALSE"; }

const char* frametypes(CsvFrame::FrameType x) {
  switch (x) {
    case CsvFrame::INITIAL:
      return "INITIAL";
    case CsvFrame::NORMAL:
      return "NORMAL";
    case CsvFrame::LAST:
      return "LAST";
  }
  return "UNKNOWN";
}

class CsvWriter {
 public:
  CsvWriter(const std::string filename) : file_(filename.c_str()) {}

  void WriteHeader() {
    file_ << "room_id,"
          << "route_id,"
          << "total_room_items,"
          << "num_lives,"
          << "is_death,"
          << "route_length_frames,"
          << "total_collected_items,"
          << "frame_idx,"
          << "frame_duration_cycles,"
          << "frame_type,"
          << "collected_items_so_far,"
          << "willy_x,"
          << "willy_y,"
          << "willy_moving,"
          << "willy_orientation,"
          << "willy_animation,"
          << "willy_falling,"
          << "willy_jumping,"
          << "willy_rope,"
          << "input,"
          << "items_collected_this_frame\n";
  }
  void Write(const CsvFrame& frame) {
    WillyPosition pos = frame.willy_pos.as_proto();
    file_ << frame.room_id << "," << frame.route_id << ","
          << frame.total_room_items << "," << frame.num_lives << ","
          << boolstr(frame.is_death) << "," << frame.route_length_frames << ","
          << frame.total_collected_items << "," << frame.frame_idx << ","
          << frame.frame_duration_cycles << "," << frametypes(frame.frame_type)
          << "," << frame.collected_items_so_far << "," << pos.x() << ","
          << pos.y() << "," << boolstr(pos.moving()) << ","
          << (pos.dir_left() ? "LEFT" : "RIGHT") << "," << (pos.x() % 4) << ","
          << pos.fall() << "," << pos.jump() << "," << pos.rope() << ","
          << frame.input.as_string() << ","
          << int(frame.items_collected_this_frame) << "\n";
  }

 private:
  std::ofstream file_;
};

}  // namespace

void play_route(const PlayCmd& cmd, const RoomState& rooms) {
  std::unique_ptr<Ui> ui;
  if (!cmd.fast()) {
    ui = std::make_unique<Ui>();
  }
  Machine machine;
  if (ui) ui->set_zx_screen_memory(machine.memory());
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.load_snapshot(get_filename("jet-set-willy", "szx", "main"));
  machine.set_event_mask(Machine::kEventBreakpoint);
  machine.add_breakpoint(kFrameAddr);
  machine.add_breakpoint(kPauseAddr);
  machine.add_breakpoint(kDeathAddr);
  machine.add_breakpoint(kRoomChangeAddr);
  if (ui) ui->draw();
  // std::getchar();
  bool initialized = false;
  uint32_t total_cycles = 0;
  uint32_t total_frames = 0;

  CsvWriter writer(cmd.csv_filename());
  writer.WriteHeader();
  CsvFrame csv_frame;

  for (const auto& route_str : cmd.route()) {
    auto addr = parse_address(route_str);
    const auto& room = rooms.room(addr.room());
    const auto& point =
        room.entryway(addr.entryway()).entrypoint(addr.entrypoint());
    if (!initialized || !cmd.retain_memory()) {
      machine.memory()[cur_room] = addr.room();
      machine.memory()[willy_lives] = cmd.lives();
      WillyPos::from_proto(point.position()).to_memory(machine.memory());
      initialized = true;
    }

    JswItemSet itemset(room.id());
    csv_frame.room_id = room.id();
    csv_frame.total_room_items = itemset.total();

    LOG(INFO) << "=== " << addr_to_str(addr) << " Room " << addr.room() << " "
              << kRoomNames[addr.room()];
    uint32_t room_cycles = 0;

    uint32_t cycles = machine.run();
    LOG(INFO) << "  Initial draw: " << cycles << " cycles, " << cycles / 3500.0
              << "ms";
    room_cycles += cycles;

    if (ui) ui->draw();
    const auto& route = point.route(addr.route_id());
    csv_frame.is_death = route.is_death();
    csv_frame.route_length_frames = route.input_length();
    csv_frame.route_id = std::to_string(room.id()) + ":" +
                         std::to_string(addr.entryway()) + "/" +
                         std::to_string(addr.entrypoint()) + "." +
                         std::to_string(addr.route_id());
    csv_frame.total_collected_items = std::__popcount(route.items_collected());
    csv_frame.num_lives = machine.memory()[willy_lives];
    csv_frame.frame_idx = 0;
    csv_frame.frame_type = CsvFrame::INITIAL;
    csv_frame.collected_items_so_far = 0;
    csv_frame.input = {};
    csv_frame.items_collected_this_frame = 0;
    csv_frame.willy_pos = WillyPos::from_memory(machine.memory());
    writer.Write(csv_frame);

    auto sequence = string_to_sequence(route.inputs());
    for (size_t i = 0; i < sequence.size(); ++i) {
      const auto& input = sequence[i];
      // WillyPos::from_memory(machine.memory()).debug_print();
      csv_frame.frame_type =
          i == sequence.size() - 1 ? CsvFrame::LAST : CsvFrame::NORMAL;
      csv_frame.input = input;
      const auto num_items = machine.memory()[items_collected];

      machine.input_state() = input;
      csv_frame.willy_pos = WillyPos::from_memory(machine.memory());
      cycles = machine.run();
      csv_frame.collected_items_so_far = itemset.count(machine);
      csv_frame.items_collected_this_frame =
          machine.memory()[items_collected] - num_items;
      writer.Write(csv_frame);
      ++csv_frame.frame_idx;
      if (!cmd.fast()) {
        LOG(INFO) << "  Frame " << i << ": " << cycles << " cycles, "
                  << cycles / 3500.0 << "ms";
      }
      room_cycles += cycles;
      if (ui) ui->draw();
      auto time_to_sleep = cycles / 3500.0;
      if (!cmd.fast()) {
        sf::sleep(sf::milliseconds(time_to_sleep));
      }

      if (machine.pc() == kDeathAddr) {
        cycles = machine.run();
        LOG(INFO) << "  Death: " << cycles << " cycles, " << cycles / 3500.0
                  << "ms";
        room_cycles += cycles;
      }
    }
    LOG(INFO) << "Total room time: " << room_cycles << " cycles, "
              << room_cycles / 3500.0 << "ms " << sequence.size() << "frames";
    total_cycles += room_cycles;
    total_frames += sequence.size();
  }
  LOG(INFO) << "SUPERTOTAL: " << total_cycles << " cycles, "
            << total_cycles / 3500.0 << "ms " << total_frames << "frames";
  /*
  machine.input_state() = InputState::from_string("Q");
  for (int i = 0; i < 50; ++i) {
    machine.run();
    ui.draw();
    sf::sleep(sf::milliseconds(65));
  } */
}

namespace {

std::unique_ptr<StateDescriptor> CreateFullState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  descriptor->add(cur_room);
  descriptor->add(willy_y);
  descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  descriptor->add(willy_fall);
  descriptor->add(willy_rope);
  descriptor->add(willy_jumping);
  descriptor->add(room_definition);
  descriptor->add(items_collected_str);
  descriptor->add(items_collected);
  descriptor->add(time_str);
  descriptor->add(seconds_counter);
  descriptor->add(willy_lives);
  descriptor->add(inactivity);
  descriptor->add(room_attrs);
  descriptor->add(room_pixels);
  descriptor->add(item_table);

  return descriptor;
}

}  // namespace

void measure_route(const MeasureRouteCmd& cmd, const RoomState& rooms) {
  size_t route_idx = 0;
  size_t total_frames = 0;
  Machine machine;
  auto state_saver = CreateFullState();
  machine.set_event_mask(Machine::kEventBreakpoint);
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.add_breakpoint(kFrameAddr);
  machine.add_breakpoint(kPauseAddr);
  // machine.add_breakpoint(kDeathAddr);
  machine.add_breakpoint(kRoomChangeAddr);

  std::map<int, int> frames_to_count;

  CsvWriter writer(cmd.filename());
  writer.WriteHeader();
  CsvFrame csv_frame;

  for (const auto& room : rooms.room()) {
    JswItemSet itemset(room.id());
    csv_frame.room_id = room.id();
    csv_frame.total_room_items = itemset.total();
    machine.load_snapshot(get_filename("jet-set-willy", "szx", "main"));
    auto state = state_saver->as_string(machine);
    absl::flat_hash_set<RouteType> route_classes;

    for (const auto& entryway : room.entryway()) {
      for (const auto& entrypoint : entryway.entrypoint()) {
        for (const auto& route : entrypoint.route()) {
          if (!route_classes
                   .insert(RouteType{route.is_death(), route.items_collected(),
                                     route.input_length()})
                   .second) {
            continue;
          }
          csv_frame.is_death = route.is_death();
          csv_frame.route_length_frames = route.input_length();
          csv_frame.route_id = std::to_string(room.id()) + ":" +
                               std::to_string(entryway.id()) + "/" +
                               std::to_string(entrypoint.id()) + "." +
                               std::to_string(route.id());
          csv_frame.total_collected_items =
              std::__popcount(route.items_collected());

          ++frames_to_count[route.input_length()];
          for (int lives = 0; lives < 8; ++lives) {
            csv_frame.num_lives = lives;

            csv_frame.frame_idx = 0;
            csv_frame.frame_type = CsvFrame::INITIAL;
            csv_frame.collected_items_so_far = 0;
            csv_frame.input = {};

            state_saver->restore_from_string(state, &machine);
            csv_frame.willy_pos = WillyPos::from_memory(machine.memory());

            machine.memory()[cur_room] = room.id();
            machine.memory()[willy_lives] = lives;
            WillyPos::from_proto(entrypoint.position())
                .to_memory(machine.memory());
            csv_frame.frame_duration_cycles = machine.run();
            writer.Write(csv_frame);

            csv_frame.frame_type = CsvFrame::NORMAL;
            for (const auto& input : string_to_sequence(route.inputs())) {
              if (csv_frame.frame_idx == (route.input_length() - 1)) {
                if (csv_frame.is_death && lives == 0) break;
                csv_frame.frame_type = CsvFrame::LAST;
              }

              csv_frame.collected_items_so_far = itemset.count(machine);
              csv_frame.willy_pos = WillyPos::from_memory(machine.memory());
              csv_frame.input = input;
              machine.input_state() = input;

              csv_frame.frame_duration_cycles = machine.run();
              writer.Write(csv_frame);

              ++csv_frame.frame_idx;
            }
            total_frames += route.input_length();

            LOG(INFO) << "Route " << route_idx << ": " << room.id() << ":"
                      << entryway.id() << "/" << entrypoint.id() << "."
                      << route.id() << " lives=" << lives
                      << " frames=" << total_frames;
          }
          route_idx++;
        }
      }
    }
  }

  /* for (const auto& x : frames_to_count) {
    LOG(INFO) << x.first << "->" << x.second << "  " << x.first * x.second;
  } */
}

void populate_arc_durations(const PopulateArcDurations& cmd, RoomState* rooms) {
  Machine machine;
  auto state_saver = CreateFullState();
  machine.set_event_mask(Machine::kEventBreakpoint);
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.add_breakpoint(kFrameAddr);
  machine.add_breakpoint(kPauseAddr);
  // machine.add_breakpoint(kDeathAddr);
  machine.add_breakpoint(kRoomChangeAddr);

  size_t count = 0;
  for (auto& room : *rooms->mutable_room()) {
    machine.load_snapshot(get_filename("jet-set-willy", "szx", "main"));
    auto state = state_saver->as_string(machine);

    for (auto& entryway : *room.mutable_entryway()) {
      for (auto& entrypoint : *entryway.mutable_entrypoint()) {
        for (auto& route : *entrypoint.mutable_route()) {
          if (route.length_tstates_size() > 0) {
            continue;
          }
          auto route_id = std::to_string(room.id()) + ":" +
                          std::to_string(entryway.id()) + "/" +
                          std::to_string(entrypoint.id()) + "." +
                          std::to_string(route.id());

          for (int lives = 0; lives < 8; ++lives) {
            if (route.is_death() && lives == 0) {
              route.add_length_tstates(0);
              continue;
            }

            size_t frame_duration_cycles = 0;

            state_saver->restore_from_string(state, &machine);
            machine.memory()[cur_room] = room.id();
            machine.memory()[willy_lives] = lives;
            WillyPos::from_proto(entrypoint.position())
                .to_memory(machine.memory());
            frame_duration_cycles += machine.run();

            for (const auto& input : string_to_sequence(route.inputs())) {
              machine.input_state() = input;
              frame_duration_cycles += machine.run();
            }
            LOG(INFO) << "idx=" << count << " route=" << route_id
                      << " lives=" << lives
                      << " duration=" << frame_duration_cycles;
            route.add_length_tstates(frame_duration_cycles);
          }
          if (++count >= cmd.count()) return;
        }
      }
    }
  }
}

}  // namespace jet_set_willy
