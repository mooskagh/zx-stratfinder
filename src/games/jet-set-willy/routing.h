#pragma once

#include "proto/jet-set-willy.pb.h"

namespace jet_set_willy {

class Routing {
 public:
  Routing();
  void run();
  void commit();
  void dump_rooms();
  void dump_rooms_nicely(const DumpCmd& cmd);
  void dump_journeys();
  void task_request(const TaskRequestCmd& req);
  void apply_result(const ApplyCmd& resp);

  void split_entryways(const SplitCmd& cmd);

  bool extend_journeys();
  void reset_status(const ResetCmd&);
  void promote(const PromoteCmd&);
  void item_stats();
  void fix_route(const FixRouteCmd&);

  Room& room_by_idx(int idx);
  Entryway& entryway_by_idx(int room_idx, int idx);
  Route& route_by_addr(const RouteAddress& addr);

 private:
  RoomState rooms_;
};

}  // namespace jet_set_willy
