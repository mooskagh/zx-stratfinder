#include "games/clive.h"

#include "search/functions.h"

namespace clive {
namespace {

std::unique_ptr<StateDescriptor> CreateFullState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);
  descriptor->set_include_interrupt_counter(true);

  descriptor->add(all_mem);

  // descriptor->add(screen);
  // descriptor->add(kbd_buf);
  // descriptor->add(rng_seed);
  // descriptor->add(attr_buf);
  // descriptor->add(collectible_colour);
  // descriptor->add(collectible_frames);
  // descriptor->add(unknown_0);
  // descriptor->add(unknown_1);
  // descriptor->add(timer);
  // descriptor->add(score);
  // descriptor->add(cur_sprite);
  // descriptor->add(unknown_2);
  // descriptor->add(train_coord);
  // descriptor->add(train_timer);
  // descriptor->add(unknown_3);
  // descriptor->add(item_1_status);
  // descriptor->add(item_2_status);
  // descriptor->add(progress_status);
  // descriptor->add(unknown_4);
  // descriptor->add(unknown_5);
  // descriptor->add(clive_data);
  // descriptor->add(grandom_data);
  // descriptor->add(ghor_data);
  // descriptor->add(gver_data);
  // descriptor->add(gseek_data);
  // descriptor->add(gcomm_data);
  // descriptor->add(unknown_6);

  return descriptor;
}

std::unique_ptr<StateDescriptor> CreateHashState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  // descriptor->add(rng_seed);
  descriptor->add(attr_buf);
  descriptor->add(collectible_colour);
  descriptor->add(collectible_frames);
  descriptor->add(unknown_0);
  descriptor->add(unknown_1);
  descriptor->add(timer);
  descriptor->add(score);
  descriptor->add(cur_sprite);
  descriptor->add(unknown_2);
  descriptor->add(train_coord);
  descriptor->add(train_timer);
  descriptor->add(unknown_3);
  descriptor->add(item_1_status);
  descriptor->add(item_2_status);
  descriptor->add(progress_status);
  descriptor->add(unknown_4);
  descriptor->add(unknown_5);
  descriptor->add(clive_data);
  descriptor->add(grandom_data);
  descriptor->add(ghor_data);
  descriptor->add(gver_data);
  descriptor->add(gseek_data);
  descriptor->add(gcomm_data);
  descriptor->add(unknown_6);

  return descriptor;
}

std::unique_ptr<StateDescriptor> CreateMinimalState() {
  auto descriptor = std::make_unique<StateDescriptor>();

  descriptor->add(item_1_status);
  descriptor->add(item_2_status);
  descriptor->add(progress_status);
  descriptor->add(clive_data);

  return descriptor;
}

constexpr uint16_t kFrameAddr = 40436;
constexpr uint16_t kDeathAddr = 45525;
constexpr uint16_t kWinAddr = 40090;
constexpr uint16_t kWinTrainAddr = 41872;

class Scorer : public ScorerBase {
  Score score(const Machine& machine, int frame,
              const std::vector<InputState>& history,
              const InputState& input) const override {
    if (machine.pc() == kDeathAddr) return Score::Loss();
    if (machine.pc() == kWinAddr || machine.pc() == kWinTrainAddr) {
      return Score::Win();
    }
    const auto& memory = machine.memory();
    if (memory[unknown_5] == 1) return Score::Win();
    return (memory[item_1_status] + memory[item_2_status] +
            memory[progress_status]) *
           0.2f;
  }
};

}  // namespace

SearchConfig GetUniversalConfig(const std::string& level) {
  SearchConfig config;
  int level_num = 0;
  // if (level.find("lvl5") == 0) level_num = 5;
  LOG(INFO) << "Level: " << level_num;
  config.game_name = "clive";
  config.run_name = level;
  config.full_state = CreateFullState();
  config.hash_state = CreateHashState();
  config.minimal_state = CreateMinimalState();
  config.scorer = std::make_unique<Scorer>();
  config.init_func = [level](Machine* machine) {
    machine->set_event_mask(Machine::kEventBreakpoint);
    machine->memory().load_rom(get_data_dir() + "roms/48.rom");
    machine->load_snapshot(get_filename("clive", "szx", level));
    machine->add_breakpoint(kFrameAddr);
    machine->add_breakpoint(kDeathAddr);
    machine->add_breakpoint(kWinAddr);
    machine->add_breakpoint(kWinTrainAddr);
    machine->set_interrupts_enabled(false);
  };
  config.actions.emplace_back(InputState::kKey_None);
  config.actions.emplace_back(InputState::kKey_6);  // Left
  config.actions.emplace_back(InputState::kKey_7);  // Right
  config.actions.emplace_back(InputState::kKey_8);  // Down
  config.actions.emplace_back(InputState::kKey_9);  // Up
  config.actions.emplace_back(InputState::kKey_6 |
                              InputState::kKey_9);  // Left + Up
  config.actions.emplace_back(InputState::kKey_7 |
                              InputState::kKey_9);  // Right + Up
  config.actions.emplace_back(InputState::kKey_6 |
                              InputState::kKey_8);  // Left + Down
  config.actions.emplace_back(InputState::kKey_7 |
                              InputState::kKey_8);  // Right + Down
  config.hash_is_per_frame = true;
  config.dedup_by_minimal_hash = false;
  // config.global_duplicate_score = Sigmoid(20, 0.999, 50, -1.5f, 0.0f);
  config.global_duplicate_score = Sigmoid(30, 0.999, 250, -0.4f, 0.0f);
  config.local_duplicate_score = Sigmoid(10, 0.8, 41, -4.0f, 0.63f);
  // config.valid_state_func = std::make_unique<ManicValidator>(is_sp);
  // config.search_width = 50;
  config.ticks_limit = 300000000;
  config.search_width = 30000;
  config.num_threads = 3;

  return config;
}
}  // namespace clive
