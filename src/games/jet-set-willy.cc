#include "games/jet-set-willy.h"

#include <glog/logging.h>

#include "proto/jet-set-willy.pb.h"
#include "search/functions.h"

namespace jet_set_willy {
namespace {
constexpr uint16_t kFrameAddr = 0x89ad;
constexpr uint16_t kPauseAddr = 0x8ad1;
constexpr uint16_t kDeathAddr = 0x8c01;
constexpr uint16_t kRoomChangeAddr = 0x8912;

}  // namespace

WillyPos::WillyPos(uint8_t y, uint8_t dir_mov, uint8_t animation,
                   uint16_t location, uint8_t fall, uint8_t jumping,
                   uint8_t rope)
    : location_(location),
      y_(y),
      dir_mov_(dir_mov),
      animation_(animation),
      fall_(fall),
      jumping_(jumping),
      rope_(rope) {}

WillyPos WillyPos::from_vars(int x, int y, bool direction_left, bool is_moving,
                             int jumping_status, int falling_status, int rope) {
  return WillyPos(y * 2, (is_moving ? 2 : 0) | (direction_left ? 1 : 0), x % 4,
                  0x5c00 + (y / 8) * 32 + (x / 4), falling_status,
                  jumping_status, rope);
}

WillyPos WillyPos::from_memory(const Memory& memory) {
  return WillyPos{memory[willy_y],
                  memory[willy_dir_mov],
                  memory[willy_animation],
                  memory[willy_location],
                  memory[willy_rope] == 0 ? memory[willy_fall] : uint8_t{0},
                  (memory[willy_rope] == 0 && memory[willy_fall] == 1)
                      ? memory[willy_jumping]
                      : uint8_t{0},
                  memory[willy_rope]};
}

void WillyPos::debug_print() const {
  LOG(INFO) << "WillyPos y=" << int(y_ / 2)
            << " x=" << (location_ - 0x5c00 - y_ / 16 * 32) * 4 + animation_
            << " dirmov=" << int(dir_mov_) << " loc=" << location_
            << " anim=" << int(animation_) << " fall=" << int(fall_)
            << " jump=" << int(jumping_) << " rope=" << int(rope_);
}

WillyPos WillyPos::from_proto(const WillyPosition& pos) {
  return from_vars(pos.x(), pos.y(), pos.dir_left(), pos.moving(), pos.jump(),
                   pos.fall(), pos.rope());
}

WillyPosition WillyPos::as_proto() const {
  WillyPosition result;
  result.set_x(4 * (location_ - 0x5c00 - 32 * (y_ / 2 / 8)) + animation_);
  result.set_y(y_ / 2);
  result.set_dir_left(dir_mov_ & 1);
  result.set_moving(dir_mov_ & 2);
  if (rope_ == 0 && fall_ == 1) result.set_jump(jumping_);
  if (rope_ == 0) result.set_fall(fall_);
  result.set_rope(rope_);
  return result;
}

void WillyPos::to_memory(Memory& memory) const {
  memory[willy_y] = y_;
  memory[willy_dir_mov] = dir_mov_;
  memory[willy_animation] = animation_;
  memory[willy_location] = location_;
  memory[willy_fall] = fall_;
  memory[willy_jumping] = jumping_;
  memory[willy_rope] = rope_;
}

void set_willy_pos(Machine* machine, int room, int x, int y,
                   bool direction_left, bool is_moving, int jumping_status,
                   int falling_status) {
  auto& memory = machine->memory();
  memory[cur_room] = room;
  WillyPos::from_vars(x, y, direction_left, is_moving, jumping_status,
                      falling_status, 0)
      .to_memory(memory);
}

namespace {
void init(Machine* machine) {
  machine->set_event_mask(Machine::kEventBreakpoint);
  machine->memory().load_rom(get_data_dir() + "roms/48.rom");
  machine->load_snapshot(get_filename("jet-set-willy", "szx", "main"));
  machine->add_breakpoint(kFrameAddr);
  machine->add_breakpoint(kPauseAddr);
  machine->add_breakpoint(kDeathAddr);
  machine->add_breakpoint(kRoomChangeAddr);
}

class InitStates {
 public:
  InitStates(uint8_t room, const google::protobuf::RepeatedPtrField<
                               jet_set_willy::WillyPosition>& entrypoints)
      : room_(room),
        entrypoints_(std::begin(entrypoints), std::end(entrypoints)) {}
  std::vector<FrameData> operator()(const Machine&,
                                    const SearchConfig& config) {
    std::vector<FrameData> frames;
    uint16_t idx = 0;
    for (const auto& pos : entrypoints_) {
      Machine machine;
      config.init_func(&machine);
      machine.memory()[cur_room] = room_;
      WillyPos::from_proto(pos).to_memory(machine.memory());
      machine.run();
      CHECK_EQ(machine.pc(), kFrameAddr);
      frames.push_back(FrameData{0.0f,
                                 config.full_state->as_string(machine),
                                 std::vector<InputState>{},
                                 {},
                                 idx++});
    }
    return frames;
  }

 private:
  uint8_t room_;
  std::vector<WillyPosition> entrypoints_;
};

std::unique_ptr<StateDescriptor> CreateFullState() {
  auto descriptor = std::make_unique<StateDescriptor>();
  descriptor->set_include_cpu_state(true);

  descriptor->add(cur_room);
  descriptor->add(willy_y);
  descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  descriptor->add(willy_fall);
  descriptor->add(willy_rope);
  descriptor->add(willy_jumping);
  descriptor->add(room_definition);
  descriptor->add(items_collected_str);
  descriptor->add(items_collected);
  descriptor->add(time_str);
  descriptor->add(seconds_counter);
  descriptor->add(willy_lives);
  descriptor->add(inactivity);
  descriptor->add(room_attrs);
  descriptor->add(room_pixels);
  descriptor->add(item_table);

  return descriptor;
}

class HashState : public StateDescriptor {
 private:
  uint64_t get_hash_seed(const Machine& machine) const override {
    const auto& memory = machine.memory();
    if (memory[willy_rope] == 1) return 0;
    uint64_t res = 1 + memory[willy_fall];
    if (memory[willy_fall] == 1) res += 1000 * memory[willy_jumping];
    return res;
  }
};

std::unique_ptr<StateDescriptor> CreateHashState() {
  auto descriptor = std::make_unique<HashState>();
  descriptor->add(willy_y);
  descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  // descriptor->add(willy_fall);
  descriptor->add(willy_rope);
  // descriptor->add(willy_jumping);
  return descriptor;
}

class MinimalState : public StateDescriptor {
 public:
  MinimalState(uint8_t room) : item_set_(room) {}

 private:
  uint64_t get_hash_seed(const Machine& machine) const override {
    const auto& memory = machine.memory();
    const auto item_score = 256 * item_set_(machine).set;
    return item_score + (memory[willy_fall] == 1 ? memory[willy_jumping] : 0);
  }
  JswItemSet item_set_;
};

std::unique_ptr<MinimalState> CreateMinimalState(uint8_t room) {
  auto descriptor = std::make_unique<MinimalState>(room);

  descriptor->add(willy_y);
  descriptor->add(willy_dir_mov);
  descriptor->add(willy_animation);
  descriptor->add(willy_location);
  descriptor->add(willy_fall);
  descriptor->add(willy_rope);
  // descriptor->add(willy_jumping);
  return descriptor;
}

class Scorer : public ScorerBase {
 public:
  Scorer(uint8_t room) : item_set_(room) {}

  Score score(const Machine& machine, int frame, const std::vector<InputState>&,
              const InputState&) const override {
    if (machine.pc() == kPauseAddr) {
      return Score::Loss();
    }
    if (machine.pc() == kDeathAddr || machine.pc() == kRoomChangeAddr) {
      return Score::Candidate();
    }
    return item_set_(machine).count() * 0.001f;
  }

 private:
  JswItemSet item_set_;
};

class JswValidator : public StateValidator {
 public:
  JswValidator(uint8_t room, const std::vector<InputState>& first_moves)
      : room_(room), first_moves_(first_moves) {}

 private:
  bool is_valid(const InputState& state, const Machine& machine, int frame,
                const std::vector<InputState>&) override {
    CHECK_EQ(machine.memory()[willy_lives], 7);
    CHECK_EQ(machine.memory()[cur_room], room_);
    CHECK_EQ(machine.pc(), kFrameAddr);
    if (frame < (int)first_moves_.size()) return state == first_moves_[frame];

    return state.empty() || empty_was_valid_ ||
           machine.memory()[willy_fall] == 0 ||
           machine.memory()[willy_rope] != 0;
  }
  void here_what_happened(const InputState& state,
                          const Machine& machine) override {
    if (machine.pc() != kFrameAddr) {
      empty_was_valid_ = true;
    } else if (state.empty()) {
      empty_was_valid_ = (machine.memory()[willy_fall] == 0 ||
                          machine.memory()[willy_rope] != 0);
    }
  }

 private:
  bool empty_was_valid_ = false;
  uint8_t room_;
  std::vector<InputState> first_moves_;
};

class CandidateKeper {
 public:
  CandidateKeper(RoomResult* result) : result_(result) {}
  void operator()(const SearchConfig& config, uint16_t source_idx,
                  std::vector<InputState>& inputs, Machine& machine) {
    result_->add(config, source_idx, inputs, machine);
  }

 private:
  RoomResult* result_;
};
}  // namespace

void RoomResult::add(const SearchConfig& config, uint16_t source_idx,
                     std::vector<InputState>& inputs, Machine& machine) {
  auto items = config.itemset_func(machine);

  if (machine.pc() == kDeathAddr) {
    for (const auto& entries : life_losses_) {
      if (entries.items.is_superset_of(items)) return;
    }
    LOG(INFO) << "New life loss. items=" << int(items.set)
              << " length=" << inputs.size();
    LOG(INFO) << sequence_as_string(inputs);
    life_losses_.push_back(RouteDef{source_idx, items, inputs});
    return;
  }

  // Must be a room transition.
  const auto& memory = machine.memory();
  const ExitKey key{WillyPos::from_memory(memory), memory[cur_room]};
  auto& portals = exits_[key];
  for (const auto& portal : portals) {
    if (portal.items.is_superset_of(items)) return;
  }

  LOG(INFO) << "New room transition: room=" << int(key.room)
            << " items=" << int(items.set) << " length=" << inputs.size();
  LOG(INFO) << sequence_as_string(inputs);
  key.willy_pos.debug_print();
  portals.push_back(RouteDef{source_idx, items, inputs});
}

TaskResult RoomResult::as_proto(const TaskRequest& request) const {
  TaskResult result;
  *result.mutable_request() = request;

  for (const auto& exit : exits_) {
    for (const auto& routedef : exit.second) {
      auto* route = result.add_route();
      route->set_entrypoint_id(routedef.source_idx);
      route->set_exit_room(exit.first.room);
      *route->mutable_exit_pos() = exit.first.willy_pos.as_proto();
      route->set_inputs(sequence_as_string(routedef.inputs));
      route->set_input_length(routedef.inputs.size());
      route->set_items_collected(routedef.items.set);
    }
  }

  for (const auto& death : life_losses_) {
    auto* route = result.add_route();
    route->set_is_death(true);
    route->set_entrypoint_id(death.source_idx);
    route->set_inputs(sequence_as_string(death.inputs));
    route->set_input_length(death.inputs.size());
    route->set_items_collected(death.items.set);
  }

  return result;
}

SearchConfig get_config(const TaskRequest& request, RoomResult* result) {
  SearchConfig config;
  auto room = request.room_id();

  config.game_name = "jet-set-willy";
  config.run_name = "task-" + std::to_string(request.id());

  config.full_state = CreateFullState();
  config.hash_state = CreateHashState();
  config.minimal_state = CreateMinimalState(room);
  config.scorer = std::make_unique<Scorer>(room);

  // Search core.
  config.init_func = init;
  config.init_states_func = InitStates(room, request.entrypoint());
  config.candidate_func = CandidateKeper(result);
  config.mode_func = StdModeFunc({
      0x89FE, 0x89FF,          // LDIR
      0x8A2F, 0x8A30,          // LDIR
      0x8B10, 0x8B11,          // LDIR
      0x89ad, 0x89ae, 0x89af,  // animate willy along the bottom(graphics)
      0x89ef, 0x89f0, 0x89f1,  // animate conveyors(graphics)
      0x8a3a, 0x8a3b, 0x8a3c,  // print the time(text)
      0x8a46, 0x8a47, 0x8a48,  // print items(text)
      0x944e, 0x944f, 0x9450,  // animates graphic item/object
      0x8e20, 0x8e21, 0x8e23, 0x8e24,  // remove the jumping/falling  sfx
  });

  // Actions.
  config.actions.emplace_back(InputState::kKey_None);
  config.actions.emplace_back(InputState::kKey_Q);  // Left
  config.actions.emplace_back(InputState::kKey_R);  // Right
  config.actions.emplace_back(InputState::kKey_M);  // Jump
  config.actions.emplace_back(InputState::kKey_Q |
                              InputState::kKey_M);  // Left + Jump
  config.actions.emplace_back(InputState::kKey_R |
                              InputState::kKey_M);  // Right + Jump

  config.hash_is_per_frame = true;
  config.dedup_by_items = true;
  config.global_duplicate_score = Sigmoid(20, 0.999, 50, -1.5f, 0.0f);
  config.valid_state_func = std::make_unique<JswValidator>(
      room, string_to_sequence(request.starting_moves()));
  config.itemset_func = JswItemSet(room);

  config.search_width = request.search_width();

  return config;
}

}  // namespace jet_set_willy
