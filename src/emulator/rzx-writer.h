#pragma once

#include <libspectrum.h>

#include <memory>
#include <string>
#include <vector>

#include "misc/fuselib.h"

class RzxWriter {
 public:
  RzxWriter();
  ~RzxWriter();

  void add_snapshot(FuseSnap snap);
  void add_input(uint8_t val);
  void add_interrupt(int ops);

  void write(const std::string& filename);

 private:
  libspectrum_rzx* const rzx_;
  std::vector<uint8_t> inputs_;
};