#pragma once

#include <cstdint>
#include <string>
#include <vector>

class InputState {
 public:
  enum Key {
    kKey_None = 0ULL,
    kKey_Shift = 1ULL << 0,
    kKey_Z = 1ULL << 1,
    kKey_X = 1ULL << 2,
    kKey_C = 1ULL << 3,
    kKey_V = 1ULL << 4,
    kKey_A = 1ULL << 5,
    kKey_S = 1ULL << 6,
    kKey_D = 1ULL << 7,
    kKey_F = 1ULL << 8,
    kKey_G = 1ULL << 9,
    kKey_Q = 1ULL << 10,
    kKey_W = 1ULL << 11,
    kKey_E = 1ULL << 12,
    kKey_R = 1ULL << 13,
    kKey_T = 1ULL << 14,
    kKey_1 = 1ULL << 15,
    kKey_2 = 1ULL << 16,
    kKey_3 = 1ULL << 17,
    kKey_4 = 1ULL << 18,
    kKey_5 = 1ULL << 19,
    kKey_0 = 1ULL << 20,
    kKey_9 = 1ULL << 21,
    kKey_8 = 1ULL << 22,
    kKey_7 = 1ULL << 23,
    kKey_6 = 1ULL << 24,
    kKey_P = 1ULL << 25,
    kKey_O = 1ULL << 26,
    kKey_I = 1ULL << 27,
    kKey_U = 1ULL << 28,
    kKey_Y = 1ULL << 29,
    kKey_Enter = 1ULL << 30,
    kKey_L = 1ULL << 31,
    kKey_K = 1ULL << 32,
    kKey_J = 1ULL << 33,
    kKey_H = 1ULL << 34,
    kKey_Space = 1ULL << 35,
    kKey_Sym = 1ULL << 36,
    kKey_M = 1ULL << 37,
    kKey_N = 1ULL << 38,
    kKey_B = 1ULL << 39,
  };

  InputState() = default;
  InputState(uint64_t state) : keyboard_state_(state) {}
  void set_keyboard(Key state) { keyboard_state_ = state; }

  uint8_t on_input(uint16_t port);
  std::string as_string() const;
  uint64_t get_keyboard_state() const { return keyboard_state_; }
  bool empty() const { return keyboard_state_ == 0; }
  bool has(Key key) const { return (key & keyboard_state_) != 0; }
  static InputState from_string(const std::string& str);

  bool operator==(const InputState& other) const {
    return keyboard_state_ == other.keyboard_state_;
  }
  bool operator!=(const InputState& other) const {
    return keyboard_state_ != other.keyboard_state_;
  }

 private:
  uint8_t handle_keyboard(uint8_t upper_port);

  uint64_t keyboard_state_ = kKey_None;
};

std::string sequence_as_string(const std::vector<InputState>& seq);
std::vector<InputState> get_inputs_from_file(const std::string& filename);
std::vector<InputState> string_to_sequence(const std::string& string);