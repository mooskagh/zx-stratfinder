#pragma once

#include <glog/logging.h>
#include <z80/z80.h>

#include <limits>
#include <memory>

#include "emulator/input.h"
#include "emulator/memory.h"
#include "emulator/rzx-writer.h"

struct CpuState {
  unsigned int_mode;
  uint32_t tstates;
  uint16_t pc;
  uint16_t sp;
  uint16_t hl;
  uint16_t de;
  uint16_t bc;
  uint16_t is_halted;
  uint16_t ix;
  uint16_t iy;
  uint16_t alt_bc;
  uint16_t alt_de;
  uint16_t alt_hl;
  uint16_t alt_af;
  uint8_t a;
  uint8_t f;
  uint8_t i;
  uint8_t r;
  bool iff1;
  bool iff2;

  void append_to_string(const Memory& memory, std::string* dst) const;
  size_t restore_from_string(std::string_view str, Memory* memory) const;
};

class Machine : public z80::z80_cpu<Machine> {
 public:
  typedef z80::z80_cpu<Machine> base;
  static constexpr uint8_t kEventInterrupt = 1;
  static constexpr uint8_t kEventBreakpoint = 2;

  uint64_t run(uint64_t max_ops = std::numeric_limits<uint64_t>::max());
  Memory& memory() { return memory_; }
  const Memory& memory() const { return memory_; }
  void set_rzx_writer(std::unique_ptr<RzxWriter> rzx_writer);
  RzxWriter* writer() const { return rzx_writer_.get(); }
  void load_snapshot(const std::string& filename);
  void save_snapshot(const std::string& filename,
                     libspectrum_id_t format = LIBSPECTRUM_ID_SNAPSHOT_SZX);
  void save_cpu_state(CpuState* state) const;
  void restore_cpu_state(const CpuState* state);
  void add_breakpoint(uint16_t addr) { breakpoints_[addr] = true; }
  void clear_breakpoints() { breakpoints_.fill(false); }
  void set_event_mask(uint8_t mask) { event_mask_ = mask; }
  InputState& input_state() { return input_state_; }
  const InputState& input_state() const { return input_state_; }
  uint32_t frame_tick() const { return frame_tick_; }
  void set_frame_tick(uint32_t v) { frame_tick_ = v; }

  // Handlers
  z80::fast_u8 on_read(z80::fast_u16 addr) { return memory_.read(addr); }
  void on_write(z80::fast_u16 addr, z80::fast_u8 n) { memory_.write(addr, n); }
  bool on_handle_active_int();
  z80::fast_u8 on_input(z80::fast_u16 port);
  void on_tick(unsigned t);
  // void on_set_pc(z80::fast_u16 addr);
  //  void on_step();
  uint16_t pc() const { return get_pc(); }
  z80::fast_u8 on_m1_fetch_cycle();
  void set_interrupts_enabled(bool enabled) { interrupts_enabled_ = enabled; }

 private:
  Memory memory_;
  std::array<bool, 65536> breakpoints_;
  std::unique_ptr<RzxWriter> rzx_writer_;
  InputState input_state_;
  libspectrum_machine machine_model = LIBSPECTRUM_MACHINE_48;

  uint8_t events_ = 0;
  uint8_t event_mask_ = 0xff;
  uint32_t frame_tick_ = 0;
  bool interrupts_enabled_ = true;
};
