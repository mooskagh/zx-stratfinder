#include "emulator/input.h"

#include <glog/logging.h>

#include <algorithm>
#include <fstream>
#include <sstream>

namespace {
static constexpr const char kKeys[] =
    "^ZXCVASDFGQWERT1234509876POIUY/LKJH_*MNB";

}  // namespace

uint8_t InputState::handle_keyboard(uint8_t upper_port) {
  uint8_t res = 0xff;
  for (int bit = 0; bit < 8; ++bit) {
    if (upper_port & (1 << bit)) continue;
    res &= ~((keyboard_state_ >> (bit * 5)) & 0x1f);
  }
  return res;
}

uint8_t InputState::on_input(uint16_t port) {
  switch (port & 0xff) {
    case 0xfe:
      return handle_keyboard(port >> 8);
    default:
      return 0xff;
  };
}

std::string InputState::as_string() const {
  std::string str;
  for (size_t i = 0; i < sizeof(kKeys); ++i) {
    if (keyboard_state_ & (1ull << i)) str += kKeys[i];
  }
  if (str.empty()) str = ".";
  return str;
}

InputState InputState::from_string(const std::string& str) {
  InputState state;
  if (str == ".") return state;
  for (const auto& c : str) {
    auto* ptr = std::find(std::begin(kKeys), std::end(kKeys), c);
    CHECK_NE(ptr, std::end(kKeys)) << "Unexpected char [" << c << "]";
    state.keyboard_state_ |= 1ull << (ptr - std::begin(kKeys));
  }

  return state;
}

std::string sequence_as_string(const std::vector<InputState>& seq) {
  std::string res;
  for (const auto& input : seq) {
    if (!res.empty()) res += ' ';
    res += input.as_string();
  }
  return res;
}

std::vector<InputState> get_inputs_from_file(const std::string& filename) {
  std::vector<InputState> res;
  std::ifstream fi(filename.c_str());
  std::string input;
  while (fi >> input) {
    res.push_back(InputState::from_string(input));
  }
  return res;
}

std::vector<InputState> string_to_sequence(const std::string& string) {
  std::vector<InputState> res;
  std::istringstream fi(string);
  std::string input;
  while (fi >> input) {
    res.push_back(InputState::from_string(input));
  }
  return res;
}
