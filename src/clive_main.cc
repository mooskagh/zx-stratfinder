#include <glog/logging.h>

#include "games/clive.h"
#include "misc/config.h"
#include "search/exception.h"
#include "search/search.h"
#include "search/trace-hook.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;

  CHECK(argc >= 2) << "Usage: ./playseq <config> [<moves to trace>]";

  Search search(std::make_unique<Ui>(), clive::GetUniversalConfig(argv[1]));
  if (argc >= 3) {
    search.set_hook(std::make_unique<TraceHook>(
        clive::GetUniversalConfig(argv[1]), get_inputs_from_file(argv[2])));
  }
  try {
    search.run(10000);
  } catch (const SearchException& ex) {
    LOG(ERROR) << "SearchException: " << ex.what();
    LOG(ERROR) << sequence_as_string(ex.inputs());
    const auto& frame = ex.frame();
    if (frame) {
      frame->debug.debug_log();
      LOG(ERROR) << "source_idx=" << frame->source_idx;
    }
    if (!ex.stack().empty()) {
      LOG(ERROR) << "Stack trace:";
      for (const auto& addr : ex.stack()) {
        LOG(ERROR) << addr;
      }
    }
    return 1;
  }

  return 0;
}
