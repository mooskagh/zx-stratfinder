#include <glog/logging.h>

#include "src/misc/set-set-manager.h"

int main() {
  {
    SetSetManager manager;
    SetSetId setset;
    bool update = false;

    LOG(INFO) << "Test1";
    LOG(INFO) << setset.idx;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(1));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(1));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
  }

  {
    SetSetManager manager;
    SetSetId setset;
    bool update = false;

    LOG(INFO) << "Test2";
    LOG(INFO) << setset.idx;
    std::tie(update, setset) = manager.update(setset, Set16(1));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(1));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(1));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
    std::tie(update, setset) = manager.update(setset, Set16(0));
    LOG(INFO) << setset.idx << ' ' << update;
  }

  return 0;
}