#include <glog/logging.h>

#include <cstdio>

#include "emulator/machine.h"
#include "games/clive.h"
#include "misc/config.h"
#include "ui/ui.h"
#include "ui/zxscreen.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;
  CHECK(argc == 4) << "Usage: ./playseq <config> <moves> <out_rzx>";

  Ui ui;
  Machine machine;

  ui.set_zx_screen_memory(machine.memory());

  auto config = clive::GetUniversalConfig(argv[1]);

  machine.set_rzx_writer(std::make_unique<RzxWriter>());
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  config.init_func(&machine);

  ui.draw();

  int frame = 0;
  // std::getchar();
  for (const auto& input : get_inputs_from_file(argv[2])) {
    machine.input_state() = input;
    machine.run();
    ui.draw();
    ++frame;
    sf::sleep(sf::milliseconds(10));
  }
  machine.input_state() = {};
  machine.set_event_mask(Machine::kEventInterrupt);
  for (int i = 0; i < 100; ++i) {
    machine.run();
    ui.draw();
    sf::sleep(sf::milliseconds(20));
  }
  machine.writer()->write(argv[3]);
}
