#pragma once

#include <zlib.h>

#include <cstdint>
#include <type_traits>
#include <vector>

class Serializer {
 public:
  virtual ~Serializer() = default;
  virtual void serialize(void* data, std::size_t size) = 0;
  virtual bool is_read() const = 0;
};

class CompressedWriterSerializer : public Serializer {
 public:
  CompressedWriterSerializer(const char* filename);

 private:
  void serialize(void* data, std::size_t size) override;
  bool is_read() const override { return false; }

  gzFile file_;
};

template <typename T>
std::enable_if_t<std::is_arithmetic_v<T>> serialize(Serializer* s, T* val) {
  s->serialize(val, sizeof(T));
}

template <typename T>
void serialize(Serializer* s, std::vector<T>* val) {
  size_t sz_tmp = val->size();
  serialize(s, &sz_tmp);
  if (s->is_read()) val->resize(sz_tmp);
  for (auto& x : *val) serialize(s, &x);
}