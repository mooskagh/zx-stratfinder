#pragma once

#include <optional>
#include <string>
#include <string_view>

std::string read_file_to_string(const std::string& filename);
void write_string_to_file(const std::string& filename,
                          const std::string_view content);

class FileBuffer {
 public:
  FileBuffer(const std::string& filename);
  const std::string& data() const { return data_; }
  const std::string& filename() const { return filename_; }

 private:
  const std::string filename_;
  std::string data_;
};
