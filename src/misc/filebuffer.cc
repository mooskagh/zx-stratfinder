#include "misc/filebuffer.h"

#include <glog/logging.h>

std::string read_file_to_string(const std::string& filename) {
  std::string result;

  std::FILE* f = std::fopen(filename.c_str(), "rb");
  CHECK(f != nullptr) << "Cannot open file " << filename;

  std::size_t last_offset = 0;
  std::size_t size = 0x10000;

  while (true) {
    const size_t len = size - last_offset;
    result.resize(size);
    auto count = std::fread(result.data() + last_offset, 1, len, f);
    if (count < len) {
      result.resize(last_offset + count);
      break;
    }
    last_offset += len;
    size *= 2;
  }

  std::fclose(f);
  return result;
}

void write_string_to_file(const std::string& filename,
                          const std::string_view content) {
  std::FILE* f = CHECK_NOTNULL(std::fopen(filename.c_str(), "wb"));
  std::fwrite(content.data(), content.size(), 1, f);
  std::fclose(f);
}

FileBuffer::FileBuffer(const std::string& filename) : filename_(filename) {
  data_ = read_file_to_string(filename);
}
