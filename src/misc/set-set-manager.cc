#include "misc/set-set-manager.h"

#include <glog/logging.h>
#include <metrohash64/metrohash64.h>

namespace {
uint64_t get_vector_hash(const std::vector<Set16>& vals) {
  uint64_t hash;
  MetroHash64::Hash(reinterpret_cast<const uint8_t*>(vals.data()),
                    sizeof(Set16) * vals.size(),
                    reinterpret_cast<uint8_t*>(&hash), 0xe087fb477bd84fe2);
  return hash;
}
}  // namespace

SetSetManager::SetSetManager() {
  set_sets_.emplace_back();
  set_sets_.emplace_back(1, Set16(0));
  hash_to_set_id_[get_vector_hash(set_sets_[0])] = SetSetId(0);
  hash_to_set_id_[get_vector_hash(set_sets_[1])] = SetSetId(1);
}

std::pair<bool, SetSetId> SetSetManager::update(SetSetId idx, Set16 set16) {
  const auto& set_set = set_sets_[idx.idx];
  for (const auto& x : set_set) {
    if (x.is_superset_of(set16)) return {true, idx};
  }

  if (set16.set == 0) return {false, SetSetId(1)};

  std::vector<Set16> new_set;
  new_set.reserve(set_set.size() + 1);
  bool used_new_set = false;
  for (const auto& x : set_set) {
    if ((x.set & set16.set) == set16.set) continue;
    if (!used_new_set && set16.set < x.set) {
      new_set.push_back(set16);
      used_new_set = true;
    }
    new_set.push_back(x);
  }
  if (!used_new_set) new_set.push_back(set16);

  uint64_t hash = get_vector_hash(new_set);

  auto iter = hash_to_set_id_.find(hash);
  if (iter != hash_to_set_id_.end()) return {false, iter->second};

  SetSetId new_id(set_sets_.size());
  hash_to_set_id_[hash] = new_id;
  set_sets_.push_back(std::move(new_set));

  return {false, new_id};
}