#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include <chrono>

#include "games/jet-set-willy.h"
#include "misc/filebuffer.h"
#include "proto/jet-set-willy.pb.h"
#include "search/exception.h"
#include "search/search.h"

ABSL_FLAG(std::string, ascii_taskfile, "",
          "ASCII protobuf of a task definition");
ABSL_FLAG(std::string, taskfile, "", "Task definition");
ABSL_FLAG(bool, ui, true, "Use UI");
ABSL_FLAG(std::string, ascii_resultfile, "", "ASCII protobuf of a task result");
ABSL_FLAG(std::string, resultfile, "", "Task result file");

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);

  CHECK(absl::GetFlag(FLAGS_ascii_taskfile).empty() !=
        absl::GetFlag(FLAGS_taskfile).empty())
      << "Exactly one of --ascii_taskfile or --taskfile must be defined";

  jet_set_willy::TaskRequest task_request;

  if (!absl::GetFlag(FLAGS_ascii_taskfile).empty()) {
    CHECK(google::protobuf::TextFormat::MergeFromString(
        read_file_to_string(absl::GetFlag(FLAGS_ascii_taskfile)),
        &task_request));
  } else if (!absl::GetFlag(FLAGS_taskfile).empty()) {
    CHECK(task_request.ParseFromString(
        read_file_to_string(absl::GetFlag(FLAGS_taskfile))));
  }

  jet_set_willy::RoomResult room_result;

  Search search(absl::GetFlag(FLAGS_ui) ? std::make_unique<Ui>() : nullptr,
                jet_set_willy::get_config(task_request, &room_result));

  int used_frames = 0;
  auto start_time = std::chrono::steady_clock::now();
  try {
    used_frames =
        search.run(task_request.max_frames(), task_request.max_no_progress());
  } catch (const SearchException& ex) {
    LOG(ERROR) << "SearchException: " << ex.what();
    LOG(ERROR) << sequence_as_string(ex.inputs());
    const auto& frame = ex.frame();
    if (frame) {
      frame->debug.debug_log();
      LOG(ERROR) << "source_idx=" << frame->source_idx;
    }
    return 1;
  }

  jet_set_willy::TaskResult result = room_result.as_proto(task_request);
  result.set_duration_seconds(std::chrono::duration_cast<std::chrono::seconds>(
                                  std::chrono::steady_clock::now() - start_time)
                                  .count());
  result.set_frames_used(used_frames);
  if (!absl::GetFlag(FLAGS_resultfile).empty()) {
    write_string_to_file(absl::GetFlag(FLAGS_resultfile),
                         result.SerializeAsString());
  }
  if (!absl::GetFlag(FLAGS_ascii_resultfile).empty()) {
    std::string out;
    CHECK(google::protobuf::TextFormat::PrintToString(result, &out));
    write_string_to_file(absl::GetFlag(FLAGS_ascii_resultfile), out);
  }

  return 0;
}
