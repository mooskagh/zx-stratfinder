#include <glog/logging.h>

#include "emulator/machine.h"
#include "games/manic-miner.h"
#include "misc/config.h"
#include "ui/ui.h"
#include "ui/zxscreen.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;
  Ui ui;

  for (int level = 0; level < 20; ++level) {
    LOG(INFO) << "Level " << level + 1;
    Machine machine;
    machine.set_event_mask(Machine::kEventInterrupt);
    ui.set_zx_screen_memory(machine.memory());
    // machine.set_rzx_writer(std::make_unique<RzxWriter>());
    machine.memory().load_rom(get_data_dir() + "roms/48.rom");
    machine.load_snapshot(get_data_dir() + "manic-miner/szx/main-irf.szx");

    for (int i = 0; i < 50; ++i) {
      machine.run();
      ui.draw();
      sf::sleep(sf::milliseconds(20));
    }

    machine.input_state().set_keyboard(InputState::kKey_Enter);
    machine.memory()[manic_miner::level_num] = level;
    machine.add_breakpoint(0x870e);
    machine.set_event_mask(Machine::kEventBreakpoint);
    machine.run();

    ui.draw();
    machine.save_snapshot(get_data_dir() + "manic-miner/szx/lvl" +
                          std::to_string(level + 1) + "-irf.szx");

    for (int i = 0; i < 50; ++i) {
      if (!ui.process_events()) return 1;
      machine.run();
      ui.draw();
      sf::sleep(sf::milliseconds(20));
    }
    // machine.writer()->write("/tmp/x.rzx");
  }
}
