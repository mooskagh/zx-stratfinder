#pragma once

#include <SFML/Graphics.hpp>

class UiView : public sf::Transformable,
               public sf::Drawable,
               private sf::NonCopyable {
 public:
  void draw(sf::RenderTarget& target,
            sf::RenderStates states) const override = 0;
};