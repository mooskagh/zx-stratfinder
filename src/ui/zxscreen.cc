#include "ui/zxscreen.h"

#include <glog/logging.h>

namespace {
constexpr uint32_t kColors[] = {
    0x000000ff, 0x0022c7ff, 0xd62816ff, 0xd433c7ff, 0x00c525ff, 0x00c7c9ff,
    0xccc82aff, 0xcacacaff, 0x000000ff, 0x002bfbff, 0xff331cff, 0xff40fcff,
    0x00f92fff, 0x00fbfeff, 0xfffc36ff, 0xffffffff,
};

constexpr int kScreenWidth = 256;
constexpr int kScreenHeight = 192;
}  // namespace

ZxScreen::ZxScreen(const Memory& memory) : memory_(memory) {}

void ZxScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  auto image = build_image();
  sf::Texture texture;
  texture.loadFromImage(image);
  sf::Sprite sprite;
  sprite.setTexture(texture);
  target.draw(sprite, states);
}

namespace {
std::pair<sf::Color, sf::Color> flash_transform(sf::Color ink,
                                                sf::Color paper) {
  return {sf::Color{paper.g, paper.g, paper.g}, sf::Color{ink.g, ink.g, ink.g}};
}
}  // namespace

sf::Image ZxScreen::build_image() const {
  sf::Image image;
  image.create(kScreenWidth, kScreenHeight * 2);

  constexpr int kNumBanks = 3;
  constexpr int kBankHeight = kScreenHeight / kNumBanks;
  constexpr int kWidthInChars = kScreenWidth / 8;
  constexpr int kCharsPerBank = 32 * 8;
  constexpr uint16_t kPixelMemory = 0x4000;
  constexpr uint16_t kAttrMemory = 0x5800;
  constexpr int kBytesPerBank = kWidthInChars * kBankHeight;

  for (int bank = 0; bank < kNumBanks; ++bank) {
    for (int byte = 0; byte < kWidthInChars * kBankHeight; ++byte) {
      auto col = byte % kWidthInChars;
      auto char_row = byte / kWidthInChars % 8;
      auto char_subrow = byte / kWidthInChars / 8;

      auto attr = memory_.read(kAttrMemory + bank * kCharsPerBank +
                               char_row * kWidthInChars + col);
      auto ink_idx = attr & 0x7;
      auto paper_idx = (attr >> 3) & 0x7;
      if (attr & 0x40) {
        ink_idx += 8;
        paper_idx += 8;
      }
      auto ink = sf::Color(kColors[ink_idx]);
      auto paper = sf::Color(kColors[paper_idx]);
      if (attr & 0x80) {
        std::tie(ink, paper) = flash_transform(ink, paper);
      }
      auto pixels = memory_.read(kPixelMemory + bank * kBytesPerBank + byte);
      for (int bit = 0; bit < 8; ++bit) {
        auto x = 8 * col + bit;
        auto y = kBankHeight * bank + char_row * 8 + char_subrow;
        auto color = sf::Color((pixels & (128 >> bit)) ? ink : paper);
        image.setPixel(x, y, color);
      }
    }
  }

  return image;
}
