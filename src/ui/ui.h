#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

#include "ui/progress-bar.h"
#include "ui/zxscreen.h"

class Ui {
 public:
  Ui();

  bool process_events();
  void draw();
  void set_zx_screen_memory(const Memory& memory);
  void remove_zx_screen();
  ProgressBar& get_progress_bar() { return progress_bar_; }

 private:
  sf::RenderWindow window_;
  ProgressBar progress_bar_;
  std::unique_ptr<ZxScreen> zx_screen_;
};