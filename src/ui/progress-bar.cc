#include "ui/progress-bar.h"

void ProgressBar::set_val(float lo, float hi, float max) {
  lo_ = lo;
  hi_ = hi;
  max_ = max;
}

void ProgressBar::draw(sf::RenderTarget& target,
                       sf::RenderStates states) const {
  sf::RectangleShape lo(sf::Vector2f(lo_ / max_, 1.0f));
  sf::RectangleShape hi(sf::Vector2f(hi_ / max_, 1.0f));
  lo.setFillColor(sf::Color(252, 186, 3));
  hi.setFillColor(sf::Color(66, 135, 245));
  target.draw(hi, states);
  target.draw(lo, states);
}
