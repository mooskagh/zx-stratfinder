#include <glog/logging.h>

#include "emulator/machine.h"
#include "games/jet-set-willy.h"
#include "misc/config.h"
#include "ui/ui.h"
#include "ui/zxscreen.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;
  Ui ui;

  Machine machine;
  machine.set_event_mask(Machine::kEventInterrupt);
  ui.set_zx_screen_memory(machine.memory());
  // machine.set_rzx_writer(std::make_unique<RzxWriter>());
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.load_snapshot(get_data_dir() + "jet-set-willy/szx/intro-screen.szx");

  for (int i = 0; i < 5; ++i) {
    machine.run();
    ui.draw();
    sf::sleep(sf::milliseconds(20));
  }

  machine.input_state().set_keyboard(InputState::kKey_Enter);
  // machine.memory()[manic_miner::level_num] = level;
  machine.add_breakpoint(0x8912);
  machine.set_event_mask(Machine::kEventBreakpoint);
  machine.run();
  machine.input_state() = InputState();

  ui.draw();
  machine.save_snapshot(get_data_dir() + "jet-set-willy/szx/main.szx");

  machine.clear_breakpoints();
  // jet_set_willy::SetWillyPos(&machine, 0x21, 100, 93, true, false, 2, 1);
  jet_set_willy::WillyPos::from_memory(machine.memory()).debug_print();
  machine.add_breakpoint(0x89AD);
  machine.run();
  ui.draw();

  //  machine.set_event_mask(Machine::kEventInterrupt);
  for (int i = 0; i < 200; ++i) {
    if (!ui.process_events()) return 1;
    // LOG(INFO) << "Moo4 " << std::hex << machine.pc();
    // LOG(INFO) << machine.run(1000000);
    machine.run();
    const auto& memory = machine.memory();
    LOG(INFO) << "Room=" << int(memory[jet_set_willy::cur_room]);
    jet_set_willy::WillyPos::from_memory(memory).debug_print();
    ui.draw();
    sf::sleep(sf::milliseconds(100));
    // std::getchar();
  }
  // machine.writer()->write("/tmp/x.rzx");
}