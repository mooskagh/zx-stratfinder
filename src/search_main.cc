#include <glog/logging.h>

#include "games/manic-miner.h"
#include "misc/config.h"
#include "search/search.h"
#include "search/trace-hook.h"

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;

  CHECK(argc >= 2) << "Usage: ./playseq <config> [<moves to trace>]";

  Search search(std::make_unique<Ui>(),
                manic_miner::GetUniversalConfig(argv[1]));
  if (argc >= 3) {
    search.set_hook(
        std::make_unique<TraceHook>(manic_miner::GetUniversalConfig(argv[1]),
                                    get_inputs_from_file(argv[2])));
  }
  search.run(10000);
  return 0;
}
