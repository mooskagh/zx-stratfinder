#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <bits/stdint-uintn.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>

#include "games/writetyper.h"
#include "misc/filebuffer.h"
#include "ui/ui.h"

ABSL_FLAG(std::string, inputsfile, "", "Inputs file");
ABSL_FLAG(std::string, out_rzx, "", "Output rzx");
ABSL_FLAG(float, speed, 1.0f, "Speed");
ABSL_FLAG(float, initial_pause, 0.0f, "Initial pause");

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  absl::ParseCommandLine(argc, argv);

  Ui ui;
  Machine machine;
  ui.set_zx_screen_memory(machine.memory());
  machine.set_rzx_writer(std::make_unique<RzxWriter>());

  machine.set_event_mask(Machine::kEventBreakpoint);
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.load_snapshot(get_filename("writetyper", "szx", "writetyper"));
  machine.add_breakpoint(0x89ad);

  machine.run();
  ui.draw();
  sf::sleep(sf::seconds(absl::GetFlag(FLAGS_initial_pause)));

  int frame = 0;
  // std::getchar();
  for (const auto& input :
       get_inputs_from_file(absl::GetFlag(FLAGS_inputsfile))) {
    machine.input_state() = input;
    auto cycles = machine.run();
    // LOG(INFO) << machine.pc();
    ui.draw();
    ++frame;
    sf::sleep(sf::milliseconds(cycles / 3500.0 / absl::GetFlag(FLAGS_speed)));
  }

  std::vector<std::string> item_image(16, std::string(32, '.'));
  for (int i = 0; i < 83; ++i) {
    uint16_t upper = machine.memory().read(0xA4AD + i);
    uint16_t lower = machine.memory().read(0xA5AD + i);
    int x = (lower & 31);
    int y = ((lower >> 5) & 7) + 8 * ((upper & 0x80) != 0);
    bool collected = (upper & 0x40) == 0;
    if (collected) {
      if (item_image[y][x] == '.') item_image[y][x] = '_';
    } else {
      item_image[y][x] = '#';
    }
  }
  for (const auto& x : item_image) LOG(INFO) << x;
  if (!absl::GetFlag(FLAGS_out_rzx).empty()) {
    machine.writer()->write(absl::GetFlag(FLAGS_out_rzx));
  }

  machine.input_state() = {};
  machine.set_event_mask(Machine::kEventInterrupt);
  for (int i = 0; i < 200; ++i) {
    machine.run(200000);
    sf::sleep(sf::milliseconds(80));
    ui.draw();
  }

  return 0;
}
