#!/usr/bin/python

import graph_pb2
import sys
import os
import re

graph = graph_pb2.GraphDef()
with open(sys.argv[1], 'rb') as fi:
    graph.ParseFromString(fi.read())

nodes = [x.label for x in graph.node]
edges = [[] for _ in nodes]

for x in graph.edge:
    edges[x.from_node].append((x.to_node, x.label, x.items, x.cost))

cur_node = int(sys.argv[2]) if len(sys.argv) > 2 else graph.start_node[0]

arcs = sys.stdin.read().split()

node = nodes[cur_node]
print(f'node={cur_node} label=[{node}]')
total_cost = 0
total_items = 0
while arcs:
    best_idx = None
    best_dst = 0
    best_len = -1
    for i, (dst, label, items, cost) in enumerate(edges[cur_node]):
        cur_arcs = label.split()
        if len(cur_arcs) < best_len:
            continue
        if arcs[:len(cur_arcs)] == cur_arcs:
            best_idx = i
            best_len = len(cur_arcs)
    (dst, label, items, cost) = edges[cur_node][best_idx]
    cur_node = dst
    total_cost += cost
    total_items |= items
    node = nodes[cur_node]
    print(f'edge=[{label}] node={cur_node} label=[{node}] cost={total_cost} items={total_items}')
    for _ in label.split():
        arcs.pop(0)
