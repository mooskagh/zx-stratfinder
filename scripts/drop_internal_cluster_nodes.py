#!/bin/env python3

from plumbum import local, cli, FG
from graph_pb2 import (Node, Edge, GraphDef)
import re
import datetime


def GetNodeList(filename):
    graph = GraphDef()
    SUBGRAPHS = [
        [36, 35, 34, 33, 30, 29, 28, 27, 39, 40, 41, 42, 43],
        [44, 14, 15, 50, 16, 17, 18, 48],
        [60, 59, 58, 51, 49, 47],
        [0, 1, 2, 3, 6, 7, 8, 9, 12, 13],
        [24, 23, 22, 25, 53, 52],
        [57, 56, 55, 54, 31, 37],
        [21, 20, 11, 26, 32, 38, 10, 19, 5, 4, 45, 46],
    ]
    subgraph_map = [0] * 61
    for i, subgraph in enumerate(SUBGRAPHS):
        for r in subgraph:
            subgraph_map[r] = i
    kLabelRe = re.compile(r'(\d+):\d+/\d+\.\d+')
    with open(filename, 'br') as fi:
        graph.ParseFromString(fi.read())
    print(f'{len(graph.node)} total nodes')

    nodes = [[set(), 0, 0] for _ in range(len(graph.node))]
    for i in graph.start_node:
        nodes[i][0].add('start')
    for i in graph.end_node:
        nodes[i][0].add('end')
    for edge in graph.edge:
        room = int(kLabelRe.match(edge.label).group(1))
        subgraph = subgraph_map[room]
        nodes[edge.from_node][0].add(subgraph)
        nodes[edge.from_node][1] += 1
        nodes[edge.to_node][0].add(subgraph)
        nodes[edge.from_node][2] += 1
    filtered = []
    for i, (y, a, b) in enumerate(nodes):
        if len(y) < 2:
            filtered.append((a * b, i))
    return [x[1] for x in sorted(filtered, reverse=True)]


TMPFILE_PREFIX = f'tmp/{datetime.datetime.now().strftime("%y%m%d-%H%M%S")}-'
TMPFILE_COUNTER = 0


def GetTmpFileName():
    global TMPFILE_COUNTER
    filename = TMPFILE_PREFIX + str(TMPFILE_COUNTER)
    TMPFILE_COUNTER += 1
    print(f"Tmp file: {filename}")
    return filename


def RunDropNodes(input, nodes):
    print(f"Dropping nodes {nodes}")
    exe = local['./remove_nodes']
    output = GetTmpFileName()
    exe[f'--input={input}', f'--output={output}',
        f'--drop={",".join([str(x) for x in nodes])}'] & FG
    return output


def RunSimpleEdgeDrop(input):
    print(f"Cleaning suboptimal edges")
    exe = local['./simple_edge_drop']
    output = GetTmpFileName()
    exe[f'--input={input}', f'--output={output}'] & FG
    return output


def RunDropSuboptimalEdges(input):
    print(f"Cleaning suboptimal edges")
    exe = local['./drop_suboptimal_edges']
    output = GetTmpFileName()
    exe[f'--input={input}', f'--output={output}'] & FG
    return output


def RunRemoveDeadEnds(input):
    print(f"Cleaning dead ends")
    exe = local['./remove_dead_ends']
    output = GetTmpFileName()
    exe[f'--input={input}', f'--output={output}'] & FG
    return output


class DropNodes(cli.Application):
    input = cli.SwitchAttr("--input", str, mandatory=True)

    def main(self):

        flow = self.input

        iteration = 0
        while True:
            print(f"Iteration {iteration}")
            nodes_to_remove = GetNodeList(flow)
            print(f'{len(nodes_to_remove)} nodes to remove')
            if not nodes_to_remove:
                break
            nodes = nodes_to_remove[:500]
            flow = RunDropNodes(flow, nodes)
            flow = RunRemoveDeadEnds(flow)
            flow = RunSimpleEdgeDrop(flow)
            if iteration % 40 == 0:
                flow = RunDropSuboptimalEdges(flow)
            iteration += 1
        flow = RunDropSuboptimalEdges(flow)


if __name__ == "__main__":
    DropNodes.run()