#!/bin/env python3

from plumbum import local, cli, FG
from graph_pb2 import (Node, Edge, GraphDef)

SANE_SETS = [
    [14, 15, 16, 27],
    [53, 54, 55, 56, 57, 58],
    [49, 35],
    [31, 32, 46, 47, 33],
    [0, 1, 2, 3],
    [10, 13],
    [6, 7, 8],
]


def ComputeMask(lst):
    res = 0
    for x in lst:
        res |= 1 << x
    return res


SANE_MASKS = [ComputeMask(x) for x in SANE_SETS]


def IsMaskInsane(mask):
    for x in SANE_MASKS:
        if (mask & x) != 0 and (mask & x) != x:
            return True
    return False


class RemoveInsane(cli.Application):
    input = cli.SwitchAttr("--input", str, mandatory=True)
    output = cli.SwitchAttr("--output", str)

    def main(self):
        graph = GraphDef()
        with open(self.input, 'br') as fi:
            graph.ParseFromString(fi.read())

        print(f'nodes={len(graph.node)}')
        print(f'edges={len(graph.edge)}')

        edges = []
        for edge in graph.edge:
            if IsMaskInsane(edge.items):
                continue
            edges.append(edge)

        del graph.edge[:]
        graph.edge.extend(edges)

        print(f'nodes={len(graph.node)}')
        print(f'edges={len(graph.edge)}')

        with open(self.output, 'bw') as fo:
            fo.write(graph.SerializeToString())


if __name__ == "__main__":
    RemoveInsane.run()