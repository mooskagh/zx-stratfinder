#!/usr/bin/perl

use strict;


#
#Room 0 +67+68+69+70+71+72+73+74+75+76+77+78  The Off Licence
#
#  Entryway 0:0 DONE 487f 4500s t=581
#    0:0/0 (54,104)
#      0:0/0.0 (10) -> D
#      0:0/0.1 (75) [68 69] -> D
#      0:0/0.3 (78) -> 1:0/14 (120,90)
#      0:0/0.21 (78) -> 1:0/1 (120,100)
#      0:0/0.20 (78) -> 1:0/5 (120,88)
#      0:0/0.19 (78) -> 1:0/16 (120,96)

my @room_name;
my $cur_room;
my @warps;

while(<>) {
    if (/Room (\d+) (?:\+\d+)*  (.*)/) {
        $cur_room = $1;
        $room_name[$1] = $2;
    }
    if (/-> (\d+):/) {
        $warps[$cur_room]{$1} = ();
    }
}

for my $i (0..$#room_name) {
    print "Room $i $room_name[$i]\n";
    for my $j (sort {$a <=> $b} keys %{$warps[$i]}) {
        print "  -> $j $room_name[$j]\n";
    }
}
