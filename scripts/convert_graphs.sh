#!/bin/bash

export GLOG_log_dir=logs/

function convert {
  filename=$1; shift
  echo Running $filename
  ./graph_convert \
    --input=data/rooms.bin \
    --output=graphs/$filename-tmp.bin \
    "$@"
  echo Removing dead ends.
  ./remove_dead_ends \
    --input=graphs/$filename-tmp.bin \
    --output=graphs/$filename.bin
  rm graphs/$filename-tmp.bin
}

convert G0 --maxlives --warpless
convert G1 --rtc --maxlives --warpless
convert G2 --maxlives
convert G3 --rtc --maxlives
convert G4 --warpless
convert G5 --rtc --warpless
convert G6
convert G7 --rtc
