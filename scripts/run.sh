#!/usr/bin/bash

if [ -f "STOP" ]; then
    echo "STOP exists, exiting."
    exit 1
fi


REQUEST_FILENAME=`mktemp -p .`
REQUEST='task_request{filename:"'$REQUEST_FILENAME$'"}\ncommit{}'
echo "$REQUEST"
TASK_ID=`echo "$REQUEST" | GLOG_log_dir=data/logs/ flock ./jsw_router.lock ./jsw_router --data_dir=data/` || exit
TASK_ID=`printf "%04d" $TASK_ID`

TASK_DIR=task-$TASK_ID
mv $REQUEST_FILENAME shared_data/requests/$TASK_ID-req.bin
mkdir shared_data/logs/$TASK_ID

GLOG_log_dir=shared_data/logs/$TASK_ID/ ./jsw \
    --taskfile=shared_data/requests/$TASK_ID-req.bin \
    --resultfile=shared_data/requests/$TASK_ID-resp.bin \
    --ui=false \
    --data_root=shared_data/ \
    || exit


REQUEST='apply{filename:"'shared_data/requests/$TASK_ID-resp.bin$'"}\ncommit{}'
echo "$REQUEST"
echo "$REQUEST" | GLOG_log_dir=data/logs/ flock ./jsw_router.lock ./jsw_router --data_dir=data/ || exit

flock ./jsw_router.lock tar cvzf shared_data/states/state$TASK_ID.tar.gz data/*.bin
