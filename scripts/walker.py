#!/usr/bin/python

import graph_pb2
import click
import sys
import os
import re

graph = graph_pb2.GraphDef()
with open(sys.argv[1], 'rb') as fi:
    graph.ParseFromString(fi.read())

nodes = [x.label for x in graph.node]
edges = [[] for _ in nodes]

for x in graph.edge:
    edges[x.from_node].append((x.to_node, x.label, x.items, x.cost))

for e in edges:
    e.sort(key=lambda x: (x[3], len(x[1]), x[1]))

click.echo(f'{len(nodes)} nodes')

cur_node = int(sys.argv[2]) if len(sys.argv) > 2 else graph.start_node[0]

ROOMS = [
    "The Off Licence", "The Bridge ", "Under the MegaTree ",
    "At the Foot of the MegaTree", "The Drive", "The Security Guard ",
    "Entrance to Hades", "Cuckoo's Nest", "Inside the MegaTrunk ",
    "On a Branch Over the Drive ", "The Front Door ", "The Hall ", "Tree Top ",
    "Out on a limb", "Rescue Esmerelda ", "I'm sure I've seen this before.. ",
    "We must perform a Quirkafleeg", "Up on the Battlements", "On the Roof",
    "The Forgotten Abbey", "Ballroom East", "Ballroom West",
    "To the Kitchens    Main Stairway ", "The Kitchen", "West of Kitchen",
    "Cold Store ", "East Wall Base ", "The Chapel ", "First Landing",
    "The Nightmare Room ", "The Banyan Tree", "Swimming Pool",
    "Halfway up the East Wall ", "The Bathroom ", "Top Landing",
    "Master Bedroom ", "A bit of tree", "Orangery ", "Priests' Hole",
    "Emergency Generator", "Dr Jones will never believe this ", "The Attic",
    "Under the Roof ", "Conservatory Roof", "On top of the house",
    "Under the Drive", "Tree Root", "[", "Nomen Luni ", "The Wine Cellar",
    "Watch Tower", "Tool  Shed ", "Back Stairway", "Back Door", "West  Wing ",
    "West Bedroom ", "West Wing Roof ", "Above the West Bedroom ", "The Beach",
    "The Yacht", "The Bow"
]

#def LabelToRoom(y):
#    return ', '.join(ROOMS[int(x.split(':')[0])] for x in y.split())


def LabelToRoom(x):
    return ROOMS[int(x.split(':')[0])]


def ItemsToList(items):
    res = []
    idx = 0
    while items:
        if items % 2 == 1:
            res.append(idx)
        idx += 1
        items //= 2
    return res


def SetToItems(x):
    res = 0
    for z in x:
        res |= (1 << z)
    return res


history = []
items_history = [set()]


def ShowHelp():
    click.echo('Commands:')
    click.echo(' h | help  This help')
    click.echo(' go <num>  Go this node')
    click.echo(' back      Undo last move')
    click.echo(' quit      Quit')


fo = open('logs/walker.log', 'a')
fo.write('====================\n')
filter = ''
while True:
    node = nodes[cur_node]
    click.echo('Current node: ' + click.style(node, fg='green') + " " +
               click.style(LabelToRoom(node), fg='magenta'))
    click.echo('Collected items: [' + click.style(' '.join(
        str(x) for x in sorted(items_history[-1])), fg='yellow') + '] total=' 
        + str(len(items_history[-1])))
    if filter:
        click.echo('Filter: ' + click.style(filter, fg='cyan'))
    click.echo('  Edges:')
    try:
        r = re.compile(filter)
    except re.error:
        filter = ''
        r = re.compile('')
    for i, e in enumerate(edges[cur_node]):
        dst_node, label, items, cost = e
        if not r.search(label):
            continue
        click.echo('%4d. %s %s [%s] (%d) %s -- %d' %
                   (i, click.style(label, fg='green'),
                    click.style(nodes[dst_node], fg='blue'), ' '.join(
                        str(x) for x in ItemsToList(items)), cost,
                    click.style(LabelToRoom(nodes[dst_node]), fg='yellow'),
                    len(edges[edges[cur_node][i][0]])))
    cmd = click.prompt('Select command, h for help')
    if cmd and cmd[0] in '0123456789':
        opts = cmd
        cmd = 'g'
    else:
        cmd, _, opts = cmd.partition(' ')

    match cmd:
        case ('h' | 'help'):
            ShowHelp()
        case ('g' | 'go'):
            value = int(opts)
            if 0 <= value < len(edges[cur_node]):
                fo.write('%d %d %d %s\n' % (value, cur_node,
                         SetToItems(items_history[-1]),
                         edges[cur_node][value][1]))
                fo.flush()
                history.append(cur_node)
                new_items = items_history[-1].copy()
                new_items.update(ItemsToList(edges[cur_node][value][2]))
                items_history.append(new_items)
                cur_node = edges[cur_node][value][0]
                filter = ''
        case ('p' | 'play'):
            value = int(opts)
            if 0 <= value < len(edges[cur_node]):
                os.system('echo "play { lives: 7 %s }" | ./shell.sh'
                          % (' '.join('route: \\"%s\\"' % x for x in
                             edges[cur_node][value][1].split())))

        case 'quit':
            break
        case ('f' | 'filter'):
            filter = opts
        case 'back':
            filter = ''
            fo.write(' -\n')
            cur_node = history.pop()
            items_history.pop()
        case _:
            click.echo(click.style('Did not understand', bg='red'))


fo.close()
