#!/usr/bin/bash

echo -n "Rooms discovered: "
cat rooms.txt | grep ":0 DONE" | wc -l
echo -n "   Tasks pending: "
cat rooms.txt | grep PENDING | wc -l
echo -n "    Needs review: "
cat rooms.txt | grep REVIEW | wc -l
echo -n "      Tasks done: "
DONE=$(cat rooms.txt | grep DONE | wc -l)
echo $DONE
echo -n "   Tasks running: "
cat rooms.txt | grep RUNNING | wc -l
echo -n "     Entrypoints: "
ENTRYPOINTS=$(cat rooms.txt | egrep "^    [0-9]" | wc -l)
echo $ENTRYPOINTS
echo -n "Percent complete: "
# echo "scale=2; $DONE * 100 / $ENTRYPOINTS" | bc | tr -d '\n'
echo "scale=2; $DONE * 100 / 5430" | bc | tr -d '\n'
echo "%"
echo -n "          Routes: "
cat rooms.txt | grep "\->" | wc -l
echo -n "Total iterations: "
cat rooms.txt | awk '$1 == "Entryway" {total+=$4} END { print total }'
echo -n "    Route frames: "
cat rooms.txt | awk '$3 == "->" {total+=substr($2, 2)} END { print total }'
