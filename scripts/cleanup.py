#!/usr/bin/python

import sys
import os

filename= sys.argv[1]
ops = sys.argv[2]

OPS = {
        'C': 'combine_useless_edges',
        'D': 'drop_suboptimal_edges',
        'L': 'eliminate_lone_edges',
        'E': 'remove_dead_ends',
        '1': 'unwrap_loops --mode=1',
        '2': 'unwrap_loops --mode=2',
        '3': 'unwrap_loops --mode=3',
        '4': 'unwrap_loops --mode=4',
        '5': 'unwrap_loops --mode=5',
}


src = filename
dst = 'tmp/' + src + '.'

for op in ops:
    command = OPS[op]
    dst += op
    os.system(f'./{command} --input={src} --output={dst}')
    src = dst

if len(sys.argv) > 3:
    os.system(f'cp {dst} {sys.argv[3]}')
