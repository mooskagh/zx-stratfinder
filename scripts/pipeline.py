#!/bin/env python3

from plumbum import local, cli, FG
import datetime

TMPFILE_PREFIX = f'tmp/pipeline-{datetime.datetime.now().strftime("%y%m%d-%H%M%S")}-'
TMPFILE_COUNTER = 0


def GetTmpFileName():
    global TMPFILE_COUNTER
    filename = TMPFILE_PREFIX + str(TMPFILE_COUNTER)
    TMPFILE_COUNTER += 1
    print(f"Tmp file: {filename}")
    return filename


def RunBit(command, input):
    print(f"command")
    exe = local[command]
    output = GetTmpFileName()
    exe[f'--input={input}', f'--output={output}'] & FG
    return output


class Pipeline(cli.Application):
    input = cli.SwitchAttr("--input", str, mandatory=True)

    def main(self):

        flow = self.input

        for i in range(100):
            print(f"Iteration {i}")
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./remove_dead_ends', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./combine_useless_edges', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./remove_dead_ends', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./drop_suboptimal_edges', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./remove_dead_ends', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./eliminate_lone_edges', flow)
            flow = RunBit('./simple_edge_drop', flow)
            flow = RunBit('./remove_dead_ends', flow)
            flow = RunBit('./simple_edge_drop', flow)


if __name__ == "__main__":
    Pipeline.run()