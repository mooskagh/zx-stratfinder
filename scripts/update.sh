#!/usr/bin/bash

cd ~/dev/zx-stratfinder/build/release/
ninja || exit
cd ~/my/games/jsw-2023/
cp ~/dev/zx-stratfinder/build/release/{jsw,jsw_router,graph_convert,build_lower_bounds,remove_dead_ends,filter_estimates,dfs,journey,combine_useless_edges,drop_suboptimal_edges,eliminate_lone_edges,unwrap_loops,remove_nodes,simple_edge_drop} .
