#!/usr/bin/bash

# Applies TaskResult manually (what run.sh does in the end).

TASK_ID=$1

REQUEST='apply{filename:"'shared_data/requests/$TASK_ID-resp.bin$'"}\ncommit{}'
echo "$REQUEST"
echo "$REQUEST" | GLOG_log_dir=data/logs/ flock ./jsw_router.lock ./jsw_router --data_dir=data/ || exit

flock ./jsw_router.lock tar cvzf shared_data/states/state$TASK_ID.tar.gz data/*.bin
